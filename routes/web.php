<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function(){
        return view('welcome');
    });
    Route::get('/path', function() {
        return route('main_telegram_hook');
    });
    Route::post('/'.config('telegram.bot_token').'/webhook', 'TelegramController@hook')
        ->name('main_telegram_hook');
    Route::get('/getMe', 'TelegramController@getMe');
    Route::get('/setHook', 'TelegramController@setWebhook');
    Route::get('/removeHook', 'TelegramController@removeWebhook');
    Route::get('/getCommands', 'TelegramController@getCommands');
    Route::get('/getUpdates', 'TelegramController@getUpdates');
});