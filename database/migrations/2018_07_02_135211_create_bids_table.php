<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');

            $table->string('token')->unique();

            $table->unsignedInteger('currency_id');

            $table->boolean('is_fixed_price')->default(true);

            $table->unsignedDecimal('amount_min', 34, 12)->default(0);
            $table->unsignedDecimal('amount_max', 34, 12)->default(0);
            $table->unsignedDecimal('amount_total', 34, 12);

            /* new bid, await for accept balance,  */
            $table->enum('status', ['prepare', 'open', 'pending', 'closed', 'empty', 'paid'])->default('prepare');

            // for creator
            $table->enum('commission_creator_type', ['bitfinex_percent'])->default('bitfinex_percent');

            $table->double('commission_creator_value');

            // system
            /* фиксированная сумма, процент от сделки */
            $table->enum('commission_type', ['fixed', 'percent', 'none'])->default('none');

            $table->double('commission_value')->default(0);

            $table->unsignedInteger('user_id');

            $table->foreign('user_id')
              ->references('id')
              ->on('users');

            $table->string('details')->default("");

            $table->enum('type', ['buy', 'sell'])->default('sell');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
