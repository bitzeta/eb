<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');

            $table->string('file_name');
            $table->string('mime_type');

            $table->unsignedInteger('thumb_id')->nullable()->default(null);

            $table->foreign('thumb_id')
              ->references('id')
              ->on('photo_sizes');

            $table->string('file_id');
            $table->unsignedInteger('file_size');

            $table->unsignedInteger('balance_accept_id')->nullable()->default(null);

            $table->foreign('balance_accept_id')
              ->references('id')
              ->on('balance_accepts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
