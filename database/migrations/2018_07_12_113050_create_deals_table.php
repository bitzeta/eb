<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');

            $table->string('token')->unique();

            $table->unsignedInteger('bid_id');

            $table->foreign('bid_id')
              ->references('id')
              ->on('bids');

            $table->unsignedDecimal('amount', 34, 12);
            $table->decimal('commission_creator_amount', 34, 12)->default(0);
            $table->unsignedDecimal('amount_bid', 34, 12);

            $table->enum('status', ['new', 'wait', 'pre_allowed', 'allowed', 'pre_disallowed', 'disallowed', 'pre_sold', 'sold'])->default('wait');

            $table->json('currency_details');

            $table->string('ask_details')->default("");

            $table->string('bid_details')->default("");

            $table->unsignedInteger('user_id');

            $table->foreign('user_id')
              ->references('id')
              ->on('users');

            $table->string('symbol');
            $table->string('symbol_details');
            $table->string('symbol_amount');

            $table->integer('auto_close_minutes');

            $table->timestamps();
        });

        Schema::table('selections', function (Blueprint $table) {
            $table->unsignedInteger('deal_id')->nullable()->default(null);

            $table->foreign('deal_id')
              ->references('id')
              ->on('deals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('selections', function (Blueprint $table) {
            $table->dropForeign('selections_deal_id_foreign');
            $table->dropColumn('deal_id');
        });

        Schema::dropIfExists('deals');
    }
}
