<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('duration');
            $table->unsignedInteger('width');
            $table->unsignedInteger('height');
            $table->string('mime_type');

            $table->unsignedInteger('thumb_id')->nullable()->default(null);

            $table->foreign('thumb_id')
              ->references('id')
              ->on('photo_sizes');

            $table->string('file_id');
            $table->unsignedInteger('file_size');

            $table->unsignedInteger('balance_accept_id')->nullable()->default(null);

            $table->foreign('balance_accept_id')
              ->references('id')
              ->on('balance_accepts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
