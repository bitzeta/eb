<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selections', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('currency_id')->nullable()->default(null);

            $table->foreign('currency_id')
              ->references('id')
              ->on('currencies');

            $table->unsignedInteger('payment_system_id')->nullable()->default(null);

            $table->foreign('payment_system_id')
              ->references('id')
              ->on('payment_systems');

            $table->float('amount')->nullable()->default(null);

            $table->string('details')->default("");

            $table->unsignedInteger('user_id');

            $table->foreign('user_id')
              ->references('id')
              ->on('users');

            $table->unsignedInteger('bid_id')->nullable()->default(null);

            $table->foreign('bid_id')
              ->references('id')
              ->on('bids');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selections');
    }
}
