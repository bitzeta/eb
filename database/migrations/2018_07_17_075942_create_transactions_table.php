<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('deal_id')->nullable()->default(null);

            $table->foreign('deal_id')
              ->references('id')
              ->on('deals');

            $table->unsignedInteger('bid_id')->nullable()->default(null);

            $table->foreign('bid_id')
              ->references('id')
              ->on('bids');

            $table->unsignedDecimal('amount', 34, 12);

            $table->unsignedInteger('user_id_from')->nullable()->default(null);

            $table->foreign('user_id_from')
              ->references('id')
              ->on('users');

            $table->unsignedInteger('user_id_to')->nullable()->default(null);

            $table->foreign('user_id_to')
              ->references('id')
              ->on('users');

            $table->enum('status', ['pending', 'allowed'])->default('pending');

            $table->string('comment')->default("");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
