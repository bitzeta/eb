<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_systems', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        Schema::table('bids', function(Blueprint $table) {

            $table->unsignedInteger('payment_system_id')->nullable()->default(null);

            $table->foreign('payment_system_id')
              ->references('id')
              ->on('payment_systems');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {

            $table->dropForeign('bids_payment_system_id_foreign');
            $table->dropColumn('payment_system_id');

        });

        Schema::dropIfExists('payment_systems');
    }
}
