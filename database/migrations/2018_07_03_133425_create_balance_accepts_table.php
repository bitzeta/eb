<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceAcceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_accepts', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('allowed')->default(false);

            $table->string('token')->unique();

            $table->timestamps();
        });

        Schema::table('bids', function(Blueprint $table) {

            $table->unsignedInteger('balance_accept_id')->nullable()->default(null);

            $table->foreign('balance_accept_id')
              ->references('id')
              ->on('balance_accepts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {

            $table->dropForeign('bids_balance_accept_id_foreign');
            $table->dropColumn('balance_accept_id');

        });

        Schema::dropIfExists('balance_accepts');
    }
}
