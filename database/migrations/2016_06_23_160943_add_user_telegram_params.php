<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserTelegramParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table){
            $table->string('command');
            $table->string('auth_token');
            $table->dateTime('last_login');
            $table->unsignedDecimal('amount', 34, 12);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table){
            $table->dropColumn('auth_token');
            $table->dropColumn('command');
            $table->dropColumn('last_login');
            $table->dropColumn('amount');
        });
    }
}
