<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_sizes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('file_id');
            $table->unsignedInteger('file_size');
            $table->unsignedInteger('width');
            $table->unsignedInteger('height');

            $table->unsignedInteger('balance_accept_id')->nullable()->default(null);

            $table->foreign('balance_accept_id')
              ->references('id')
              ->on('balance_accepts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_sizes');
    }
}
