<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $user = \App\User::find(174366505);
        if (!$user) {
            \App\User::create([
                'id'    => 174366505,
                'name'  => 'Serhii',
                'username'=>'zetabit',
                'command'=> '',
                'auth_token'=> str_random(),
                'last_login'=> date('Y-m-d H:i:s'),
                'op'     => 1,
                'chat_id'=> 174366505,
                'amount' => 1000000,
                'is_admin'  => true,
            ]);
        }
        $user = \App\User::find(204367870);
        if (!$user) {
            \App\User::create([
                'id'    => 204367870,
                'name'  => 'mr. Zerno',
                'username'=>'zerno_cleaner',
                'command'=> '',
                'auth_token'=> str_random(),
                'last_login'=> date('Y-m-d H:i:s'),
                'op'     => 1,
                'chat_id'=> 204367870,
                'amount' => 222000,
                'is_admin'  => false,
            ]);
        }

        $this->createCurrencies();
        $this->createPaymentSystems();
        $this->createBids();
    }

    protected function createCurrencies()
    {
        // $data = [
        //   [
        //     'shortcode' => 'UAH',
        //     'symbol'    => '₴'
        //   ],
        //   [
        //     'shortcode' => 'RUB',
        //     'symbol'    => '₽'
        //   ],
        //   [
        //     'shortcode' => 'USD',
        //     'symbol'    => '$'
        //   ],
        //   [
        //     'shortcode' => 'EUR',
        //     'symbol'    => '€'
        //   ],
        // ];
        $data = [
          [
            'shortcode' => 'BTC',
            'symbol'    => 'btc',
            'is_fiat'   => false,
          ],
          [
            'shortcode' => 'ETH',
            'symbol'    => 'eth',
            'is_fiat'   => false,
          ],
          [
            'shortcode' => 'EOS',
            'symbol'    => 'eos',
            'is_fiat'   => false,
          ],
          [
            'shortcode' => 'UAH',
            'symbol'    => '₴',
            'is_fiat'   => true,
          ],
          [
            'shortcode' => 'RUB',
            'symbol'    => '₽',
            'is_fiat'   => true,
          ],
          [
            'shortcode' => 'USD',
            'symbol'    => '$',
            'is_fiat'   => true,
          ],
          [
            'shortcode' => 'EUR',
            'symbol'    => '€',
            'is_fiat'   => true,
          ],
          [
            'shortcode' => 'USDT',
            'symbol'    => '$',
            'is_fiat'   => false
          ]
        ];

        foreach ($data as $item) {
            \App\Models\Currency::firstOrCreate(array_merge($item, ['allowed' => true]));
        }
    }

    protected function createPaymentSystems()
    {
        $data = [
            ['name' => 'PrivatBank'],
            ['name' => 'наличные Киев'],
            ['name' => 'Monobank'],
            ['name' => 'наличные Москва'],
            ['name' => 'Тинькофф'],
            ['name' => 'Сбербанк'],
            ['name' => 'Webmoney'],
            ['name' => 'Yandex Деньги'],
            ['name' => 'PayPal'],
            ['name' => 'МИР']
        ];

        foreach ($data as $item) {
            \App\Models\PaymentSystem::firstOrCreate(array_merge($item));
        }
    }

    protected function createBids()
    {
        /** @var \App\Models\Currency[]|\Illuminate\Support\Collection $currencies */
        $currencies = \App\Models\Currency::all();
        /** @var \App\Models\PaymentSystem[]|\Illuminate\Support\Collection $paymentSystems */
        $paymentSystems = \App\Models\PaymentSystem::all();

        for($i = 0; $i < 1000; $i++) {

            $type           = rand(0, 1) ? \App\Models\Bid::TYPE_SELL : \App\Models\Bid::TYPE_BUY;
            $iCurrency      = rand(0, sizeof($currencies)-1);
            $iPaymentSystem = rand(0, sizeof($paymentSystems)-1);
            $total          = $currencies[$iCurrency]->is_fiat ? rand(1, 10000) : (rand(1, 10000) / 1000);
            $is_fixed       = rand(0, 1);

            $bid = \App\Models\Bid::firstOrCreate([
                'token'             => str_random(8),
                'currency_id'       => $currencies[$iCurrency]->id,
                'payment_system_id' => $currencies[$iCurrency]->is_fiat ? $paymentSystems[$iPaymentSystem]->id : null,
                'is_fixed_price'    => $is_fixed,
                'amount_min'        => $is_fixed ? 0 : (rand(1, 50) / 100) * $total,
                'amount_max'        => $is_fixed ? 0 : (rand(51, 100) / 100) * $total,
                'amount_total'      => $total,
                'status'            => \App\Models\Bid::STATUS_OPEN,
                'commission_creator_type'   => \App\Models\Bid::COMISSION_CREATOR_BITFINEX,
                'commission_creator_value'  => rand(-1, 2),
                'commission_type'   => \App\Models\Bid::COMISSION_SYSTEM_NONE,
                'user_id'           => 174366505,
                'type'              => $type,
            ]);

            $bid->refresh();
            if ($bid->amount_max > $bid->amount_total) {
                throw new Exception('wtf');
            }

        }
    }
}
