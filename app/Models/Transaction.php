<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction
 *
 * @mixin \Eloquent
 * @property-read \App\Models\Bid $bid
 * @property-read \App\Models\Deal $deal
 * @property-read \App\User $userFrom
 * @property-read \App\User $userTo
 * @property int    deal_id
 * @property int    bid_id
 * @property double amount
 * @property int    user_id_from
 * @property int    user_id_to
 * @property string status
 */
class Transaction extends Model
{
    protected $fillable = [
        'deal_id',
        'bid_id',
        'amount',
        'user_id_from',
        'user_id_to',
        'status',
        'comment',
    ];

    public function deal()
    {
        return $this->belongsTo(Deal::class);
    }

    public function bid()
    {
        return $this->belongsTo(Bid::class);
    }

    public function userFrom()
    {
        return $this->belongsTo(User::class,'user_id_from');
    }

    public function userTo()
    {
        return $this->belongsTo(User::class, 'user_id_to');
    }
}
