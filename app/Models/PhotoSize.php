<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PhotoSize
 *
 * @property string  file_id
 * @property integer file_size
 * @property integer width
 * @property integer height
 * @package App\Models
 * @property-read \App\Models\BalanceAccept $balanceAccept
 * @mixin \Eloquent
 */
class PhotoSize extends Model
{
    protected $fillable = [
        'file_id',
        'file_size',
        'width',
        'height',
        'balance_accept_id',
    ];

    public function balanceAccept()
    {
        return $this->belongsTo(BalanceAccept::class);
    }

    public function forTelegram($chat_id)
    {
        return array_merge(
          [
            'photo' => $this->file_id
          ],
          compact('chat_id')
        );
    }
}
