<?php

namespace App\Models;

use App\Services\PriceService;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Bid
 *
 * @property-read \App\Models\BalanceAccept $balanceAccept
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\PaymentSystem $paymentSystem
 * @property string token
 * @property int    currency_id
 * @property int    payment_system_id
 * @property int    balance_accept_id
 * @property boolean is_fixed_price
 * @property int    amount_min
 * @property int    amount_max
 * @property int    amount_total
 * @property string status
 * @property string commission_creator_type
 * @property double commission_creator_value
 * @property string commission_type
 * @property double commission_value
 * @property int    user_id
 * @property string details
 * @property string type
 * @property string status_template
 * @property string commandname_template
 * @mixin \Eloquent
 * @property-read \App\User $user
 */
class Bid extends Model
{
    const STATUS_PREPARE = 'prepare', STATUS_OPEN = 'open', STATUS_PENDING = 'pending', STATUS_CLOSED = 'closed', STATUS_EMPTY = 'empty', STATUS_PAID = 'paid';

    const COMISSION_CREATOR_BITFINEX = 'bitfinex_percent';

    /*                  фиксированная сумма,                    процент от сделки */
    const COMISSION_SYSTEM_SYSTEM = 'system', COMISSION_SYSTEM_PERCENT = 'percent', COMISSION_SYSTEM_NONE = 'none';

    const TYPE_BUY = 'buy', TYPE_SELL = 'sell';

    protected $fillable = [
        'token',
        'currency_id',
        'payment_system_id',
        'balance_accept_id',
        'is_fixed_price',
        'amount_min',               // при покупке это основная валюта, продажа - валюта сделки
        'amount_max',               // при покупке это основная валюта, продажа - валюта сделки
        'amount_total',             // при покупке это основная валюта, продажа - валюта сделки
        'status',
        'commission_creator_type',
        'commission_creator_value',
        'commission_type',
        'commission_value',
        'user_id',
        'details',
        'type',
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function paymentSystem()
    {
        return $this->belongsTo(PaymentSystem::class);
    }

    public function balanceAccept()
    {
        return $this->belongsTo(BalanceAccept::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Нужен ли кошелек в момент создания сделки.
     *
     * @param \App\Models\Bid $bid
     *
     * @return array|bool
     */
    public function isNeedsWalletBidMessage()
    {
        return isNeedsWalletBidMessage($this->type, $this->currency->is_fiat);
    }

    public function isAvailable(User $user, PriceService $priceService)
    {
        if ($this->type == Bid::TYPE_SELL) {    //если продает то в валюте предложения
            $details = [];

            if ($this->is_fixed_price) {
                $total = $priceService->calc(
                  $this->currency->shortcode,
                  $this->amount_total,
                  config('pricing.associated_currency'),
                  $this->type == Bid::TYPE_SELL ? 'ask' : 'bid',
                  $details,
                  $this->commission_creator_value
                );
                return $user->amount >= $total;
            } else {
                $details = [];
                $min = $priceService->calc(
                  $this->currency->shortcode,
                  $this->amount_min,
                  config('pricing.associated_currency'),
                  $this->type == Bid::TYPE_SELL ? 'ask' : 'bid',
                  $details,
                  $this->commission_creator_value
                );
                $max = $priceService->calc(
                  $this->currency->shortcode,
                  $this->amount_max,
                  config('pricing.associated_currency'),
                  $this->type == Bid::TYPE_SELL ? 'ask' : 'bid',
                  $details,
                  $this->commission_creator_value
                );
                return $min <= $user->amount;
            }
        } else {    //если покупает то в основной валюте
            return $this->is_fixed_price ?
              $user->amount >= $this->amount_total
              :
              $user->amount >= $this->amount_min;
        }
    }

    public function getStatusTemplateAttribute()
    {
        if ($this->status == Bid::STATUS_OPEN) {
            return "активно";
        } elseif ($this->status == Bid::STATUS_PREPARE) {
            return "временно";
        } elseif ($this->status == Bid::STATUS_CLOSED) {
            return "закрыто";
        } elseif ($this->status == Bid::STATUS_EMPTY) {
            return "сумма исчерпана";
        } elseif ($this->status == Bid::STATUS_PENDING) {
            return "ожидание оплаты";
        } elseif ($this->status == Bid::STATUS_PAID) {
            return "оплачено";
        }
    }

    public function getCommandnameTemplateAttribute()
    {
        if ($this->type == Bid::TYPE_SELL) {
            if ($this->currency->is_fiat) {
                return 'accbuy';
            } else {
                return 'buy';
            }
        } else {
            if ($this->currency->is_fiat) {
                return 'accsell';
            } else {
                return 'sell';
            }
        }
    }
}
