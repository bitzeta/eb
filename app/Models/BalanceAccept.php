<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BalanceAccept
 *
 * @property boolean        allowed
 * @property string         token
 *
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PhotoSize[] $photoSizes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Video[] $videos
 */
class BalanceAccept extends Model
{
    protected $fillable = [
        'allowed',
        'token'
    ];

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function photoSizes()
    {
        return $this->hasMany(PhotoSize::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
