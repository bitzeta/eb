<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Selection
 *
 * @property-read \App\Models\Bid $bid
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\PaymentSystem $paymentSystem
 * @property-read \App\User $user
 * @property int    currency_id
 * @property int    payment_system_id
 * @property double amount
 * @property string details
 * @property int    user_id
 * @property int    bid_id
 * @property int    deal_id
 * @mixin \Eloquent
 * @property-read \App\Models\Deal $deal
 */
class Selection extends Model
{
    protected $fillable = [
        'currency_id',
        'payment_system_id',
        'amount',
        'details',
        'user_id',
        'bid_id',
        'deal_id',
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function paymentSystem()
    {
        return $this->belongsTo(PaymentSystem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bid()
    {
        return $this->belongsTo(Bid::class);
    }

    public function deal()
    {
        return $this->belongsTo(Deal::class);
    }
}
