<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Video
 *
 * @property integer    duration
 * @property integer    width
 * @property integer    height
 * @property string     mime_type
 * @property integer    thumb_id
 * @property string     file_id
 * @property integer    file_size
 * @property integer    balance_accept_id
 *
 * @package App\Models
 * @property-read \App\Models\BalanceAccept $balanceAccept
 * @mixin \Eloquent
 */
class Video extends Model
{
    protected $fillable = [
        'duration',
        'width',
        'height',
        'mime_type',
        'thumb_id',
        'file_id',
        'file_size',
        'balance_accept_id',
    ];

    public function balanceAccept()
    {
        return $this->belongsTo(BalanceAccept::class);
    }

    public function forTelegram($chat_id)
    {
        return array_merge(
          [
            'video' => $this->file_id
          ],
          compact('chat_id')
        );
    }
}
