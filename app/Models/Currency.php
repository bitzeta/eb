<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Currency
 *
 * @property string     shortcode
 * @property string     symbol
 * @property boolean    allowed
 * @property boolean    is_fiat
 * @mixin \Eloquent
 */
class Currency extends Model
{
    protected $fillable = [
        'id',
        'shortcode',
        'symbol',
        'allowed',
        'is_fiat',
    ];

    protected $casts = [
        'allowed'   => 'boolean'
    ];

    public $timestamps = false;
}
