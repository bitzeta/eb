<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentSystem
 *
 * @property string name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Bid[] $bids
 * @mixin \Eloquent
 */
class PaymentSystem extends Model
{
    protected $fillable = [
        'name',
    ];

    public function bids()
    {
        return $this->hasMany(Bid::class, 'payment_system_id');
    }

    public function bidsOpen()
    {
        return $this->bids()->where('status', Bid::STATUS_OPEN);
    }
}
