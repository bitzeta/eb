<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Document
 *
 * @property string     file_name
 * @property string     mime_type
 * @property integer    thumb_id
 * @property string     file_id
 * @property integer    file_size
 * @property integer    balance_accept_id
 *
 * @package App\Models
 * @property-read \App\Models\BalanceAccept $balanceAccept
 * @mixin \Eloquent
 */
class Document extends Model
{
    protected $fillable = [
        'file_name',
        'mime_type',
        'thumb_id',
        'file_id',
        'file_size',
        'balance_accept_id',
    ];

    public function balanceAccept()
    {
        return $this->belongsTo(BalanceAccept::class);
    }

    public function forTelegram($chat_id)
    {
        return array_merge(
          [
            'document' => $this->file_id
          ],
          compact('chat_id')
        );
    }
}
