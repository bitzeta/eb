<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Deal
 *
 * @property-read \App\Models\Bid $bid
 * @property-read \App\User $user
 * @property string token
 * @property int    bid_id
 * @property double amount
 * @property double amount_bid
 * @property string status
 * @property int    user_id
 * @property array  currency_details
 * @property string ask_details
 * @property string bid_details
 * @property string symbol
 * @property string symbol_details
 * @property string symbol_amount
 * @property int    auto_close_minutes
 * @property string status_template
 * @mixin \Eloquent
 */
class Deal extends Model
{
    const STATUS_NEW = 'new', STATUS_WAIT = 'wait', STATUS_PRE_SOLD = 'pre_sold', STATUS_SOLD = 'sold',
        STATUS_PRE_ALLOWED = 'pre_allowed', STATUS_ALLOWED = 'allowed',
        STATUS_PRE_DISALLOWED = 'pre_disallowed', STATUS_DISALLOWED = 'disallowed';

    protected $fillable = [
        'token',
        'bid_id',
        'amount_bid',       //сколько начислять на личный счет после завершения сделки
        'amount',           //сумма которую должен получить/отдать на руки/кошелек, на прямую, которая просчитана в через биржу
        'commission_creator_amount', //сколько процентов снялось с суммы в пользу создателя заявки
        'status',
        'user_id',
        'currency_details',
        'ask_details',      //детали текстом с биржи
        'bid_details',      //детали текстом с биржи
        'symbol',           //для выбранной валютной пары
        'symbol_details',   //детали просчета на момент времени текстом
        'symbol_amount',    //просчитанное число на момент вермени
        'auto_close_minutes',//закрывать сделку по истечению времени
    ];

    protected $casts = [
      'currency_details'  => 'array'
    ];

    public function __construct(array $attributes = [])
    {
        $this->currency_details = [];
        parent::__construct($attributes);
    }

    public function bid()
    {
        return $this->belongsTo(Bid::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isUserIdAllowedToSold($user_id)
    {
        if ($this->bid->type == Bid::TYPE_BUY) {
            if (!$this->bid->currency->is_fiat && $user_id == $this->bid->user->id) {   //инициатор продает крипту за долл
                return true;
            } elseif ($this->bid->currency->is_fiat && $user_id == $this->user->id) {  //инициатор продает долл за фиат
                return true;
            }
        } else {    //sell
            if (!$this->bid->currency->is_fiat && $user_id == $this->user->id) {    //инициатор покупает крипту за долл
                return true;
            } elseif ($this->bid->currency->is_fiat && $user_id == $this->bid->user->id) {  //инициатор покупает долл за фиат
                return true;
            }
        }
    }

    public function getStatusTemplateAttribute()
    {
        if ($this->status == Deal::STATUS_SOLD) {
            return "оплачена";
        } elseif ($this->status == Deal::STATUS_WAIT) {
            return "ожидание";
        } elseif ($this->status == Deal::STATUS_NEW) {
            return "активная";
        } elseif ($this->status == Deal::STATUS_ALLOWED) {
            return "утверждена сторонами";
        } elseif ($this->status == Deal::STATUS_PRE_SOLD) {
            return "ожидание подтверждения";
        } elseif ($this->status == Deal::STATUS_DISALLOWED) {
            return "отменена";
        } elseif ($this->status == Deal::STATUS_PRE_ALLOWED) {
            return "зарезервировано";
        } elseif ($this->status == Deal::STATUS_PRE_DISALLOWED) {
            return "зарезервировано";
        }
    }
}
