<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class StartCommand extends Command
{
    protected $name = "start";
    protected $description = "Начните с команды start";

    /**
     * @var Message $message
     */
    protected $message;

    /**
     * @var \App\User $user
     */
    protected $user;

    /**
     * @var int $user_id
     */
    protected $user_id;

    /**
     * @var string $text
     */
    protected $text;

    public function handle($arguments)
    {
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($this->getUpdate());

        if (!$user && $from && $chat) {
            $user = \App\User::create([
                'id'        => $from->getId(),
                'name'      => $from->getFirstName(),
                'command'   => 'start',
                'auth_token'=> str_random(128),
                'op'        => 1,
                'chat_id'   => $chat->getId(),
                'username'  => $from->getUsername() ? $from->getUsername() : '',    //empty username
                'last_login'=> date('Y-m-d H:i:s'),
                'amount'    => 0,
            ]);

            $this->replyWithMessage([
                'text'  => "Регистрация прошла успешно.\r\n"
            ]);
        }

        // $this->replyWithMessage([
        //     'text'  => "Добро пожаловать в анонимную торговую биржу - ГАРАНТ!\r\n"
        // ]);

        $user->setStateCommand('main');
        $this->triggerCommand('main');
    }
}

