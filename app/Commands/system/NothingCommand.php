<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands\system;

use \Telegram\Bot\Commands\Command;

class NothingCommand extends Command
{
    protected $name = "nothing";
    protected $description = "команда ничего не делает";

    public function handle($arguments)
    {
        global $bNotChangeCommand, $bAccessChangeData;
        $bNotChangeCommand = true;
    }
}

