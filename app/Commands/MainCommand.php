<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use \Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class MainCommand extends Command
{
    protected $name = "main";
    protected $description = "меню для авторизованных";
    protected $user, $user_id, $text;

    public function handle($arguments)
    {
        $this->replyWithChatAction(['action'=>Actions::TYPING]);
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($this->getUpdate());

        $user_id = $user ? $user->id : null;

        if($chat && $user_id!=$chat->getId()){
            global $bNotChangeCommand;
            $bNotChangeCommand = true;
            $reply_markup = Keyboard::hide();
            $this->replyWithMessage([
                'text' => 'Данная команда работает только тет-а-тет (1 на 1)',
                'reply_markup' => $reply_markup
            ]);
            return;
        }


        if (!$user) {
            $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            return;
        }

        $keyboard = [
            [
                ['text'=>trans('menu.buy')],
                ['text'=>trans('menu.sell')],
            ],
            [
                ['text'=>trans('menu.account')],
                ['text'=>trans('menu.cabinet')],
            ]
        ];

        $reply_markup = Keyboard::make([
            'keyboard'  =>  $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false
        ]);

        if ( $text == trans('menu.buy') ){

            $user->setStateCommand('buy');
            $this->triggerCommand('buy');

        } elseif ( $text == trans('menu.sell') ) {

            $user->setStateCommand('sell');
            $this->triggerCommand('sell');

        } elseif ( $text == trans('menu.account') ) {

            $user->setStateCommand('account');
            $this->triggerCommand('account');

        } elseif ( $text == trans('menu.fill_in') ){

            $user->setStateCommand('accbuy');
            $this->triggerCommand('accbuy');

        } elseif ( $text == trans('menu.withdraw') ) {

            $user->setStateCommand('accsell');
            $this->triggerCommand('accsell');

        } elseif ( $text == trans('menu.cabinet') ) {

            $user->setStateCommand('cabinet');
            $this->triggerCommand('cabinet');

        } else {
            $this->replyWithMessage(
              [
                'text'         =>  "Добро пожаловать в анонимную торговую биржу - ГАРАНТ!\r\n",
                'reply_markup' => $reply_markup,
                'parse_mode'   => 'Markdown'
              ]
            );
        }
    }
}

