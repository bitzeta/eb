<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Models\BalanceAccept;
use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\Document;
use App\Models\PaymentSystem;
use App\Models\PhotoSize;
use App\Models\Selection;
use App\Models\Video;
use App\Services\BidsService;
use Illuminate\Support\Facades\DB;
use \Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\Objects\User;

class AddBuyCryptCommand extends AddBidCommand
{
    protected $name = "addbuycrypt";
    protected $description = "добавить свое предложение по покупке [crypt]";

    /**
     * @var Chat    $chat
     * @var Message $message
     * @var User    $from
     * @var \App\User $user
     * @var string  $text
     */
    protected $chat, $message, $from, $user, $text;

    /** @var Selection */
    protected $selection;

    /** @var BidsService */
    protected $bidsService;

    /** @var boolean */
    protected $is_fiat = false;

    protected $is_sell = false;
}

