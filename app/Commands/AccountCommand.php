<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class AccountCommand extends Command
{
    protected $name = "account";
    protected $description = "Ваш счет";

    /**
     * @var Message $message
     */
    protected $message;

    /**
     * @var \App\User $user
     */
    protected $user;

    /**
     * @var int $user_id
     */
    protected $user_id;

    /**
     * @var string $text
     */
    protected $text;

    public function handle($arguments)
    {
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($this->getUpdate());

        if($chat && $from->getId()!=$chat->getId()){
            global $bNotChangeCommand;
            $bNotChangeCommand = true;
            $reply_markup = Keyboard::hide();
            $this->replyWithMessage([
              'text' => 'Данная команда работает только тет-а-тет (1 на 1)',
              'reply_markup' => $reply_markup
            ]);
            return;
        }

        if (!$user) {
            $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            return;
        }

        if ($text == trans('menu.to_menu')) {

            $user->setStateCommand('main');
            $this->triggerCommand('main');

        } elseif ( $text == trans('menu.fill_in') ){

            $user->setStateCommand('accbuy');
            $this->triggerCommand('accbuy');

        } elseif ( $text == trans('menu.withdraw') ) {

            $user->setStateCommand('accsell');
            $this->triggerCommand('accsell');

        } else {
            $keyboard = [
              [
                [
                  'text'          => trans('menu.fill_in'),
                ],
                [
                  'text'          => trans('menu.withdraw'),
                ],
              ],
              [
                [
                  'text'          => trans('menu.to_menu'),
                ],
                [
                  'text'          => trans('menu.update'),
                ],
              ]
            ];
            $resize_keyboard = true;

            $this->replyWithMessage([
              'text'  => 'На вашем счету ' . $user->amount . " $",
              'reply_markup'  => Keyboard::make(compact('keyboard', 'resize_keyboard')),
            ]);
        }
    }
}

