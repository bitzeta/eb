<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Models\Bid;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class SellCommand extends BuyCommand
{
    protected $name = "sell";
    protected $description = "Продать крипту на $";

    protected $is_fiat = false;

    protected $bidType = Bid::TYPE_BUY;
}

