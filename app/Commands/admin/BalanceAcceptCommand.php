<?php

namespace Telegram\Bot\Commands\admin;

use App\Models\BalanceAccept;
use App\Models\Bid;
use App\Services\BidsService;
use Telegram\Bot\Actions;
use Telegram\Bot\Api;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

/**
 * Class HelpCommand.
 */
class BalanceAcceptCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'blaccept';

    /**
     * @var array Command Aliases
     */
    protected $aliases = [];

    /**
     * @var string Command Description
     */
    protected $description = 'Подтверждение оплаты, посмотреть заявку, подтвердить (только админам)';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        global $bNotChangeCommand;
        $bNotChangeCommand = true;

        $this->bidsService = $bidsService = new BidsService($this->getTelegram());

        $this->replyWithChatAction(['action'=>Actions::TYPING]);
        $update = $this->getUpdate();
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($this->getUpdate());

        $user_id = $user ? $user->id : null;

        if($chat && $user_id!=$chat->getId()){
            global $bNotChangeCommand;
            $bNotChangeCommand = true;
            $reply_markup = Keyboard::hide();
            $this->replyWithMessage([
              'text' => 'Данная команда работает только тет-а-тет (1 на 1)',
              'reply_markup' => $reply_markup
            ]);
            return;
        }


        if (!$user) {
            $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            return;
        }

        if (!is_array($arguments)) $arguments = [$arguments];

        if(isset($update['callback_query']) && is_array($arguments)) {  //answer
            $token = ""; $type = ""; $value = null;
            if (sizeof($arguments) == 3)
                list($type, $token, $value) = $arguments;
            elseif (sizeof($arguments) == 2)
                list($type, $token) = $arguments;
            elseif (sizeof($arguments) == 1)
                list($token) = $arguments;

            $balanceAccept = BalanceAccept::where('token', $token)->first();

            if (!$balanceAccept) {
                $this->replyWithMessage([
                  'text'    => 'Не найден запрос на подтверждение баланса.'
                ]);
                return;
            }

            $bid           = Bid::where('balance_accept_id', $balanceAccept->id)->first();

            if (!$bid) {
                $this->replyWithMessage([
                  'text'    => 'Не найдено предложение на это подтверждение баланса.'
                ]);
                return;
            }

            if ($bid->status != Bid::STATUS_PREPARE) {
                $this->replyWithMessage([
                  'text'    => 'По этому предложению уже есть решение.'
                ]);
                return;
            }

            if ($type == 'allow') {
                $balanceAccept->allowed = true;
                $balanceAccept->save();
                $bid->status = Bid::STATUS_OPEN;
                $bid->save();
                $this->getTelegram()->answerCallbackQuery([
                  'callback_query_id' => $update['callback_query']['id'],
                  'text' => 'Предложение опубликовано'
                ]);
            } elseif ($type == 'deny') {
                $balanceAccept->allowed = false;
                $balanceAccept->save();
                $bid->status = Bid::STATUS_CLOSED;
                $bid->save();
                $this->getTelegram()->answerCallbackQuery([
                  'callback_query_id' => $update['callback_query']['id'],
                  'text' => 'Предложение заблокировано'
                ]);
            } else {
                self::message($chat->getId(), $this->telegram, $balanceAccept);
            }
        } elseif (is_array($arguments) && isset($arguments['step'])) {
            ;
        } elseif (!isset($update['callback_query']) && sizeof($arguments) > 0) {
            if(sizeof($arguments) == 1){ //cmd_acceptToken
                $val = $arguments[0];

                $balanceAccept = BalanceAccept::where('token', $val)->first();

                if (!$balanceAccept) {
                    $this->replyWithMessage([
                      'text'    => 'Не найден запрос на подтверждение баланса.'
                    ]);
                    return;
                }

                self::message($chat->getId(), $this->telegram, $balanceAccept);
            }
        } else {    //in message command
        }
    }

    public static function message($chat_id, Api $telegram, BalanceAccept $balanceAccept){
        $telegram->sendMessage([
          'chat_id' => $chat_id,
          'text'    => '--- Начало тела запроса [' . $balanceAccept->id . '] ---'
        ]);

        if ($balanceAccept->videos->count() > 0) {
            foreach ($balanceAccept->videos as $video) {
                $telegram->sendVideo($video->forTelegram($chat_id));
            }
        }

        if ($balanceAccept->documents->count() > 0) {
            foreach ($balanceAccept->documents as $document) {
                $telegram->sendDocument($document->forTelegram($chat_id));
            }
        }

        if ($balanceAccept->photoSizes->count() > 0) {

            foreach ($balanceAccept->photoSizes as $photoSize) {
                $telegram->sendPhoto($photoSize->forTelegram($chat_id));
            }
        }

        $telegram->sendMessage([
          'chat_id' => $chat_id,
          'text'    => '--- Конец тела запроса [' . $balanceAccept->id . '] ---'
        ]);
    }
}
