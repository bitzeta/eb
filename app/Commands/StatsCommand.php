<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Bitfinex;
use \Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class StatsCommand extends Command
{
    protected $name = "stats";
    protected $description = "Bitfinex stats";

    public function handle($arguments)
    {
        $this->replyWithChatAction(['action'=>Actions::TYPING]);
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($this->getUpdate());

        $user_id = $user->id;

        $bitfinex = new Bitfinex(env('BITFINEX_KEY'), env('BITFINEX_SECRET'));

        $stats = $bitfinex->get_ticker();

        $this->replyWithMessage([
            'text'  => var_export($stats, true)
        ]);
    }
}

