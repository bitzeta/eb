<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\PaymentSystem;
use App\Models\Selection;
use App\Services\BidsService;
use Illuminate\Support\Facades\DB;
use \Telegram\Bot\Actions;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\Objects\User;

class BidCommand extends Command
{
    protected $name = "bid";
    protected $description = "предложение, создать";

    /**
     * @var Chat    $chat
     * @var Message $message
     * @var User    $from
     * @var \App\User $user
     * @var string  $text
     */
    protected $chat, $message, $from, $user, $text;

    /** @var Selection */
    protected $selection;

    /** @var BidsService */
    protected $bidsService;

    /** @var boolean */
    protected $is_fiat = false;

    /**
     * For callback cmdName_token_typeCommand_value.
     *              cmdName_token_typeCommand_ [value optional]
     *              cmdName_token_typeCommand
     *
     * @param $arguments
     */
    public function handle($arguments)
    {
        global $bNotChangeCommand;
        $bNotChangeCommand = true;
        $this->bidsService = $bidsService = new BidsService($this->telegram);
        $this->replyWithChatAction(['action'=>Actions::TYPING]);
        $update = $this->getUpdate();
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($update);
        $this->user = $user;

        $user_id = $user ? $user->id : null;

        if (!$user) {
            if ($user_id==$chat->getId()) {
                $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            }
            return;
        }

        if (isset($update['callback_query']) && is_array($arguments)) {
            $message_id = $update['callback_query']['message']['message_id'];

            $token = ""; $type = ""; $value = null;
            if (sizeof($arguments) == 3)
                list($token, $type, $value) = $arguments;
            elseif (sizeof($arguments) == 2)
                list($token, $type) = $arguments;
            elseif (sizeof($arguments) == 1)
                list($token) = $arguments;

            $bid = Bid::where('token', $token)->first();

            if (!$bid) {
                return;
            }

            if ($type == 'preClose') {
                if (!($bid->status == Bid::STATUS_CLOSED || $bid->status == Bid::STATUS_EMPTY)) {
                    $inline_keyboard = [
                      [
                        [
                          'text'            => 'да, отменить предложение',
                          'callback_data'   => 'bid_' . $bid->token . '_close',
                        ],
                      ],
                      [
                        [
                          'text'            => 'нет, не хочу отменять',
                          'callback_data'   => 'bid_' . $bid->token . '_show',
                        ],
                      ],
                    ];

                    $reply_markup   = Keyboard::make(compact('inline_keyboard'));

                    $params = [
                      'text'  => 'Вы действительно хотите отменить предложение /bid_' . $bid->token . '? Отменить это действие невозможно.',
                      'reply_markup'  => $reply_markup,
                    ];

                    if ($message_id) {
                        $params['message_id'] = $message_id;
                        $params['chat_id']  = $user->chat_id;
                        try {
                            $this->getTelegram()->editMessageText($params);
                        } catch (\Exception $exception) {
                            unset($params['message_id']);
                            $this->getTelegram()->sendMessage($params);
                        }
                    } else {
                        $this->replyWithMessage($params);
                    }
                }
            } elseif ($type == 'close') {
                if (!($bid->status == Bid::STATUS_CLOSED || $bid->status == Bid::STATUS_EMPTY)) {
                    $bidsService->closeBid($bid);
                    $bid->refresh();

                    $params = [
                      'text'  => 'Предложение /bid_' . $bid->token . ' закрыто.',
                    ];

                    if ($message_id) {
                        $params['message_id'] = $message_id;
                        $params['chat_id']  = $user->chat_id;
                        try {
                            $this->getTelegram()->editMessageText($params);
                        } catch (\Exception $exception) {
                            unset($params['message_id']);
                            $this->getTelegram()->sendMessage($params);
                        }
                    } else {
                        $this->replyWithMessage($params);
                    }
                }

                if ($bid->status == Bid::STATUS_CLOSED) {
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'Предложение закрыто'
                    ]);
                } else {
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'Ошибка закрытия предложения! Предложение всё еще открыто?'
                    ]);
                }
            } elseif ($type == 'show') {
                $params = [
                  'text'  => 'Действие отменено',
                ];

                if ($message_id) {
                    $params['message_id'] = $message_id;
                    $params['chat_id']  = $user->chat_id;
                    try {
                        $this->getTelegram()->editMessageText($params);
                    } catch (\Exception $exception) {
                        unset($params['message_id']);
                        $this->getTelegram()->sendMessage($params);
                    }
                } else {
                    $this->replyWithMessage($params);
                }
            }

        } elseif (is_array($arguments) && isset($arguments[0])) {

            $bid = Bid::where('token', $arguments[0])->first();
            $selection = Selection::where('user_id', $user_id)->orderBy('created_at', 'desc')->first();
            if (!$selection) {
                $selection = Selection::create(['user_id'   => $user->id]);
            }

            if (!$bid) {
                $this->replyWithMessage([
                    'text'  => 'Такая сделка не найдена'
                ]);
                return;
            }


            $params = [
              'text'  => self::bidMessage($user, $bid->paymentSystem, $bid, $bid->type, $bid->currency->is_fiat)
            ];

            if (!($bid->status == Bid::STATUS_CLOSED || $bid->status == Bid::STATUS_EMPTY) && ($user->id == $bid->user->id || $user->is_admin)) {
                $inline_keyboard = [
                  [
                    [
                      'text'            => 'отменить предложение',
                      'callback_data'   => 'bid_' . $bid->token . '_preClose',
                    ],
                    [
                      'text'      => 'Начать сделку',
                      'callback_data'=> $bid->commandname_template . '_' . BuyCommand::STEP_SELECTED_BID . '_' . 0 . '_' . $selection->id . '_' . $bid->id,
                    ],
                  ],
                ];
            } else {
                $inline_keyboard = [
                  [
                    [
                      'text'      => 'Предложение закрыто',
                      'callback_data'   => 'nothing'
                    ],
                  ],
                ];
            }
            $reply_markup   = Keyboard::make(compact('inline_keyboard'));
            $params['reply_markup'] = $reply_markup;

            $this->replyWithMessage($params);
            //
        } else {
            $user->setStateCommand('main');
            $this->triggerCommand('main');
            return;
        }
    }

    public static function bidMessage(\App\User $user, PaymentSystem $paymentSystem = null, Bid $bid, $type, $is_fiat)
    {
        $bidsService = new BidsService(new Api(config('telegram.bot_token')));
        return
          (($bid->status == Bid::STATUS_CLOSED || $bid->status == Bid::STATUS_EMPTY) ? "Предложение уже не действительно. \r\n" : "")
          . ($type == Bid::TYPE_SELL ? 'Купить' : 'Продать') . (config('app.env') == 'local' ? '[' . $bid->id . ']' : '')
          . ($paymentSystem ? ' через (' . $paymentSystem->name . ')' : '') . "\r\n"
          . ($bid->isNeedsWalletBidMessage() ? "Кошелек: " . $bid->details . "\r\n" : "" )
          . "По этому предложению можно " . ($type == Bid::TYPE_SELL ? 'купить' : 'продать') . ' ' . ($is_fiat ? ' USD за ' : '') . $bid->currency->shortcode . "\r\n"
          . "на сумму " . ($bid->is_fixed_price ? ' в ' . $bid->amount_total . $bid->currency->shortcode : ' от ' . $bid->amount_min . ' до ' . ($bid->amount_max > $bid->amount_total ? $bid->amount_total : $bid->amount_max) . $bid->currency->shortcode ) . '. ' . "\r\n"
          . ($bid->isAvailable($user, $bidsService->priceService) || ($is_fiat && $type == Bid::TYPE_SELL) || (!$is_fiat && $type == Bid::TYPE_BUY) ? 'Вы можете начать сделку' : ' У вас недостаточно средств для начала этой сделки. Пополнить можно командой /accbuy') . " \r\n"
          //. ($bid->user_id == $user->id ? "отменить сделку /bid_" . $bid->token . '_cancel' : "")
          . (($bid->status == Bid::STATUS_CLOSED || $bid->status == Bid::STATUS_EMPTY) ? "Предложение уже не действительно. \r\n" : "")
        ;
    }

}

