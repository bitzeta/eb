<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Models\Bid;
use App\Models\Deal;
use App\Models\Transaction;
use App\Services\BidsService;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class CabinetCommand extends Command
{
    protected $name = "cabinet";
    protected $description = "Кабинет";

    /**
     * @var Message $message
     */
    protected $message;

    /**
     * @var \App\User $user
     */
    protected $user;

    /**
     * @var int $user_id
     */
    protected $user_id;

    /**
     * @var string $text
     */
    protected $text;

    public function handle($arguments)
    {
        $update = $this->getUpdate();

        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($this->getUpdate());

        if($chat && $from->getId()!=$chat->getId()){
            global $bNotChangeCommand;
            $bNotChangeCommand = true;
            $reply_markup = Keyboard::hide();
            $this->replyWithMessage([
              'text' => 'Данная команда работает только тет-а-тет (1 на 1)',
              'reply_markup' => $reply_markup,
            ]);
            return;
        }

        if (!$user) {
            $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            return;
        }

        if (isset($update['callback_query']) && is_array($arguments)) {
            $message_id = $update['callback_query']['message']['message_id']; //$message ? $message->getMessageId() : null;

            $type = ""; $value = null;
            if (sizeof($arguments) == 2)
                list($type, $value) = $arguments;
            elseif (sizeof($arguments) == 1)
                list($type) = $arguments;

            if ($type == 'bids' && $value == 'closeAll') {
                $inline_keyboard [] = [
                  [
                    'text'          => trans('menu.back'),
                    'callback_data' => $this->name.'_bids_1',
                  ],
                  [
                    'text'          => trans('bid.yes_close'),
                    'callback_data' => $this->name.'_bids_yesCloseAll'
                  ]
                ];

                $params = [
                  'message_id'   => $message_id,
                  'chat_id'      => $this->getUpdate()->getChat()->getId(),
                  'text'         => 'Действительно хотите закрыть все свои заявки на предложение? Отменить данное действие невозможно',
                  'reply_markup' => Keyboard::make(compact('inline_keyboard')),
                ];
                $this->getTelegram()->editMessageText($params);
            } elseif ($type == 'bids' && $value == 'yesCloseAll') {
                $bids = Bid::where('user_id', $user->id)->get();
                $params = [
                  'message_id'   => $message_id,
                  'chat_id'      => $this->getUpdate()->getChat()->getId(),
                  'text'         => 'Закрываю заявки, ожидайте...',
                ];
                try {
                    $this->getTelegram()->editMessageText($params);
                } catch (\Exception $exception) {
                    unset($params['message_id']);
                    $this->getTelegram()->sendMessage($params);
                }
                $bidsService = new BidsService(new Api(config('telegram.bot_token')));
                foreach ($bids as $bid) {
                    $bidsService->closeBid($bid);
                }
                $params['text'] = 'Заявки закрыты.';

                try {
                    $this->getTelegram()->editMessageText($params);
                } catch (\Exception $exception) {
                    unset($params['message_id']);
                    $this->getTelegram()->sendMessage($params);
                }
            } elseif ($type == 'bids') {
                if ($value <= 0) {
                    return;
                }
                $this->bidsMessage($user, $value, $user->chat_id, $message_id);
            }

            return;
        }

        if ($text == trans('menu.to_menu')) {

            $user->setStateCommand('main');
            $this->triggerCommand('main');

        } elseif ( $text == trans('menu.my_bids') ) {

            $this->bidsMessage($user, 1);

        } elseif ( $text == trans('menu.my_deals') ) {

            $deals = Deal::where('user_id', $user->id)->get();

            $text = "Ваши сделки (как активные так и завершенные):\r\n";
            /** @var Deal $deal */
            foreach ($deals as $deal) {
                $text .= " (" . ($deal->status_template) . ") /deal_" . $deal->token . " \r\n";
            }

            $this->replyWithMessage([
              'text'    => $deals->count() > 0 ? $text : 'У вас еще не было сделок.',
            ]);

        } elseif ( $text == trans('menu.my_transactions') ) {

            $transactions = Transaction::where('user_id_from', $user->id)->orWhere('user_id_to', $user->id)->get();

            $text = "Ваши завершенные транзакции:\r\n";
            /** @var Transaction $transaction */
            foreach ($transactions as $transaction) {
                $text = $text
                    . ($transaction->user_id_from == $user->id ? 'кредит' : 'дебет') . " "
                    . ' ' . ($transaction->user_id_from == $user->id ? '-' : '+') . currencyHumanized(true, $transaction->amount) . " $ "
                    . ($transaction->bid ? "предложение /bid_" . $transaction->bid->token : "") . " "
                    . ($transaction->deal ? "сделка /deal_" . $transaction->deal->token : "")
                    . "\r\n"
                ;
            }

            $this->replyWithMessage([
              'text'    => $transactions->count() > 0 ? $text : 'У вас еще не было транзакций.',
            ]);
        } else {
            $keyboard = [
              [
                [
                  'text'          => trans('menu.my_bids'),
                ],
                [
                  'text'          => trans('menu.my_deals'),
                ],
              ],
              [
                [
                  'text'          => trans('menu.to_menu'),
                ],
                [
                  'text'          => trans('menu.my_transactions'),
                ],
              ],
            ];
            $resize_keyboard = true;

            $this->replyWithMessage([
              'text'  => 'На вашем счету ' . $user->amount . " $ \r\n"
                    .   "Выберите команду.",
              'reply_markup'  => Keyboard::make(compact('keyboard', 'resize_keyboard')),
            ]);
        }
    }

    public function bidsMessage(\App\User $user, int $page = 1, $chat_id = null, $message_id = null)
    {
        $perPage = 20;
        $dbPage = $page - 1;
        $bids = Bid::where('user_id', $user->id)->offset($perPage * $dbPage)->limit($perPage)->get();
        $bidsCount = Bid::where('user_id', $user->id)->count();
        $lastPage = ceil($bidsCount/$perPage);
        //if ($bidsCount % $perPage != 0) $lastPage += 1;

        $inline_keyboard = [
          [
            [
              'text'            => 'Закрыть все',
              'callback_data'   => $this->name . '_bids_closeAll'
            ]
          ],
          [
            [
              'text'            => '<<<',
              'callback_data'   => $this->name . '_bids_' . 1,
            ],
            [
              'text'            => ($page - 1) . '<<',
              'callback_data'   => $this->name . '_bids_' . ($page - 1),
            ],
            [
              'text'            => '>>' . ($page + 1),
              'callback_data'   => $this->name . '_bids_' . ($page + 1),
            ],
            [
              'text'            => '>>>',
              'callback_data'   => $this->name . '_bids_' . $lastPage,
            ],
          ],
        ];
        $reply_markup   = Keyboard::make(compact('inline_keyboard'));

        $text = "Ваши заявки (как активные так и завершенные):\r\n";
        /** @var Bid $bid */
        foreach ($bids as $bid) {
            $text .= " (" . ($bid->status_template) . ") /bid_" . $bid->token . " \r\n";
        }
        $text .= "[" . str_random(8) . "]";

        $params = [
          'text'    => $bids->count() > 0 ? $text : 'У вас еще не было предложений.',
          'reply_markup'  => $reply_markup,
        ];

        if ($message_id && $chat_id) {
            $params['message_id'] = $message_id;
            $params['chat_id'] = $chat_id;
            try {
                $this->getTelegram()->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $this->getTelegram()->sendMessage($params);
            }
        } else {
            $this->replyWithMessage($params);
        }

    }
}

