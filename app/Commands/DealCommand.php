<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Events\DealStatusChangedEvent;
use App\Models\Bid;
use App\Models\Deal;
use App\Services\BidsService;
use App\Services\DealService;
use \Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class DealCommand extends Command
{
    protected $name = "deal";
    protected $description = "сделка (системная команда)";

    /**
     * @var Chat    $chat
     * @var Message $message
     * @var User    $from
     * @var \App\User $user
     * @var string  $text
     */
    protected $chat, $message, $from, $user, $text;

    /** @var BidsService */
    protected $bidsService;

    /**
     * For callback cmdName_token_typeCommand_value.
     *              cmdName_token_typeCommand_ [value optional]
     *              cmdName_token_typeCommand
     *
     * @param $arguments
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function handle($arguments)
    {
        global $bNotChangeCommand;
        $bNotChangeCommand = true;
        $this->bidsService = $bidsService = new BidsService($this->getTelegram());
        $this->replyWithChatAction(['action'=>Actions::TYPING]);
        $update = $this->getUpdate();
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($update);
        $this->user = $user;

        $user_id = $user ? $user->id : null;

        if (!$user) {
            if ($user_id==$chat->getId()) {
                $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            }
            if (isset($update['callback_query'])) {
                $this->getTelegram()->answerCallbackQuery([
                  'callback_query_id' => $update['callback_query']['id'],
                  'text' => 'Пожалуйста закончите регистрацию... Используйте /start'
                ]);
            }
            return;
        }

        if (isset($update['callback_query']) && is_array($arguments)) {
            $token = ""; $type = ""; $value = null;
            if (sizeof($arguments) == 3)
                list($token, $type, $value) = $arguments;
            elseif (sizeof($arguments) == 2)
                list($token, $type) = $arguments;
            elseif (sizeof($arguments) == 1)
                list($token) = $arguments;

            $deal = Deal::where('token', $token)->first();

            if (!$deal) {
                return;
            }

            $message_id = $update['callback_query']['message']['message_id']; //$message ? $message->getMessageId() : null;

            if ($deal->status == Deal::STATUS_DISALLOWED || $deal->status == Deal::STATUS_SOLD) {
                return;
            }

            switch ($type) {
                case 'allow':   //подтверждение сделки
                    if ($from->getId() == $deal->bid->user->id) {
                        $this->getTelegram()->answerCallbackQuery([
                          'callback_query_id' => $update['callback_query']['id'],
                          'text' => 'Спасибо, уведомление отправлено.'
                        ]);
                        if ($deal->bid->type == Bid::TYPE_SELL) {   //инициатор покупает
                            if ($deal->bid->currency->is_fiat) {
                                //создателю запрос и сообщение с чё-куда оплатить
                                $this->bidsService->dealAllow($deal, false, null);
                                $this->bidsService->messageDeal($deal, $message_id, true);
                            } else {
                                //создателю запрос и сообщение с чё-куда оплатить
                                $this->bidsService->dealAllow($deal, true, $message_id);
                                $this->bidsService->messageDeal($deal, null, false);
                            }
                        } else {    //инициатор продает
                            if ($deal->bid->currency->is_fiat) {
                                //инициатор запрос и сообщение с чё-куда оплатить
                                $this->bidsService->dealAllow($deal, false, null);
                                $this->bidsService->messageDeal($deal, $message_id, true);
                            } else {
                                //инициатор запрос и сообщение с чё-куда оплатить
                                $this->bidsService->dealAllow($deal, false, null);
                                $this->bidsService->messageDeal($deal, $message_id, true);
                            }
                        }
                        //принимаем сделку и для покупателя новое сообщение или для продавца обновляем
                        //$this->bidsService->dealAllow($deal, $deal->bid->type == Bid::TYPE_SELL && $deal->bid->currency->is_fiat ? null : $message_id);
                        //отправляем "условия утверждены сторонами" в обновленное сообщение покупателю или в новое продавцу
                    } else {
                        $this->getTelegram()->answerCallbackQuery([
                          'callback_query_id' => $update['callback_query']['id'],
                          'text' => 'Только создатель предложения может поменять статус'
                        ]);
                    }
                    // $this->getTelegram()->answerCallbackQuery([
                    //   'callback_query_id' => $update['callback_query']['id'],
                    //   'text' => 'Предложение подтверждено'
                    // ]);
                    break;
                case 'smaller':
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'В разработке'
                    ]);
                    break;
                case 'disallow':
                    $this->bidsService->dealDisallow($deal);
                    $this->bidsService->messageDeal($deal, $message_id);
                    break;
                case 'preSold'://покупатель/продавец отмечает что выполнил условия сделки
                    //TODO: check initiator
                    $deal->status = Deal::STATUS_PRE_SOLD;
                    $deal->save();
                    event(new DealStatusChangedEvent($deal));
                    $this->bidsService->dealPreSoldInitiator(
                        $deal,
                        $deal->bid->isNeedsWalletBidMessage() ? $message_id : null,
                        !$deal->bid->currency->is_fiat && !$deal->bid->isNeedsWalletBidMessage() ? $message_id : null
                    );
                    break;
                // case 'soldMessage':    //продавец отмечает что выполнил условия сделки
                //     if ($from->getId() == $deal->bid->user->id) {
                //         $this->getTelegram()->answerCallbackQuery([
                //           'callback_query_id' => $update['callback_query']['id'],
                //           'text' => 'Спасибо, уведомление отправлено.'
                //         ]);
                //
                //         $this->bidsService->messageDeal($deal, null, false);
                //     } else {
                //         $this->getTelegram()->answerCallbackQuery([
                //           'callback_query_id' => $update['callback_query']['id'],
                //           'text' => 'Только создатель предложения может поменять статус'
                //         ]);
                //     }
                //     break;
                case 'sold':    //создатель предложения должен поставить этот статус
                    if ($deal->isUserIdAllowedToSold($from->getId())) {
                        $this->bidsService->dealSold($deal);
                        $this->bidsService->messageDeal($deal, !$deal->bid->isNeedsWalletBidMessage() ? $message_id : null, true);
                        $this->bidsService->messageDeal($deal, $deal->bid->isNeedsWalletBidMessage() ? $message_id : null, false);
                    } else {
                        $this->getTelegram()->answerCallbackQuery([
                          'callback_query_id' => $update['callback_query']['id'],
                          'text' => 'У вас недостаточно прав на изменение статуса'
                        ]);
                    }
                    break;
                case 'cancel':    //отменили сделку
                    // разобратсья кто, продавец или покупатель, кому снижать например , рейтинг
                    $this->bidsService->dealDisallow($deal);
                    $this->bidsService->messageDeal($deal, $message_id, false);
                    break;
                case 'discussion':    //открыли спор
                    // разобратсья кто, продавец или покупатель
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'В разработке'
                    ]);
                    // зачем разбираться, сразу обоим в новом сообщении шлем что спор открыт, общаться можно через внутренюю систему сообщений
                    break;
                default:
                    break;
            }

        } elseif (is_array($arguments)) {
            if ($text == trans('menu.to_menu')) {
                $user->setStateCommand('main');
                $this->triggerCommand('main');
                return;
            }

            list($token) = $arguments;

            /** @var Deal $deal */
            $deal = Deal::with('bid', 'bid.currency')->where('token', $token)->first();

            if (!$deal) {
                return;
            }

            $this->replyWithMessage([
                'text' => 'Сделка (' . $deal->status_template . ') по предложению /bid_' . $deal->bid->token . "\r\n"
                . ' на сумму ' . $deal->amount . '$ что эквивалентно ' . $deal->amount_bid . $deal->bid->currency->shortcode . " по курсу " . $deal->symbol_details
                . " на момент создания сделки\r\n"
            ]);
            //
        }  elseif (is_array($arguments) && isset($arguments['step'])) {
            $step = $arguments['step'];

            if ($text == trans('menu.to_menu')) {
                $user->setStateCommand('main');
                $this->triggerCommand('main');
                return;
            }

            $this->replyWithMessage([
                'text'  => var_export($arguments, true)
            ]);

            //
        } else {
            $this->replyWithMessage([
              'text'  => var_export($update, true)
            ]);
            $user->setStateCommand('main');
            $this->triggerCommand('main');
            return;
        }
    }

    public function reply_markupBuilder(array $arr, $key = 'name'){
        $keyboard = buttonBuilder($arr, $key);
        $reply_markup = Keyboard::make([
          'keyboard'  =>  $keyboard,
          'resize_keyboard' => true,
          'one_time_keyboard' => false,
        ]);
        return $reply_markup;
    }
}

