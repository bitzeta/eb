<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Models\BalanceAccept;
use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\Document;
use App\Models\PaymentSystem;
use App\Models\PhotoSize;
use App\Models\Selection;
use App\Models\Video;
use App\Services\BidsService;
use Illuminate\Support\Facades\DB;
use \Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\Objects\User;

class AddBidCommand extends Command
{
    protected $name = "addbid";
    protected $description = "добавить свое предложение";

    const OP_SELECT_CUR = 1, OP_SELECT_PAY_SYS = 2, OP_SELECT_BID = 3, OP_FILL_DETAILS = 4, OP_IS_FIXED_AMOUNT = 5,
        OP_PERCENT = 0x5a, OP_FILL_AMOUNT = 6;

    /**
     * @var Chat    $chat
     * @var Message $message
     * @var User    $from
     * @var \App\User $user
     * @var string  $text
     */
    protected $chat, $message, $from, $user, $text;

    /** @var Selection */
    protected $selection;

    /** @var BidsService */
    protected $bidsService;

    /** @var boolean */
    protected $is_fiat = true;

    protected $is_sell = true;

    /**
     * For callback cmdName_token_typeCommand_value.
     *              cmdName_token_typeCommand_ [value optional]
     *              cmdName_token_typeCommand
     *
     * @param $arguments
     */
    public function handle($arguments)
    {
        $this->bidsService = $bidsService = new BidsService($this->telegram);
        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $update = $this->getUpdate();
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($update);
        $this->user = $user;

        $user_id = $user ? $user->id : null;

        if (!$user) {
            if ($user_id == $chat->getId()) {
                $this->replyWithMessage(['text'=>'Пожалуйста закончите регистрацию... Используйте /start']);
            }
            return;
        }

        $selection = Selection::where('user_id', $user_id)->orderBy('created_at', 'desc')->first();

        if (!$selection)
            $selection = Selection::create(['user_id' => $user_id]);

        $this->selection = $selection;

        if (isset($update['callback_query']) && is_array($arguments)) {
            $op = 1; $page = 0; $selectionId = null; $value = null;
            if (sizeof($arguments) == 4)
                list($op, $page, $selectionId, $value) = $arguments;
            elseif (sizeof($arguments) == 3)
                list($op, $page, $selectionId) = $arguments;
            elseif (sizeof($arguments) == 2)
                list($op, $page) = $arguments;
            elseif (sizeof($arguments) == 1)
                list($op) = $arguments;

            if ($selectionId != null && !$selection) {
                $selection = Selection::find($selectionId);
            }

            if (!$selection) {
                return;
            }

            $message_id = $update['callback_query']['message']['message_id']; //$message ? $message->getMessageId() : null;

            $bid  = $selection->bid;

            if ($bid) { //предложение уже создано
                return;
            }

            if ($op == self::OP_SELECT_CUR) { //select currency
                $this->selectCurrencyMessage($selection, $message_id);
            } elseif ($op == self::OP_SELECT_PAY_SYS && $value) {  //currency got, select payment system
                $currency = Currency::find($value);
                if (!$currency) return;
                $selection->currency_id = $currency->id;
                $selection->save();
                if ($this->is_fiat) {
                    $this->paymentSystemMessage($selection, $message_id);
                } else {
                    if (!$this->is_sell) {
                        $this->walletMessage($selection, $message_id);
                    } else {
                        $selection->details = "";
                        $selection->save();
                        $this->bidsService->createBid($selection, $user->id, $this->is_sell ? Bid::TYPE_SELL : Bid::TYPE_BUY);
                        $this->canISubBidsMessage();
                        $user->setStateCommand($this->name, self::OP_IS_FIXED_AMOUNT, $selection->id);
                    }
                }
            } elseif ($op == self::OP_SELECT_BID && $value) {  //paymentSystem got, select bid
                $paymentSystem = PaymentSystem::find($value);
                if (!$paymentSystem) return;
                $selection->payment_system_id = $paymentSystem->id;
                $selection->save();

                if (!$this->is_sell) {
                    $this->walletMessage($selection, $message_id);
                } else {
                    $selection->details = "";
                    $selection->save();
                    $this->bidsService->createBid($selection, $user->id, $this->is_sell ? Bid::TYPE_SELL : Bid::TYPE_BUY);
                    $this->canISubBidsMessage();
                    $user->setStateCommand($this->name, self::OP_IS_FIXED_AMOUNT, $selection->id);
                }
            }
        } elseif (is_array($arguments) && isset($arguments['step'])) {
            $step = $arguments['step'];

            if ($text == trans('menu.to_menu')) {
                $user->setStateCommand('main');
                $this->triggerCommand('main');
                return;
            }

            $bid = Bid::find($selection->bid_id);
            if ($bid && $bid->status != Bid::STATUS_PREPARE) {
                $this->replyWithMessage([
                  'text'  => 'К сожалению сделка уже в стакане.',
                ]);
                return;
            }
            // if ($bid) { //предложение уже создано
            //     return;
            // }

            if ($step == self::OP_FILL_DETAILS && mb_strlen($text) > 0) {
                $selection->details = $text;
                $selection->save();

                $this->bidsService->createBid($selection, $user->id, $this->is_sell ? Bid::TYPE_SELL : Bid::TYPE_BUY);

                $this->canISubBidsMessage();

                $user->setStateCommand($this->name, self::OP_IS_FIXED_AMOUNT, $selection->id);
            } elseif ($step == self::OP_IS_FIXED_AMOUNT && mb_strlen($text) > 0 && $bid) {
                if ($text == trans('bid.sub_bids.no')) {
                    $bid->is_fixed_price = true;
                    $bid->save();

                    $this->whatCommissionMessage($selection);
                    $user->setStateCommand($this->name, self::OP_PERCENT, $selection->id);

                } elseif ($text == trans('bid.sub_bids.yes')) {
                    $bid->is_fixed_price = false;
                    $bid->save();

                    $this->whatCommissionMessage($selection);
                    $user->setStateCommand($this->name, self::OP_PERCENT, $selection->id);

                } else {
                    $this->replyWithMessage(
                      [
                        'text' => 'Я Вас не понимаю',
                      ]
                    );
                    $this->canISubBidsMessage();
                }
            } elseif ($step == self::OP_PERCENT && mb_strlen($text) > 0 && $bid) {
                $text = str_replace(" ", "", $text);
                $text = str_replace(",", ".", $text);
                $amount = floatval($text);

                $bid->commission_creator_type = Bid::COMISSION_CREATOR_BITFINEX;
                $bid->commission_creator_value = $amount;
                $bid->save();

                $this->totalAmountMessage($selection);
                $user->setStateCommand($this->name, self::OP_FILL_AMOUNT, $selection->id);
            } elseif ($step == self::OP_FILL_AMOUNT && mb_strlen($text) > 0 && $bid) { //amount
                $text = str_replace(" ", "", $text);
                $text = str_replace(",", ".", $text);
                $amount = floatval($text);

                if ($amount <= 0) {
                    $this->replyWithMessage([
                      'text'  => 'Отрицательная сумма (или 0) это прикольно :) Но увы таких сумм не понимаю.',
                    ]);
                    $this->totalAmountMessage($selection);
                    return;
                }

                if ($bid->type == Bid::TYPE_BUY && $user->amount < $amount) {
                    $this->replyWithMessage([
                      'text'  => 'У вас недостаточно средств на счету, доступно ' . $user->amount . ' $. Пополнить можно командой /accbuy',
                    ]);
                    $this->totalAmountMessage($selection);
                    return;
                }

                $this->bidsService->setBidAmount($bid, $amount);

                if ($bid->is_fixed_price) {
                    if (($this->is_sell && $this->is_fiat) && (!$this->is_sell && !$this->is_fiat)) {   //привыводе долл и при покупке крипты ненадо
                        $this->doneMessage($user, $bid);
                        return;
                    } else {
                        $this->submitYourBalanceMessage($selection);
                    }
                } else {
                    $this->minAmountBidsMessage();
                }

                $user->setStateCommand($this->name, $bid->is_fixed_price ? 9 : 7, $selection->id);
            } elseif ($step == 7 && mb_strlen($text) > 0 && $bid) { //min amount
                $text = str_replace(" ", "", $text);
                $text = str_replace(",", ".", $text);
                $amount = floatval($text);

                if ($amount <= 0) {
                    $this->replyWithMessage([
                      'text'  => 'Отрицательная сумма (или 0) это прикольно :) Но увы таких сумм не понимаю.',
                    ]);
                    return;
                }

                $bid->amount_min = $amount;
                $bid->save();

                $this->maxAmountBidsMessage($selection);
                $user->setStateCommand($this->name, 8, $selection->id);
            } elseif ($step == 8 && mb_strlen($text) > 0 && $bid) { //max amount
                $text = str_replace(" ", "", $text);
                $text = str_replace(",", ".", $text);
                $amount = floatval($text);

                if ($amount <= 0) {
                    $this->replyWithMessage([
                      'text'  => 'Отрицательная сумма (или 0) это прикольно :) Но увы таких сумм не понимаю.',
                    ]);
                    return;
                }

                $bid->amount_max = $amount;
                $bid->save();

                if (!$this->is_sell) {  //заявка
                    $this->doneMessage($user, $bid);
                    return;
                }

                if (($this->is_sell && $this->is_fiat) && (!$this->is_sell && !$this->is_fiat)) {   //привыводе долл и при покупке крипты ненадо
                    $this->doneMessage($user, $bid);
                    return;
                } else {
                    $this->submitYourBalanceMessage($selection);
                }
                $user->setStateCommand($this->name, 9, $selection->id);
            } elseif ($step == 9 && $bid) {
                if (!$bid->balanceAccept) {
                    $balAc = BalanceAccept::create([
                        'token' => str_random(),
                    ]);
                    $bid->balance_accept_id = $balAc->id;
                    $bid->save();
                    $bid->refresh();
                }

                if ($text == trans('bid.accept_done')) {
                    if ($bid->balanceAccept->videos->count() <= 0) {
                        $this->replyWithMessage([
                          'text'  => 'Видео является обязательным для проверки Вашей заявки.',
                        ]);
                        return;
                    } else {
                        $this->doneMessage($user, $bid);
                        return;
                    }
                }

                if ($video = $this->getUpdate()->getMessage()->getVideo()) {
                    $v = Video::create(array_merge($video->toArray(), ['balance_accept_id' => $bid->balanceAccept->id]));
                    $this->replyWithMessage([
                      'text'  => 'Видео сохранено. Для завершения нажмите "готово".',
                    ]);
                    return;
                }

                if ($photo = $this->getUpdate()->getMessage()->getPhoto()) {
                    $p = PhotoSize::create(array_merge($photo->last(), ['balance_accept_id' => $bid->balanceAccept->id]));
                    $this->replyWithMessage([
                      'text'  => 'Фото сохранено. Для завершения нажмите "готово".',
                    ]);
                    return;
                }

                if ($document = $this->getUpdate()->getMessage()->getDocument()) {
                    $doc = Document::create(array_merge($document->toArray(), ['balance_accept_id' => $bid->balanceAccept->id]));
                    $this->replyWithMessage([
                      'text'  => 'Файл сохранен. Для завершения нажмите "готово".',
                    ]);
                    return;
                }

                $this->submitYourBalanceMessage($selection);
            }
        } else {
            global $bNotChangeCommand;
            $bNotChangeCommand = true;
            $selection = Selection::create(['user_id' => $user_id]);
            $this->selection = $selection;
            $this->selectCurrencyMessage($selection);
            $user->setStateCommand('main');
            return;
        }
    }

    protected function selectCurrencyMessage(Selection $selection, $message_id = null)
    {
        $currencies = Currency::where('allowed', true)->where('is_fiat', $this->is_fiat)->get();

        $inline_keyboard = [];

        $iterator = 0; $items = [];
        foreach ($currencies as $currency) {
            if (sizeof($items) > 0 && $iterator%3 == 0) {
                $inline_keyboard[] = $items;
                $items = [];
                $iterator = 0;
            }
            $items[] = [
              'text'          => $currency->shortcode,
              'callback_data' => $this->name . '_' . 2 . '_' . 0 . '_' . $selection->id . '_' . $currency->id,
            ];
            $iterator++;
        }
        if (sizeof($items) > 0) $inline_keyboard[] = $items;

        $reply_markup = Keyboard::make(compact('inline_keyboard'));

        $params = [
          'chat_id'       => $this->getUpdate()->getChat()->getId(),
          'text'          => 'Выберите какую валюту Вы хотите ' . ($this->is_sell ? 'продать' : 'купить'),
          'reply_markup'  => $reply_markup,
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }
    }

    protected function paymentSystemMessage(Selection $selection, $message_id = null)
    {
        $payIds     = DB::select(
          'SELECT DISTINCT(`payment_system_id`) as id, COUNT(`payment_system_id`) as cnt, name
                  FROM `bids`
                   INNER JOIN payment_systems ON payment_systems.id = payment_system_id
                  WHERE bids.currency_id = ' . $selection->currency_id . '
                  GROUP BY `payment_system_id`'
        );

        $inline_keyboard = [];
        foreach ($payIds as &$item) {
            $arr = (array)$item;
            $inline_keyboard[] = [
              [
                'text'          =>  $arr['name'] . '(' . $arr['cnt'] . ')',
                'callback_data' =>  $this->name . '_' . 3 . '_' . 0 . '_' . $selection->id . '_' . $arr['id'],
              ],
            ];
        }

        $inline_keyboard [] = [
          [
            'text'          => trans('menu.back'),
            'callback_data' => $this->name . '_' . 1 . '_' . 0 . '_' . $selection->id,
          ],
        ];

        $params = [
          'chat_id'       => $this->getUpdate()->getChat()->getId(),
          'text'          => 'Выберите платежный сервис',
          'reply_markup'  => Keyboard::make(compact('inline_keyboard')),
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }

        return;
    }

    protected function walletMessage(Selection $selection, $message_id)
    {
        $keyboard =
          [
            [
              [
                'text'      => trans('menu.to_menu'),
              ],
              [
                'text'      => trans('buy.no_comment'),
              ],
            ],
          ];

        if (!$this->is_sell) {
            $keyboard =
              [
                [
                  [
                    'text'      => trans('menu.to_menu'),
                  ],
                ],
              ];
        }

        $resize_keyboard = true;

        $params = [
          'text'    => ($selection->paymentSystem ? 'Способ оплаты выбран' : 'Валюта ' . $selection->currency->shortcode . ' выбрана') .
            ($this->is_sell ? ', можете ввести комментарий' : ', укажите номер кошелька'),
          'reply_markup'    => Keyboard::make(compact('keyboard', 'resize_keyboard')),
        ];

        if ($message_id) {
            try {
                $this->telegram->editMessageText(
                  [
                    'chat_id'    => $this->getUpdate()->getChat()->getId(),
                    'message_id' => $message_id,
                    'text'       => $selection->paymentSystem ? 'Способ оплаты выбран' : 'Валюта '.$selection->currency->shortcode.' выбрана',
                  ]
                );
            } catch (\Exception $exception) {
                $this->telegram->sendMessage(
                  [
                      'chat_id' => $this->getUpdate()->getChat()->getId(),
                      'text'       => $selection->paymentSystem ? 'Способ оплаты выбран' : 'Валюта '.$selection->currency->shortcode.' выбрана',
                  ]
                );
            }
        }

        $this->replyWithMessage($params);

        $selection->user->setStateCommand($this->name, self::OP_FILL_DETAILS, $selection->id);
    }

    protected function canISubBidsMessage()
    {
        $reply_markup = Keyboard::make([
          'keyboard'  => [
            [
              [
                'text' => trans('bid.sub_bids.no'),
              ],
            ],
            [
              [
                'text' => trans('bid.sub_bids.yes'),
              ],
            ],
            [
              [
                'text' => trans('menu.to_menu'),
              ],
            ],
          ],
          'resize_keyboard' => true
        ]);

        $this->replyWithMessage([
          'text'  => 'Можно разбивать обмен на несколько меньших?',
          'reply_markup'  => $reply_markup,
        ]);
    }

    protected function minAmountBidsMessage()
    {
        $reply_markup = Keyboard::make([
          'keyboard'  => [
            [
              [
                'text' => trans('menu.to_menu'),
              ],
            ],
          ],
          'resize_keyboard' => true,
        ]);

        $this->replyWithMessage([
          'text'  => 'Минимально возможная сумма ' . ($this->is_sell ? '' : '$') . ' за раз:',
          'reply_markup'  => $reply_markup,
        ]);
    }

    protected function maxAmountBidsMessage(Selection $selection)
    {
        $reply_markup = Keyboard::make([
          'keyboard'  => [
            [
              [
                'text' => currencyHumanized(!$this->is_sell, $selection->bid->amount_total),
              ],
            ],
            [
              [
                'text' => trans('menu.to_menu'),
              ],
            ],
          ],
          'resize_keyboard' => true,
        ]);

        $this->replyWithMessage([
          'text'  => 'Максимально возможная сумма ' . ($this->is_sell ? '' : '$') . ' за раз:',
          'reply_markup'  => $reply_markup,
        ]);
    }

    protected function whatCommissionMessage(Selection $selection)
    {
        $reply_markup = Keyboard::make([
          'keyboard'  => [
            [
              [
                'text' => trans('menu.to_menu'),
              ],
            ],
          ],
          'resize_keyboard' => true,
        ]);

        $this->replyWithMessage([
          'text'  => 'Какой процент от курса, (например: 0 - по курсу; процент > 0 - наценка в вашу пользу; процент < 0 - наценка не в Вашу пользу):',
          'reply_markup'  => $reply_markup,
        ]);
    }

    protected function totalAmountMessage(Selection $selection)
    {
        $reply_markup = Keyboard::make([
          'keyboard'  => [
            [
              [
                'text' => trans('menu.to_menu'),
              ],
            ],
          ],
          'resize_keyboard' => true,
        ]);

        $this->replyWithMessage([
          'text'  => ($this->is_sell ? 'Укажите сколько' : 'Укажите за сколько $ со своего счета') . ' Вы готовы ' . ($this->is_sell ? 'продать' : 'купить') . ' ' . $selection->bid->currency->shortcode,
          'reply_markup'  => $reply_markup,
        ]);
    }

    protected function submitYourBalanceMessage(Selection $selection)
    {
        $reply_markup = Keyboard::make([
          'keyboard'  => [
            [
              [
                'text'  => trans('bid.accept_done'),
              ],
            ],
            [
              [
                'text' => trans('menu.to_menu'),
              ],
            ],
          ],
          'resize_keyboard' => true,
        ]);

        $this->replyWithMessage([
          'text'  => 'Подтверждение баланса, пришлите видео с суммой и этим кошельком. Это поможет администрации на много быстрее принять решение.',
          'reply_markup'  => $reply_markup,
        ]);
    }

    protected function newBidMessages(Bid $bid)
    {
        $admins = \App\User::where('is_admin', true)->get();

        /** @var \App\User $admin */
        foreach ($admins as $admin) {
            $inline_keyboard = [
              [
                [
                  'text'          => 'заблокировать',
                  'callback_data' => 'blaccept_deny_' . $bid->balanceAccept->token
                ],
                [
                  'text'          => 'материалы',
                  'callback_data' => 'blaccept_' . $bid->balanceAccept->token
                ],
                [
                  'text'          => 'опубликовать',
                  'callback_data' => 'blaccept_allow_' . $bid->balanceAccept->token
                ],
              ]
            ];

            $reply_markup = Keyboard::make(compact('inline_keyboard'));

            $this->telegram->sendMessage([
                'chat_id'   => $admin->chat_id,
                'text'      => 'Создано новое предложение /bid_' . $bid->token . "\r\n" .
                    "материалы подтверждающие баланс /blaccept_" . $bid->balanceAccept->token,
                'reply_markup'  => $reply_markup,
            ]);
        }
    }

    protected function doneMessage(\App\User $user, Bid $bid)
    {
        if (!$bid->balanceAccept) {
            $balAc = BalanceAccept::create([
              'token' => str_random(),
            ]);
            $bid->balance_accept_id = $balAc->id;
            $bid->save();
            $bid->refresh();
        }

        $this->replyWithMessage([
          'text'  => 'Заявка на предложение /bid_' . $bid->token . ' отправлена на рассмотрении администрации, и о результате мы Вам сообщим, спасибо.',
        ]);
        $user->setStateCommand('main');
        $this->triggerCommand('main');

        $this->newBidMessages($bid);
    }

    public function reply_markupBuilder(array $arr, $key = 'name'){
        $keyboard = buttonBuilder($arr, $key);
        $reply_markup = Keyboard::make([
          'keyboard'  =>  $keyboard,
          'resize_keyboard' => true,
          'one_time_keyboard' => false,
        ]);
        return $reply_markup;
    }
}

