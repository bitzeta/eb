<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.06.2016
 * Time: 18:15
 */
namespace Telegram\Bot\Commands;

use App\Models\Bid;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Selection;
use App\Services\BidsService;
use Illuminate\Support\Facades\DB;
use \Telegram\Bot\Actions;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Chat;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class BuyCommand extends Command
{
    protected $name = "buy";
    protected $description = "Купить крипту за $";
    /**
     * @var Chat    $chat
     * @var Message $message
     * @var User    $from
     * @var \App\User $user
     * @var string  $text
     */
    protected $chat, $message, $from, $user, $text;

    /** @var Selection */
    protected $selection;

    /** @var BidsService */
    protected $bidsService;

    protected $is_fiat = false;

    protected $bidType = Bid::TYPE_SELL;    //хотим купить - ишем у тех кто продает / и наоборот

    protected $priceByBidCurrency = false;

    const STEP_SELECT_CURR = 1, STEP_SELECTED_CURR = 2, STEP_INPUT_AMOUNT = 0x2a, STEP_SELECTED_PAY_SYS = 3,
        STEP_SELECTED_BID_PRE = 0x3a, STEP_SELECTED_BID = 4, STEP_GOT_DETAILS_OR_WALLET = 5, STEP_GOT_AMOUNT = 6;

    /**
     * For callback cmdName_op_page_selection_value.
     *              cmdName_op_page_selection [value optional]
     *              cmdName_op_page [value optional]  if selection is not present => create
     *              cmdName_op  [page=1]
     *
     * @param $arguments
     */
    public function handle($arguments)
    {
        $this->bidsService = $bidsService = new BidsService($this->getTelegram());
        $this->replyWithChatAction(['action'=>Actions::TYPING]);
        $update = $this->getUpdate();
        /**
         * @var Chat        $chat
         * @var Message     $message
         * @var User        $from
         * @var \App\User   $user
         * @var string      $text
         */
        list($chat, $message, $from, $user, $text) = init_command_params($update);
        $this->user = $user;

        $user_id = $user ? $user->id : null;

        if($chat && $user_id!=$chat->getId()){
            global $bNotChangeCommand;
            $bNotChangeCommand = true;
            $reply_markup = Keyboard::hide();
            $this->replyWithMessage([
              'text' => 'Данная команда работает только тет-а-тет (1 на 1)',
              'reply_markup' => $reply_markup,
            ]);
            return;
        }


        if (!$user) {
            if ($user_id==$chat->getId()) {
                $this->replyWithMessage([
                  'text' =>  'Пожалуйста закончите регистрацию... Используйте /start'
                ]);
            }
            if (isset($update['callback_query'])) {
                $this->getTelegram()->answerCallbackQuery([
                  'callback_query_id' => $update['callback_query']['id'],
                  'text' => 'Пожалуйста закончите регистрацию... Используйте /start'
                ]);
            }
            return;
        }

        $selection = isset($arguments['step']) && $arguments['step'] >= 5 ?
          Selection::find($user->additional_int) : Selection::where('user_id', $user_id)->orderBy('created_at', 'desc')->first();

        if (!$selection)
            $selection = Selection::create(['user_id' => $user_id]);

        $this->selection = $selection;

        $this->priceByBidCurrency = cache()->get('priceByBidCurrency_' . $selection->id, false);

        if (isset($update['callback_query']) && is_array($arguments)) {
            $op = 1; $page = 0; $selectionId = null; $value = null; $additional = null;
            if (sizeof($arguments) == 5)
                list($op, $page, $selectionId, $value, $additional) = $arguments;
            if (sizeof($arguments) == 4)
                list($op, $page, $selectionId, $value) = $arguments;
            elseif (sizeof($arguments) == 3)
                list($op, $page, $selectionId) = $arguments;
            elseif (sizeof($arguments) == 2)
                list($op, $page) = $arguments;
            elseif (sizeof($arguments) == 1)
                list($op) = $arguments;

            $selection = Selection::find($selectionId);

            if (!$selection) {
                return;
            }

            $message_id = $update['callback_query']['message']['message_id']; //$message ? $message->getMessageId() : null;

            if ($op == self::STEP_SELECT_CURR) { //select currency
                $this->selectCurrencyMessage($selection, $message_id);
            }elseif ($op == self::STEP_SELECTED_CURR && $value) {  //currency got, select payment system
                $currency = Currency::find($value);
                if (!$currency) return;
                $selection->currency_id = $currency->id;
                $selection->save();
                if (!$this->is_fiat) {
                    $this->whatAmountMessage($user, $selection->currency, $message_id);
                    //$this->bidsMessage($selection, $page, $message_id);
                    $user->setStateCommand($this->name, self::STEP_INPUT_AMOUNT, $selection->id);
                } else {    //for fiat show payment_system selection
                    $this->paymentSystemMessage($selection, $message_id);   //paymentSystems OR show amount (in crypto mode , !$this->is_fiat)
                }
            }elseif ($op == self::STEP_SELECTED_PAY_SYS && $value) {  //paymentSystem got, select bid
                $paymentSystem = PaymentSystem::find($value);
                if (!$paymentSystem && $this->is_fiat) {
                    return;
                }  //for fiat payment system must be setup
                if ($paymentSystem) {
                    $selection->payment_system_id = $paymentSystem->id;
                }
                $selection->save();
                if ($additional) {
                    $this->priceByBidCurrency = $additional;
                    cache()->set('priceByBidCurrency_'.$selection->id, $additional);
                }
                $this->bidsMessage($selection, $page, $message_id);
            }elseif ($op == self::STEP_SELECTED_BID_PRE && $value) {
                $bid = Bid::find($value);
                if (!$bid || $bid->status != Bid::STATUS_OPEN) {
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'К сожалению сделка уже не является открытой.'
                    ]);
                    return;
                }
                $this->preBidMessage($selection, $bid, $message_id);
            }elseif ($op == self::STEP_SELECTED_BID && $value) {  //bid got, to fill wallet
                $bid = Bid::find($value);
                if (!$bid || $bid->status != Bid::STATUS_OPEN) {
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'К сожалению сделка уже не является открытой.'
                    ]);
                    return;
                }
                if (($bid && $bid->is_fixed_price && ($bid->amount_total > $user->amount)) && !($this->is_fiat && $this->bidType == Bid::TYPE_SELL)) {
                    $this->getTelegram()->answerCallbackQuery([
                      'callback_query_id' => $update['callback_query']['id'],
                      'text' => 'У вас недостаточно $ для этой сделки.'
                    ]);
                    return;
                }
                $selection->bid_id = $bid->id;
                $selection->save();
                $user->setStateCommand($this->name, $op + 1, $selection->id);
                $keyboard = [];
                $keyboard[] = [
                  [
                    'text' => trans('menu.to_menu')
                  ]
                ];
                if (!$bid->isNeedsWalletBidMessage()) {
                    $keyboard[] = [
                      [
                        'text' => trans('buy.no_comment')
                      ]
                    ];
                }
                $this->replyWithMessage([
                  'text'  => 'Сделка выбрана. ' .
                        ($bid->isNeedsWalletBidMessage() ? 'Введите кошелек, куда ожидать перевод.' : '' ) . ' Можно добавить комментарий.' . "\r\n" .
                        "Администрация не несет ответственности за ошибки в реквизитах, проверяйте внимательно.",
                  'reply_markup'  => Keyboard::make([
                    'keyboard'  => $keyboard,
                    'resize_keyboard'   => true,
                  ])
                ]);
            }

        } elseif (
          is_array($arguments) && isset($arguments['step']) &&
          ($arguments['step'] >= self::STEP_GOT_DETAILS_OR_WALLET || $arguments['step'] == self::STEP_INPUT_AMOUNT)
        ) {
            $step = $arguments['step'];

            if ($text == trans('menu.to_menu')) {
                $user->setStateCommand('main');
                $this->triggerCommand('main');
                return;
            }

            if ($step == self::STEP_INPUT_AMOUNT && mb_strlen($text) > 0) {
                $text = str_replace(" ", "", $text);
                $text = str_replace(",", ".", $text);
                $amount = floatval($text);

                if ($amount <= 0) {
                    $this->replyWithMessage([
                        'text'  => 'Не верная сумма. Введите сумму или вернуться => /main',
                        'reply_markup'  => Keyboard::hide()
                    ]);
                    return;
                }

                $selection->amount = $amount;
                $selection->save();
                $user->setStateCommand('main');
                $this->bidsMessage($selection);
                return;
            }

            $bid = Bid::find($selection->bid_id);
            if (!$bid || $bid->status != Bid::STATUS_OPEN) {
                $this->replyWithMessage([
                  'text'  => 'К сожалению сделка уже не является открытой.'
                ]);
                return;
            }

            if ($step == self::STEP_GOT_DETAILS_OR_WALLET && mb_strlen($text) > 0) {
                $selection->details = $text;
                $selection->save();

                if ($bid->is_fixed_price) {
                    $details = [];
                    $amount = $bidsService->priceService->calc(
                      $bid->currency->shortcode,
                      $bid->amount_total,
                      config('pricing.associated_currency'),
                      $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',
                      $details,
                      $bid->commission_creator_value
                    );

                    $this->createDeal($selection, $bid, $amount);
                    $user->setStateCommand('main');
                    $this->triggerCommand('main');
                } else {
                    $user->setStateCommand($this->name, $step+1, $selection->id);
                    $max_amount = ($bid->amount_max < $bid->amount_total ? $bid->amount_max : $bid->amount_total);
                    $max_amount = (($this->bidType == Bid::TYPE_BUY && $this->is_fiat) || ($this->bidType == Bid::TYPE_SELL && !$this->is_fiat)) && $user->amount < $max_amount ? $user->amount : $max_amount;

                    if ($this->bidType == Bid::TYPE_SELL && !$this->is_fiat) { //покупаем крипту у тех кто продает, получаем сколько максимально можно долл
                        $details = [];
                        $max_amount = $bidsService->priceService->calc(
                          $bid->currency->shortcode,
                          $max_amount,
                          config('pricing.associated_currency'),
                          $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',
                          $details,
                          $bid->commission_creator_value
                        );
                        $max_amount = $user->amount < $max_amount ? $user->amount : $max_amount;
                    }

                    //если продаем крипту то надо больше 2 знаков после запятой
                    $humanized = !$this->is_fiat && $this->bidType == Bid::TYPE_SELL ? number_format($max_amount, 8) : currencyHumanized(true, $max_amount);
                    if (floatval($humanized) > $max_amount) {
                        if (!$this->is_fiat) {
                            $max_amount -= 0.00000001;
                        } else {
                            $max_amount -= 0.01;
                        }
                    }
                    $humanized = !$this->is_fiat ? number_format($max_amount, 8, '.', ' ') : currencyHumanized(true, $max_amount);

                    $this->replyWithMessage([
                      'text' => 'Введите сколько ' . ($bid->isNeedsWalletBidMessage() ? ' готовы отдать $' : 'продаете ' . $bid->currency->shortcode) . '? ' . '(' . $bid->amount_min . ' - ' . ($bid->amount_total < $bid->amount_max ? $bid->amount_total : $bid->amount_max) . ') ' . ($bid->isNeedsWalletBidMessage() ? 'за ' . $bid->currency->symbol : 'за $') . "\r\n" .
                        $this->getCryptCourse($bid->currency, $bid) ."\r\n" .
                        "У вас на счету " . $user->amount . " $. \r\n" .
                        "Внимание! Окончательный курс будет просчитан в момент ввода этой суммы, после ввода суммы будет отправлено уведомление создателю предложения с запросом подтвердить сделку.",
                      'reply_markup'  => Keyboard::make([
                        'keyboard'  => [
                          [
                            [
                              'text' => $humanized,
                            ]
                          ],
                          [
                            [
                              'text' => trans('menu.to_menu')
                            ]
                          ]
                        ],
                        'resize_keyboard'   => true,
                      ])
                    ]);
                }
            } elseif ($step == self::STEP_GOT_AMOUNT && mb_strlen($text) > 0) {
                $text = str_replace(" ", "", $text);
                $text = str_replace(",", ".", $text);
                $amount = floatval($text);

                //покупаем FIAT/cryp за долл у тех кто продает
                //nope

                // или продаем крипту/фиат тем кто покупает (узнаем сколько это в долл)
                if ($this->bidType == Bid::TYPE_BUY || ($this->bidType == Bid::TYPE_SELL && $this->is_fiat)) {
                    $details = [];
                    $amount = $bidsService->priceService->calc(
                      $bid->currency->shortcode,
                      $amount,
                      config('pricing.associated_currency'),
                      $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',
                      $details,
                      $bid->commission_creator_value
                    );
                }

                if ($this->createDeal($selection, $bid, $amount)) {
                    $user->setStateCommand('main');
                    $this->triggerCommand('main');
                }
            }
        } else {
            $selection = Selection::create(['user_id' => $user_id]);
            $this->selection = $selection;
            $this->selectCurrencyMessage($selection);
            $user->setStateCommand('main');
        }
    }

    protected function selectCurrencyMessage(Selection $selection, $message_id = null)
    {
        $currencies = Currency::where('allowed', true)->where('is_fiat', $this->is_fiat)->get();

        $inline_keyboard = [];

        $iterator = 0; $items = [];
        $inline_keyboard[] =
        [
          [
            'text'  => 'добавить сделку',
            'callback_data' => $this->is_fiat ? ($this->bidType != Bid::TYPE_SELL ? 'addsellfiat' : 'addbuyfiat') : ($this->bidType != Bid::TYPE_SELL ? '/addsellcrypt' : '/addbuycrypt')
          ]
        ];
        foreach ($currencies as $currency) {
            if (sizeof($items) > 0 && $iterator%3 == 0) {
                $inline_keyboard[] = $items;
                $items = [];
                $iterator = 0;
            }
            $items[] = [
              'text'          => $currency->shortcode,
              'callback_data' => $this->name . '_' . self::STEP_SELECTED_CURR . '_' . 0 . '_' . $selection->id . '_' . $currency->id,
            ];
            $iterator++;
        }
        if (sizeof($items) > 0) $inline_keyboard[] = $items;

        $keyboard = [
        ];

        $reply_markup = Keyboard::make(compact('inline_keyboard', 'keyboard'));

        $params = [
          'chat_id'       => $this->getUpdate()->getChat()->getId(),
          'text'          => $this->is_fiat ?
                'Выберите за какую валюту Вы хотите ' . ($this->bidType == Bid::TYPE_SELL ? 'купить' : 'продать') . ' $' . "\r\n"
                // . 'Добавить сделку ' . ($this->bidType != Bid::TYPE_SELL ? '/addsellfiat' : '/addbuyfiat')
            :
                'Выберите какую валюту Вы хотите ' . ($this->bidType == Bid::TYPE_SELL ? 'купить' : 'продать') . '' . "\r\n"
                // . 'Добавить сделку ' . ($this->bidType != Bid::TYPE_SELL ? '/addsellcrypt' : '/addbuycrypt')
          ,
          'reply_markup'  => $reply_markup,
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }
    }

    protected function paymentSystemMessage(Selection $selection, $message_id = null)
    {
        $payIds     = DB::select(
          'SELECT DISTINCT(`payment_system_id`) as id, COUNT(`payment_system_id`) as cnt, name
                  FROM `bids`
                   INNER JOIN payment_systems ON payment_systems.id = payment_system_id
                  WHERE bids.currency_id = ' . $selection->currency_id . ' AND bids.status = \'' . Bid::STATUS_OPEN . '\' AND bids.type = \'' . $this->bidType . '\'
                  GROUP BY `payment_system_id`'
        );

        $inline_keyboard = [];
        foreach ($payIds as &$item) {
            $arr = (array)$item;
            $inline_keyboard[] = [
              [
                'text'          =>  $arr['name'] . '(' . $arr['cnt'] . ')',
                'callback_data' =>  $this->name . '_' . self::STEP_SELECTED_PAY_SYS . '_' . 0 . '_' . $selection->id . '_' . $arr['id'],
              ],
            ];
        }

        $inline_keyboard [] = [
          [
            'text'          => trans('menu.back'),
            'callback_data' => $this->name . '_' . self::STEP_SELECT_CURR . '_' . 0 . '_' . $selection->id,
          ],
        ];

        $params = [
          'chat_id'       => $this->getUpdate()->getChat()->getId(),
          'text' => 'Выберите способ оплаты',
          'reply_markup'  => Keyboard::make(compact('inline_keyboard')),
        ];

        if (!$this->is_fiat) {
            $inline_keyboard = [];

            $inline_keyboard [] = [
              [
                'text'          => trans('menu.update'),
                'callback_data' => $this->name . '_' . self::STEP_SELECTED_CURR . '_' . 0 . '_' . $selection->id . '_' . $selection->currency_id,
              ],
            ];
            $inline_keyboard [] = [
              [
                'text'          => trans('buy.next'),
                'callback_data' => $this->name . '_' . self::STEP_SELECTED_PAY_SYS . '_' . 0 . '_' . $selection->id . '_null',
              ],
            ];



            $params = [
              'chat_id'   => $this->getUpdate()->getChat()->getId(),
              'text'      =>  $this->getCryptCourse($selection->currency) . "\r\n" .
                'У вас на счету ' . $selection->user->amount . " $. \r\n" .
                "Пополнить - /accbuy \r\n" .
                "date: " . date('Y-m-d H:i:s') . " [" . str_random(4) . "]",
              'reply_markup'  => Keyboard::make(compact('inline_keyboard')),
            ];
        }

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }
        return;
    }

    /**
     * Сколько крипта в долларах или солько доллар в фиатах.
     *
     * @param Currency $currencyFrom
     * @param Bid|null $bid
     *
     * @return string
     * @throws \Exception
     */
    protected function getCryptCourse(Currency $currencyFrom, Bid $bid = null)
    {
        $details        = [];
        $amount         = $this->bidsService->priceService->calc(
          $currencyFrom->is_fiat ? config('pricing.associated_currency') : $currencyFrom->shortcode,
          1,
          !$currencyFrom->is_fiat ? config('pricing.associated_currency') : $currencyFrom->shortcode,
          $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
          $details,
          $bid ? $bid->commission_creator_value : 0
        );

        return "Курс (bitfin) " . ($currencyFrom->shortcode . '/' . config('pricing.associated_currency')) .
          ' ' . currencyHumanized(1, $amount) . ($bid ? ' c учетом процента [' . $bid->commission_creator_value . '%] от курса' : '');
    }

    // protected function bidShortDetails(Bid $bid)
    // {
    //     $return = $bid->currency->shortcode . ' ';
    //     $details = [];
    //
    //     if ($bid->is_fixed_price) {
    //         ;
    //     } else {
    //         $return .= '(';
    //         $return .= $this->bidsService->priceService->calc(
    //             config('pricing.associated_currency'),
    //             $bid->amount_min,
    //             $bid->currency->shortcode,
    //             $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
    //             $details
    //           );
    //         $return .= '-';
    //         $return .= $this->bidsService->priceService->calc(
    //           config('pricing.associated_currency'),
    //           $bid->amount_max,
    //           $bid->currency->shortcode,
    //           $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
    //           $details
    //         );
    //         $return .= ')';
    //     }
    // }

    protected function whatAmountMessage(\App\User $user, Currency $currency, $message_id = null)
    {
        $params = [
          'chat_id'   => $user->chat_id,
          'text'      => "У вас на счету " . $user->amount . "$ \r\n" .
            'Сколько готовы ' . (isNeedsWalletBidMessage($this->bidType, $this->is_fiat) ? 'отдать $ за ' : 'продать') . ' ' . $currency->shortcode . ' ?',
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }
    }

    protected function bidsMessage(Selection $selection, $page = 0, $message_id = null)
    {
        if ($page < 0) return;
        $limit = 10;
        $offset= $limit * $page;

        $amount = $selection->amount;
        if ($amount > 0 && isNeedsWalletBidMessage($this->bidType, $this->is_fiat)) {  //если покупает то надо пересчитать по курсу битфина в текущую валюту
            $details = [];
            $amount = $this->bidsService->priceService->calc(
              config('pricing.associated_currency'),
              $amount,
              $selection->currency->shortcode,
              'ask',
              $details
            );
        }

        $bids = Bid::where([
          ['currency_id', '=', $selection->currency_id],
          ['payment_system_id', '=', $selection->payment_system_id],
          ['status',      '=', Bid::STATUS_OPEN],
          ['type',        '=', $this->bidType]
        ])
          ->where(function (\Illuminate\Database\Eloquent\Builder $query) use ($amount) {
              if ($amount > 0) {
                  $query->where([
                    ['is_fixed_price', '=', true],
                    ['amount_total', '=', $amount],
                  ])
                    ->orWhere([
                      ['is_fixed_price', '=', false],
                      ['amount_min', '<=', $amount],
                      ['amount_max', '>=', $amount]
                    ]);
              }
          })
          ->with('currency')
          ->orderBy('commission_creator_value', 'ASC')
          ->limit(10)
          ->offset($offset)
            ->get();

        // $this->telegram->sendMessage([
        //   'chat_id'       => $this->getUpdate()->getChat()->getId(),
        //   'text' => $bids->getQuery()->toSql(),
        // ]);
        //
        // $bids = $bids->get();

        $price = function (Bid $bid) {
            $details        = [];
            $rate = $this->bidsService->priceService->calc(
              $bid->currency->shortcode,
              1,
              config('pricing.associated_currency'),
              $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
              $details,
              $bid->commission_creator_value
            );
            // $amount_total         = $this->bidsService->priceService->calc(
            //   config('pricing.associated_currency'),
            //   $bid->amount_total,
            //   $bid->currency->shortcode,
            //   $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
            //   $details,
            //   $bid->commission_creator_value
            // );
            // $amount_min         = $this->bidsService->priceService->calc(
            //   config('pricing.associated_currency'),
            //   $bid->amount_min,
            //   $bid->currency->shortcode,
            //   $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
            //   $details,
            //   $bid->commission_creator_value
            // );
            // $amount_max         = $this->bidsService->priceService->calc(
            //   config('pricing.associated_currency'),
            //   $bid->amount_max,
            //   $bid->currency->shortcode,
            //   $this->bidType == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель
            //   $details,
            //   $bid->commission_creator_value
            // );

            return number_format($rate, 2) . ' $' .
              ' (' .
              ( $bid->is_fixed_price ?
                currencyHumanized(true, $bid->amount_total) : (currencyHumanized(true, $bid->amount_min) . ' - ' . currencyHumanized(true, $bid->amount_max))
              ) . ' ' . $bid->currency->shortcode . ' ) ' .
              str_pad($bid->commission_creator_value . '%', 6, " ", STR_PAD_LEFT);
        };

        $inline_keyboard = [];
        /** @var Bid $bid */
        foreach ($bids as $bid) {
            $inline_keyboard[] = [
              [
                'text'      =>
                //$bid->currency->shortcode . ' ' .
                  $price($bid),
                'callback_data'=> $this->name . '_' . self::STEP_SELECTED_BID_PRE . '_' . $page . '_' . $selection->id . '_' . $bid->id,
              ]
            ];
        }

        $inline_keyboard[] = [
          [
            'text'      => '<< ' . ($page+1),
            'callback_data'=> $this->name . '_' . self::STEP_SELECTED_PAY_SYS . '_' . ($page-1) . '_' . $selection->id . '_' . ($selection->paymentSystem ? $selection->payment_system_id : 'null'),
          ],
          [
            'text'      => 'Назад',
            'callback_data'=> $this->name . '_' . self::STEP_SELECT_CURR . '_' . 0 . '_' . $selection->id . '_' . $selection->currency_id,
          ],
          [
            'text'      => ($page+2) . ' >>',
            'callback_data'=> $this->name . '_' . self::STEP_SELECTED_PAY_SYS . '_' . ($page+1) . '_' . $selection->id . '_' . ($selection->paymentSystem ? $selection->payment_system_id : 'null'),
          ],
        ];

        $params = [
          'chat_id'       => $this->getUpdate()->getChat()->getId(),
          'text' => 'Список сделок, ' . ($this->bidType == Bid::TYPE_SELL ? 'купить' : 'продать') . ' ' . ($this->is_fiat ? '$ за ' : '') . $selection->currency->shortcode . (!$this->is_fiat ? ' за $ ' : '') . "\r\n" .
            $this->getCryptCourse($selection->currency) . " (" . rand(1, 10000) . ")" . "\r\n",
          'reply_markup'  => Keyboard::make(compact('inline_keyboard')),
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }
    }

    protected function preBidMessage(Selection $selection, Bid $bid, $message_id = null)
    {
        $inline_keyboard = [];

        $inline_keyboard[] = [
          [
            'text'      => 'Назад',
            'callback_data'=> $this->name . '_' . self::STEP_SELECTED_PAY_SYS . '_' . 0 . '_' . $selection->id . '_' . ($selection->paymentSystem ? $selection->payment_system_id : 'null'),
          ],
        ];

        if ($bid->isAvailable($this->user, $this->bidsService->priceService) || ($this->is_fiat && $this->bidType == Bid::TYPE_SELL) || (!$this->is_fiat && $this->bidType == Bid::TYPE_BUY)) {
            $inline_keyboard = [];
            $inline_keyboard[] = [
              [
                'text'      => 'Назад',
                'callback_data'=> $this->name . '_' . self::STEP_SELECTED_PAY_SYS . '_' . 0 . '_' . $selection->id . '_' . ($selection->paymentSystem ? $selection->payment_system_id : 'null'),
              ],
              [
                'text'      => 'Начать сделку',
                'callback_data'=> $this->name . '_' . self::STEP_SELECTED_BID . '_' . 0 . '_' . $selection->id . '_' . $bid->id,
              ],
            ];
        }

        $params = [
          'chat_id'       => $this->getUpdate()->getChat()->getId(),
          'text' => BidCommand::bidMessage($selection->user, $selection->paymentSystem, $bid, $this->bidType, $this->is_fiat),
          'reply_markup'  => Keyboard::make(compact('inline_keyboard')),
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }
    }

    /**
     * Создать сделку
     *
     * @param Bid       $bid
     * @param           $amount_usd кол-во в долларах
     * @param \App\User $user
     * @param Selection $selection
     *
     * @throws \Exception
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    protected function createDeal(Selection $selection, Bid $bid, $amount_usd)
    {
        $deal = $this->bidsService->createDeal($selection->user, $bid, $amount_usd, 0);

        if (is_array($deal) || $deal == null) {
            $this->replyWithMessage([
                'text'  => isset($deal['error']) ? $deal['error'] : 'Невозможно создать сделку'
            ]);
            return false;
        }

        $deal->ask_details = $selection->details;
        $deal->save();

        $selection->deal_id = $deal->id;
        $selection->save();

        $this->bidsService->messageDeal($deal);

        $this->telegram->sendMessage([
          'chat_id' =>  $selection->user->chat_id,
          'text'    =>  'Запрос /deal_' . $deal->token . ' отправлен владельцу заявки, после утверждения Вам прийдет уведомление.'
        ]);

        return true;
    }

    public function reply_markupBuilder(array $arr, $key = 'name'){
        $keyboard = buttonBuilder($arr, $key);
        $reply_markup = Keyboard::make([
          'keyboard'  =>  $keyboard,
          'resize_keyboard' => true,
          'one_time_keyboard' => false,
        ]);
        return $reply_markup;
    }
}

