<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 12.07.18
 * Time: 19:46
 */

namespace App\Services;


use App\Bitfinex;
use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\Selection;
use App\Models\Transaction;
use App\User;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;

class PriceService
{
    /** @var Bitfinex $bitfinex */
    protected $bitfinex;

    /** @var array */
    protected $bitfinexRates = [];  //key shortcodes => value

    public function __construct()
    {
        $this->bitfinex = new Bitfinex(env('BITFINEX_KEY'), env('BITFINEX_SECRET'));
    }

    /**
     * @param        $fromShortCode
     * @param        $fromAmount
     * @param        $toShortCode
     * @param string $type bid|ask
     * @param        $cryptDetails
     *
     * @param int    $percent
     *
     * @return float|int
     * @throws \Exception
     */
    public function calc($fromShortCode, $fromAmount, $toShortCode, $type = 'bid', &$cryptDetails, $percent = 0)
    {
        $fromShortCode = strtoupper($fromShortCode); $toShortCode = strtoupper($toShortCode);
        $usdAmount  = 0;
        $toAmount   = 0;
        $cryptDetails = [];

        if ($this->isFiat($fromShortCode)) {
            $usdAmount = $this->fiat2usd($fromShortCode, $fromAmount, $percent);
        } else {
            $cryptDetails['from'] = $this->crypt2usd($fromShortCode, $fromAmount, $percent);
            $usdAmount = $cryptDetails['from']['calculated'][$type . 'Amount'];
        }

        if ($this->isFiat($toShortCode)) {
            $toAmount = $this->usd2fiat($toShortCode, $usdAmount, $percent);
        } else {
            $cryptDetails['to'] = $this->usd2crypt($toShortCode, $usdAmount, $percent);
            $toAmount = $cryptDetails['to']['calculated'][$type . 'Amount'];
        }

        return $toAmount;
    }

    public function isFiat($shortcode)
    {
        $shortcode = strtoupper($shortcode);

        $curr = $this->getFiatRates()->where('cc', $shortcode)->first();

        return $curr || $shortcode == "UAH";
    }

    public function fiat2usd($fiatShortCode, $fiatAmount, $percent = 0)
    {
        $fiatShortCode = strtoupper($fiatShortCode);

        if ($fiatShortCode == 'USD') return $fiatAmount;

        return $this->fiat2fiat($fiatShortCode, $fiatAmount, "USD", $percent);
    }

    public function fiat2fiat($fiatShortCode, $fiatAmount, $toFiatShortcode, $percent = 0)
    {
        $fiatShortCode = strtoupper($fiatShortCode);

        if ($fiatShortCode == $toFiatShortcode) return $fiatAmount;

        $rate = $this->getFiatRates()->where('cc', $fiatShortCode)->first();
        $mainRate = $this->getFiatRates()->where('cc', $toFiatShortcode)->first();

        if (!$rate && $fiatShortCode != 'UAH') throw new \Exception('Fiat is not found.');

        $mainRate = $mainRate->rate + ($mainRate->rate * $percent / 100);
        //!=UAH привести сколько будет в гривнах
        $uahAmount = $fiatShortCode != "UAH" ? $rate->rate * $fiatAmount : $fiatAmount;

        //привести сколько будет в $toFiatShortcode
        $usdAmount = $uahAmount / $mainRate;

        return $usdAmount;
    }

    public function usd2fiat($fiatShortCode, $usdAmount, $percent = 0)
    {
        $fiatShortCode = strtoupper($fiatShortCode);

        if ($fiatShortCode == 'USD') return $usdAmount;

        $rate = $this->getFiatRates()->where('cc', $fiatShortCode)->first();
        $mainRate = $this->getFiatRates()->where('cc', config('pricing.associated_currency'))->first();

        if (!$rate && $fiatShortCode != 'UAH') throw new \Exception('Fiat is not found.');

        $mainRate = $mainRate->rate + ($mainRate->rate * $percent / 100);

        //привести сколько будет в гривнах
        $uahAmount = $mainRate * $usdAmount;

        $otherRate  = $rate ? $rate->rate + ($rate->rate * $percent / 100) : 0;
        $otherAmount= $rate ? $uahAmount / $otherRate : 0;

        return $fiatShortCode != "UAH" && $rate ? $otherAmount : $uahAmount;
    }

    public function usd2crypt($cryptShortCode, $usdAmount, $percent = 0)
    {
        $cryptShortCode = strtoupper($cryptShortCode);

        if ($cryptShortCode == 'USDT') {
            return [
              'bid' => 1,
              'ask' => 1,
              'calculated'  => [
                'bidAmount' => $usdAmount / (1 + (1 * $percent / 100)),
                'askAmount' => $usdAmount / (1 + (1 * $percent / 100)),
              ]
            ];
        }

        $stats = null;
        if (isset($this->bitfinexRates[$cryptShortCode . 'USD'])) {
            $stats = $this->bitfinexRates[$cryptShortCode . 'USD'];
        } else {
            $stats = $this->bitfinex->get_ticker($cryptShortCode.'USD');
        }

        if (!is_array($stats) || !isset($stats['bid']) || !isset($stats['ask'])) {
            throw new \Exception('Bitfinex is not work for symbol ' . $cryptShortCode . 'USD, ' . var_export($stats, true));
        }

        $this->bitfinexRates[$cryptShortCode . 'USD'] = $stats;

        return array_merge($stats, [
            'calculated'    => [
                'bidAmount' => $usdAmount / ($stats['bid'] + ($stats['bid'] * $percent / 100)),
                'askAmount' => $usdAmount / ($stats['ask'] + ($stats['ask'] * $percent / 100)),
            ]
        ]);
    }

    public function crypt2usd($cryptShortCode, $cryptAmount, $percent = 0)
    {
        $cryptShortCode = strtoupper($cryptShortCode);

        if ($cryptShortCode == 'USDT') {
            return [
              'bid' => 1,
              'ask' => 1,
              'calculated'  => [
                'bidAmount' => $cryptAmount * (1 + (1 * $percent / 100)),
                'askAmount' => $cryptAmount * (1 + (1 * $percent / 100)),
              ]
            ];
        }

        $stats = null;
        if (isset($this->bitfinexRates[$cryptShortCode . 'USD'])) {
            $stats = $this->bitfinexRates[$cryptShortCode . 'USD'];
        } else {
            $stats = $this->bitfinex->get_ticker($cryptShortCode.'USD');
        }

        if (!is_array($stats) || !isset($stats['bid']) || !isset($stats['ask'])) {
            throw new \Exception('Bitfinex is not work for symbol ' . $cryptShortCode . 'USD, ' . var_export($stats, true));
        }

        $this->bitfinexRates[$cryptShortCode . 'USD'] = $stats;

        return array_merge($stats, [
            'calculated'  => [
                'bidAmount' => $cryptAmount * ($stats['bid'] + ($stats['bid'] * $percent / 100)),
                'askAmount' => $cryptAmount * ($stats['ask'] + ($stats['ask'] * $percent / 100)),
            ]
        ]);
    }

    /**
     * TODO: решить что куда брать
     *
     * @param $cryptShortCode
     * @param $cryptAmount
     * @param $toCryptShortCode
     *
     * @return array
     * @throws \Exception
     */
    public function crypt2crypt($cryptShortCode, $cryptAmount, $toCryptShortCode, $percent = 0)
    {
        $rate1 = $this->crypt2usd($cryptShortCode, $cryptAmount, $percent);

        $calculated = $rate1['calculated'];

        $bid = $this->usd2crypt($toCryptShortCode, $calculated['bidAmount'], $percent);
        $ask = $this->usd2crypt($toCryptShortCode, $calculated['askAmount'], $percent);

        return compact('bid', 'ask');
    }

    public function getFiatRates()
    {
        $cache_key = 'fiat_rates_' . date('Ymd');
        $rates = \Cache::get($cache_key);

        if ($rates)
            return collect($rates);

        try {
            $rates = (array)json_decode(file_get_contents("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json&date=" . date('Ymd')));
            \Cache::set($cache_key, $rates, new \DateInterval('P1D'));
        } catch (\Exception $exception) {
            $rates = [];
        }

        return collect($rates);
    }

    public function addTransaction(User $userFrom = null, User $userTo = null, $amount, Bid $bid = null, Deal $deal = null)
    {
        $transaction = Transaction::create([
            'user_id_from'  => $userFrom ? $userFrom->id : null,
            'user_id_to'    => $userTo ? $userTo->id : null,
            'amount'        => $amount,
            'bid_id'        => $bid     ? $bid->id : null,
            'deal_id'       => $deal    ? $deal->id : null,
        ]);

        $transaction->refresh();

        return $transaction;
    }

    public function calcAmount(User $user)
    {
        $transactions = Transaction::where('user_id_from', $user->id)
            ->orWhere('user_id_to', $user->id)
            ->get();

        $total = "0";

        foreach ($transactions as $transaction) {
            if ($transaction->user_id_from == null && $transaction->user_id_to == $user->id)
                $total = bcadd($total, $transaction->amount, 10);
            elseif ($transaction->user_id_from == $user->id && $transaction->user_id_to != $transaction->user_id_from) {
                $total = bcsub($total, $transaction->amount, 10);
            }
        }

        return $total;
    }
}