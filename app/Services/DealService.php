<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 12.07.18
 * Time: 19:46
 */

namespace App\Services;


use App\Bitfinex;
use App\Models\Bid;
use App\Models\Deal;
use App\Models\Selection;
use App\User;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Работа с сделками и предложениями, сообщения.
 * Если $bid->currency->is_fiat то всё происходит наоборот для пользователя (покупают уже не понету а крипту за монеты, продают уже не монету а крипту к монете).
 *
 * Class BidsService
 *
 * @package App\Services
 */
class DealService
{
    /** @var Api */
    protected $telegram;

    /** @var BidsService */
    public $bidsService;

    public function __construct(Api $telegram, BidsService $bidsService)
    {
        $this->telegram = $telegram;
        $this->bidsService = $bidsService;
    }

    /**
     * Соискатель создает предложение по сделке.
     * Если соискатель готов продать то сколько валюты той которой создатель готов купить. Затем получаем кол-во в долларах, которое снимем с создателя.
     * Если соискатель готов купить то сколько валюты той которой создатель готов продать. Затем получаем кол-во в долларах, которое снимем с соискателя.
     *
     * @param Bid $bid
     * @param     $amount_bid
     * @param     $userId
     *
     * @return Deal|null
     * @throws \Exception
     */
    public function createDeal(User $user, Bid $bid, $amount_usd, $amount_bid, $details)
    {
        if ($amount_usd == 0 || $amount_bid == 0) {
            throw new \Exception('Not all amounts are pass to createDeal function');
        }

        if ($bid->amount_total < $amount_bid) {
            throw new \Exception('Deal not allowed to create, needs usd: ' . var_export($amount_usd, true) . ', with amount_bid:' . var_export($amount_bid, true) . ', amount_total: ' . var_export($bid->amount_total, true));
        }

        $bid->amount_total = $bid->amount_total - $amount_bid;

        if ($bid->amount_total <= 0 || $bid->amount_total < $bid->amount_min || round($bid->amount_total, 10) <= 0.00000001) {
            $bid->status = Bid::STATUS_EMPTY;
        }

        $symbol = $bid->currency->shortcode . config('pricing.associated_currency');
        $pubSymbol = $bid->currency->shortcode . '/' . config('pricing.associated_currency');

        $type = $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid';
        $symbol_amount = isset($details['to']) && isset($details['to'][$type . 'Amount']) ? $details['to'][$type . 'Amount'] : ($amount_usd / $amount_bid);
        $symbol_timestamp = isset($details['to']) ? $details['to']['timestamp'] : now()->getTimestamp();

        $bid->save();

        $without_commission = $this->bidsService->priceService->calc(
          $bid->currency->shortcode,
          $amount_bid,
          config('pricing.associated_currency'),
          $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель должен получить
          $details
        );

        $commission_creator_amount =$amount_usd - $without_commission;
        //$amount_usd = $amount_usd - $commission_creator_amount;

        $deal = Deal::create([
            'bid_id'    => $bid->id,
            'amount'    => $amount_usd,
            'amount_bid'=> $amount_bid,
            'commission_creator_amount'   => $commission_creator_amount,
            'user_id'   => $user->id,
            'currency_details'  => $details,
            'token'     => str_random(8),
            'symbol'    => $symbol,
            'symbol_details' => $pubSymbol . ' ' . currencyHumanized(true, $symbol_amount) . ' 🕐:' . date('Y-m-d H:i:s', $symbol_timestamp),
            'symbol_amount'=> $symbol_amount,
            'auto_close_minutes'    => 40,
        ]);
        $deal->refresh();

        if ($deal->bid->type == Bid::TYPE_SELL && !$this->blockFunds($user, $deal, $amount_usd)) {
            $this->dealDisallow($deal);
        }

        //TODO:: если сделка не утверждена и висит долго то надо по таймауту закрывать сделки, amount timeout?

        return $deal;
    }

    protected function blockFunds(User $user, Deal $deal, $amount_usd)
    {
        $user->refresh();
        if ($user->amount < $amount_usd) {
            return false;
        }
        $user->amount = bcsub($user->amount, $amount_usd, 10);
        $user->save();

        $this->bidsService->priceService->addTransaction($user, null, $amount_usd, $deal->bid, $deal);

        return true;
    }

    protected function unblockFunds(User $user, Deal $deal, $amount_usd)
    {
        $user->refresh();
        $user->amount = bcadd($user->amount, $amount_usd, 10);
        $user->save();

        $this->bidsService->priceService->addTransaction(null, $user, $amount_usd, $deal->bid, $deal);

        return true;
    }

    protected function isNeedsBlockFunds(Deal $deal) {
        //создатель сделки покупает за долл у   продавца крипты,    заблок долл у создателя сделки
        //создатель сделки продает крипту       покупателю за долл  заблок долл в момент создания предложения

        //создатель сделки покупает за долл у   продавца фиат       заблок долл у создателя сделки
        //создатель сделки продает фиат         покупателю за долл  заблок долл в момент создания предложения
    }

    /**
     * @param User $user
     * @param Bid  $bid
     * @param int  $amount_usd
     * @param int  $amount_bid
     * @param      $details
     *
     * @return array|bool
     * @throws \Exception
     */
    public function checkCreateDeal(User $user, Bid $bid, $amount_usd = 0, $amount_bid = 0, $details)
    {
        if ($amount_usd == 0 || $amount_bid == 0) {
            return [
              'error' =>    'Not all amounts are pass to createDeal function'
            ];
        }

        if ($bid->amount_total < $amount_bid) {
            return [
              'error'   =>  'Deal not allowed to create [check], needs usd: ' . var_export($amount_usd, true) . ', with amount_bid: ' . var_export($amount_bid, true) . ', amount_total: ' . var_export($bid->amount_total, true)
            ];
        }

        $max_amount = ($bid->amount_max > 0 && $bid->amount_max < $bid->amount_total ? $bid->amount_max : $bid->amount_total);
        $min_amount = $bid->is_fixed_price ? $bid->amount_total : $bid->amount_min;
        if ($bid->currency->shortcode != config('pricing.associated_currency')) { //покупаем у тех кто продает, переводим в доллары по текущему курсу
            $details = [];
            $max_amount = $this->bidsService->priceService->calc(
              $bid->currency->shortcode,
              $max_amount,
              config('pricing.associated_currency'),
              $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',
              $details,
              $bid->commission_creator_value
            );
            $min_amount = $this->bidsService->priceService->calc(
              $bid->currency->shortcode,
              $min_amount,
              config('pricing.associated_currency'),
              $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',
              $details,
              $bid->commission_creator_value
            );
        }

        $max_amount = round($max_amount, 10);
        $min_amount = round($min_amount, 10);

        if ($amount_usd <= 0 || $amount_usd > $max_amount) {
            return [
              'error'   =>  'Не верная сумма, или сумма больше доступной, может быть курс уже поменялся? ' . var_export($amount_usd, true) . ' > ' . var_export($max_amount, true)
            ];
        }
        if ($amount_usd <= 0 || $amount_usd < $min_amount) {
            return [
              'error'   =>  'Не верная сумма, или сумма меньше минимально доступной, может быть курс уже поменялся? ' . var_export($amount_usd, true) . ' < ' . var_export($min_amount, true)
            ];
        }

        if (($amount_usd > $user->amount) && $bid->type == Bid::TYPE_SELL) {
            return [
              'error'   =>  'У вас недостаточно на счету $, пополнить можно командой /accbuy.'
            ];
        }

        return true;
    }

    /**
     * Сообщение о сделке по предложению, создателю предложения bid.
     *
     * @param Deal $deal
     * @param null $message_id
     *
     *
     * @param bool $forBidCreator
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function messageDeal(Deal $deal, $message_id = null, $forBidCreator = true)
    {
        $bid = $deal->bid;
        $user = $deal->user;
        $inline_keyboard = [];
        $params = null;

        if ($deal->status == Deal::STATUS_NEW || $deal->status == Deal::STATUS_WAIT) {
            $inline_keyboard = [
              [
                [
                  'text'            => 'подтверждаю сделку',
                  'callback_data'   => 'deal_' . $deal->token . '_allow',
                ],
              ],
              [
                [
                  'text'            => 'предложить меньше',
                  'callback_data'   => 'deal_' . $deal->token . '_smaller',
                ],
              ],
              [
                [
                  'text'            => 'отменить сделку',
                  'callback_data'   => 'deal_' . $deal->token . '_cancel',
                ],
              ],
            ];
            $reply_markup   = Keyboard::make(compact('inline_keyboard'));

            $params = [
              'chat_id'   => $forBidCreator ? $bid->user->chat_id : $deal->user->chat_id,
              'text'      => 'Пользователя заинтересовало Ваше предложение /bid_' . $bid->token . "\r\n".
                ' его встречные условия /deal_' . $deal->token . "\r\n" .
                (
                  $forBidCreator ?
                    ($bid->type == Bid::TYPE_SELL ? 'Вы продаете ' : 'Вы покупаете ')
                    :
                    ($bid->type == Bid::TYPE_SELL ? 'Вам продают ' : 'У вас покупают ')
                ) . "\r\n" .  //для создателя предложения нужно понять он продает или покупает по сделке
                $deal->amount_bid . ' ' . $deal->bid->currency->shortcode . ' на сумму ' . currencyHumanized(1, $deal->amount) . " $ \r\n" .
                ' курс ' . $deal->symbol_details . "\r\n" .
                ' кошелек/комментарий:' . "\r\n" .
                $deal->ask_details . $deal->bid_details . "\r\n",
              'reply_markup'  => $reply_markup,
            ];
        } elseif ($deal->status == Deal::STATUS_DISALLOWED) {
            $params = [
              'chat_id'   => $forBidCreator ? $bid->user->chat_id : $deal->user->chat_id,
              'text'      => 'Сделка /deal_' . $deal->token . ' отменена.',
            ];
        } elseif ($deal->status == Deal::STATUS_ALLOWED) {  //сделка подтверждена владельцем предложения
            $params = [
              'chat_id'   => $forBidCreator ? $bid->user->chat_id : $deal->user->chat_id,
              'text'      => 'Условия утверждены сторонами, Вам прийдет уведомление, когда участник выполнит свои обязательства. ' . "\r\n" .
                    "Детали условий /deal_" . $deal->token,
            ];
        } elseif ($deal->status == Deal::STATUS_SOLD) {
            $params = [
              'chat_id'   => $forBidCreator ? $bid->user->chat_id : $deal->user->chat_id,
              'text'      => 'Сделка /deal_' . $deal->token . ' проведена.',
            ];
        }

        if (is_array($params)) {
            $message = null;
            if ($message_id != null) {
                $params['message_id'] = $message_id;
                try {
                    $message = $this->telegram->editMessageText($params);
                } catch (\Exception $exception) {
                    unset($params['message_id']);
                    $message = $this->telegram->sendMessage($params);
                }
            } else {
                $message = $this->telegram->sendMessage($params);
            }
            return $message;
        }
        return null;
    }

    /**
     * Сделка подтверждена, сообщение
     *
     * @param Deal $deal
     *
     * @param bool $bForBidCreator
     * @param null $message_id
     *
     * @return \Telegram\Bot\Objects\Message | bool
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function dealAllow(Deal $deal, $bForBidCreator = false, $message_id = null)
    {
        if ($deal->status == Deal::STATUS_DISALLOWED) return false;

        $deal->status = Deal::STATUS_ALLOWED;
        $deal->save();

        return $this->dealPreSoldMessage($deal, $bForBidCreator, $message_id);
    }

    /**
     * Сделку приняли, продавец/покупатель указует что выполнил условие, посмотреть что нужно для выполенния условий
     *
     * @param Deal $deal
     * @param bool $bForBidCreator
     * @param null $message_id
     *
     * @return bool|\Telegram\Bot\Objects\Message | null
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function dealPreSoldMessage(Deal $deal, $bForBidCreator = false, $message_id = null)
    {
        if ($deal->status == Deal::STATUS_DISALLOWED) return false;

        $inline_keyboard = [
          [
            [
              'text'            => 'условия выполнил',
              'callback_data'   => 'deal_' . $deal->token . '_preSold',
            ],
          ],
          [
            [
              'text'            => 'отменить сделку',
              'callback_data'   => 'deal_' . $deal->token . '_cancel',
            ],
          ],
        ];
        $reply_markup   = Keyboard::make(compact('inline_keyboard'));

        $params = [
          'chat_id'   => $bForBidCreator ? $deal->bid->user->chat_id : $deal->user->chat_id,
          'text'      => 'Приняты условия сделки /deal_' . $deal->token . "\r\n" .
            "По предложению /bid_" . $deal->token . "\r\n" .
            'Сумма ' . $deal->amount . ' $ за ' . $deal->amount_bid . $deal->bid->currency->shortcode . "\r\n" .
            "Комментарий/кошелек :" . $deal->bid->details . "\r\n" .
            "Вы должны выполнить перевод на сумму " . $deal->amount_bid . ' ' . $deal->bid->currency->symbol . ', что эквивалентно ' . $deal->amount . ' $ по курсу сделки на момент создания сделки.',
          'reply_markup'  => $reply_markup,
        ];

        $message = null;
        if ($message_id != null) {
            $params['message_id'] = $message_id;
            try {
                $message = $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $message = $this->telegram->sendMessage($params);
            }
        } else {
            $message = $this->telegram->sendMessage($params);
        }

        return $message;
    }

    /**
     * Уведомление продавцу/покупателю что покупатель/продавец выполнил условия.
     *
     * @param Deal $deal
     *
     * @param null $message_id_initiator
     * @param null $message_id_accepter
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function dealPreSoldInitiator(Deal $deal, $message_id_initiator = null, $message_id_accepter = null)
    {
        if ($deal->status == Deal::STATUS_DISALLOWED) return;

        $deal->status = Deal::STATUS_PRE_SOLD;
        $deal->save();

        $inline_keyboard = [
          // [
          //   [
          //     'text'            => 'Продавец всё оплатил',
          //     'callback_data'   => 'deal_' . $deal->token . '_sold',
          //   ],
          // ],
          [
            [
              'text'            => 'Открыть спор',
              'callback_data'   => 'deal_' . $deal->token . '_discussion',
            ],
          ],
        ];
        $reply_markup   = Keyboard::make(compact('inline_keyboard'));

        $params = [
          'chat_id'   => !$deal->bid->isNeedsWalletBidMessage() ? $deal->user->chat_id : $deal->bid->user->chat_id,
          'text'      => 'Уведомление о том что Вы выполнили свои обязательства по сделке /deal_' . $deal->token . ' отправлено. ' . "\r\n" .
                "Участник может подтвердить сделку в течении (" . $deal->auto_close_minutes . ") минут, иначе сделка будет считаться успешной автоматом. \r\n"
                // . 'Вы можете завершенную сделку отметьтить кнопками под этим сообщением.'
                ,
          'reply_markup'  => $reply_markup,
        ];

        if ($message_id_initiator != null) {
            $params['message_id'] = $message_id_initiator;
            try {
                $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $this->telegram->sendMessage($params);
            }
        } else {
            $this->telegram->sendMessage($params);
        }

        $inline_keyboard = [
          [
            [
              'text'            => 'Да, всё хорошо',
              'callback_data'   => 'deal_' . $deal->token . '_sold',
            ],
          ],
          [
            [
              'text'            => 'Открыть спор',
              'callback_data'   => 'deal_' . $deal->token . '_discussion',
            ],
          ],
        ];
        $reply_markup   = Keyboard::make(compact('inline_keyboard'));

        $params = [
          'chat_id'   => !$deal->bid->isNeedsWalletBidMessage() ? $deal->bid->user->chat_id : $deal->user->chat_id,
          'text'      => 'Участник указал что выполнил все свои обязательства по сделке /deal_' . $deal->token . "\r\n" .
                "Комментарий предложения: " . ($deal->bid->details) . "\r\n" .
                "Комментарий сделки: " . ($deal->ask_details . $deal->bid_details) . "\r\n" .
                "У вас есть на проверку этого (" . $deal->auto_close_minutes . ") минут. \r\n" .
                'Завершенную сделку отметьте пожалуйста кнопками под этим сообщением.',
          'reply_markup'  => $reply_markup,
        ];

        if ($message_id_accepter != null) {
            $params['message_id'] = $message_id_accepter;
            try {
                $this->telegram->editMessageText($params);
            } catch (\Exception $exception) {
                unset($params['message_id']);
                $this->telegram->sendMessage($params);
            }
        } else {
            $this->telegram->sendMessage($params);
        }
    }

    public function dealSold(Deal $deal)
    {
        if ($deal->status == Deal::STATUS_SOLD || $deal->status == Deal::STATUS_DISALLOWED) return;

        $deal->status = Deal::STATUS_SOLD;
        $deal->save();

        $deal->user->refresh();
        $deal->bid->user->refresh();
        //в момент создания сделки средства уже блокировались в пользу предложения
        if ($deal->bid->type == Bid::TYPE_BUY) {    //если продавалась крипта/фиат создателем deal
            $deal->user->amount = bcadd($deal->user->amount, $deal->amount, 10);
            $this->bidsService->priceService->addTransaction(null, $deal->user, $deal->amount, $deal->bid, $deal);
        } else {    //если покупалась крипта/фиат создателем deal
            $deal->bid->user->amount = bcadd($deal->bid->user->amount, $deal->amount, 10);
            $this->bidsService->priceService->addTransaction(null, $deal->bid->user, $deal->amount, $deal->bid, $deal);
        }
        $deal->bid->user->save();
        $deal->user->save();
    }

    public function dealDisallow(Deal $deal)
    {
        if ($deal->status == Deal::STATUS_DISALLOWED) return;

        $deal->status = Deal::STATUS_DISALLOWED;

        $deal->bid->amount_total = bcadd($deal->bid->amount_total, $deal->amount_bid, 10);

        if ($deal->bid->status == Bid::STATUS_EMPTY && $deal->bid->amount_total > 0 && $deal->bid->amount_total > $deal->bid->amount_min) {
            $deal->bid->status = Bid::STATUS_OPEN;
        }

        if (round($deal->bid->amount_total, 10) <= 0.00000001) {
            $deal->bid->status = Bid::STATUS_EMPTY;
        }

        $deal->bid->save();
        $deal->save();

        if ($deal->bid->type == Bid::TYPE_SELL && !$this->unblockFunds($deal->user, $deal, $deal->amount)) {
            throw new \Exception('cannot unblock funds, deal [' . $deal->id . ']');
        }
    }
}