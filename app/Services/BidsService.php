<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 12.07.18
 * Time: 19:46
 */

namespace App\Services;


use App\Bitfinex;
use App\Models\Bid;
use App\Models\Deal;
use App\Models\Selection;
use App\User;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Работа с сделками и предложениями, сообщения.
 * Если $bid->currency->is_fiat то всё происходит наоборот для пользователя (покупают уже не понету а крипту за монеты, продают уже не монету а крипту к монете).
 *
 * Class BidsService
 *
 * @package App\Services
 */
class BidsService
{
    /** @var Api */
    protected $telegram;

    /** @var Bitfinex */
    protected $bitfinex;

    /** @var PriceService */
    public $priceService;

    /** @var DealService */
    public $dealService;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
        $this->bitfinex = new Bitfinex(env('BITFINEX_KEY'), env('BITFINEX_SECRET'));
        $this->priceService = new PriceService();
        $this->dealService  = new DealService($telegram, $this);
    }

    /**
     * @param Selection $selection
     * @param           $userId
     * @param           $type
     *
     * @return \Illuminate\Database\Eloquent\Model|Bid
     */
    public function createBid(Selection $selection, $userId, $type)
    {
        $bid = Bid::create([
            'token' => str_random(8),
            'currency_id'   => $selection->currency_id,
            'amount_total'  => 0,
            'status'        => Bid::STATUS_PREPARE,
            'commission_creator_value'  => 0,
            'user_id'       => $userId,
            'details'       => $selection->details,
            'type'          => $type,
        ]);

        $selection->bid_id = $bid->id;
        $selection->save();

        return $bid;
    }

    /**
     * При создании сделки указали сколько хотят купить/продать крипту/фиат.
     * Всё покупаем за доллары, которые блокируем на счету
     *
     * @param Bid $bid
     * @param     $amount
     */
    public function setBidAmount(Bid $bid, $amount)
    {
        //buy btc   -usd
        //sell btc  +usd
        //buy fiat  -usd
        //sell fiat +usd

        $is_sell = $bid->type == Bid::TYPE_SELL;
        $is_fiat = $bid->currency->is_fiat;

        if (!$is_sell && !$is_fiat) { //buy cryp => block money ( +cryp -money )
            $bid->user->refresh();
            $bid->user->amount = bcsub($bid->user->amount, $amount, 10);
            $bid->user->save();
            $this->priceService->addTransaction($bid->user, null, $amount, $bid);
        } elseif ($is_sell && !$is_fiat) { //sell cryp => without block ( -cryp +money from deal)
            ;
        } elseif (!$is_sell && $is_fiat) {   //buy fiat => block money ( +fiat -money )
            $bid->user->refresh();
            $bid->user->amount = bcsub($bid->user->amount, $amount, 10);
            $bid->user->save();
            $this->priceService->addTransaction($bid->user, null, $amount, $bid);
        } elseif ($is_sell && $is_fiat) { //sell fiat => without ( -fiat +money from deal )
            ;
        }

        $bid->amount_total = $amount;
        $bid->save();
    }

    public function closeBid(Bid $bid)
    {
        if ($bid->status == Bid::STATUS_CLOSED) return;

        $is_sell = $bid->type == Bid::TYPE_SELL;
        $is_fiat = $bid->currency->is_fiat;
        $amount = $bid->amount_total;

        if (!$is_sell && !$is_fiat) { //buy cryp => block money ( +cryp -money )
            $bid->user->refresh();
            $bid->user->amount = bcadd($bid->user->amount, $amount, 10);
            $bid->user->save();
            $this->priceService->addTransaction(null, $bid->user, $amount, $bid);
        } elseif ($is_sell && !$is_fiat) { //sell cryp => without block ( -cryp +money from deal)
            ;
        } elseif (!$is_sell && $is_fiat) {   //buy fiat => block money ( +fiat -money )
            $bid->user->refresh();
            $bid->user->amount = bcadd($bid->user->amount, $amount, 10);
            $bid->user->save();
            $this->priceService->addTransaction(null, $bid->user, $amount, $bid);
        } elseif ($is_sell && $is_fiat) { //sell fiat => without ( -fiat +money from deal )
            ;
        }

        $bid->status = Bid::STATUS_CLOSED;
        $bid->save();
    }

    /**
     * Bid amount => deal amount.
     *
     * @param Bid $bid
     * @param     $amount_bid
     * @param     $amount_usd
     *
     * @return Deal|array|bool|null
     * @throws \Exception
     */
    public function createDeal(User $user, Bid $bid, $amount_usd = 0, $amount_bid = 0)
    {
        $amount_usd = round($amount_usd, 12);
        $amount_bid = round($amount_bid, 12);
        $details = [];
        $comission_usd = 0; $comission_bid = 0;

        //если есть доллары то переводим сколько это будет в валюте предложения
        //если есть сумма в валюте предложения то считаем сколько то будет в долларах

        if ($amount_usd > 0) {  //покупали напр валюту
            $amount_bid = $this->priceService->calc(
              config('pricing.associated_currency'),
              $amount_usd,
              $bid->currency->shortcode,
              $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель должен получить
              $details,
              $bid->commission_creator_value   //минус коммриссия  TODO:
            );
        } else {    //продают валюту
            $amount_usd = $this->priceService->calc(
              $bid->currency->shortcode,
              $amount_bid,
              config('pricing.associated_currency'),
              $bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',   //продавец продает ? сколько покупатель должен отдать : сколько покупатель должен получить
              $details,
              $bid->commission_creator_value //минус коммиссия  TODO:
            );
        }

        $amount_usd = round($amount_usd, 10);
        $amount_bid = round($amount_bid, 10);

        $check = $this->dealService->checkCreateDeal($user, $bid, $amount_usd, $amount_bid, $details);

        if (is_array($check)) {
            return $check;
        }

        return $this->dealService->createDeal($user, $bid, $amount_usd, $amount_bid, $details);
    }

    /**
     * Сообщение о сделке по предложению, создателю предложения bid.
     *
     * @param Deal $deal
     * @param null $message_id
     *
     *
     * @param bool $forBidCreator
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function messageDeal(Deal $deal, $message_id = null, $forBidCreator = true)
    {
        return $this->dealService->messageDeal($deal, $message_id, $forBidCreator);
    }

    /**
     * Сделка подтверждена, сообщение
     *
     * @param Deal $deal
     *
     * @param bool $bForBidCreator
     * @param null $message_id
     *
     * @return void
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function dealAllow(Deal $deal, $bForBidCreator = false, $message_id = null)
    {
        $this->dealService->dealAllow($deal, $bForBidCreator, $message_id);
    }

    /**
     * Сделку приняли, продавец/покупатель указует что выполнил условие, посмотреть что нужно для выполенния условий
     *
     * @param Deal $deal
     * @param null $message_id
     *
     * @return bool|\Telegram\Bot\Objects\Message | null
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function dealPreSoldMessage(Deal $deal, $message_id = null)
    {
        $this->dealService->dealPreSoldMessage($deal, $message_id);
    }

    /**
     * Уведомление продавцу/покупателю что покупатель/продавец выполнил условия.
     *
     * @param Deal $deal
     *
     * @param null $message_id_initiator
     * @param null $message_id_accepter
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function dealPreSoldInitiator(Deal $deal, $message_id_initiator = null, $message_id_accepter = null)
    {
        $this->dealService->dealPreSoldInitiator($deal, $message_id_initiator, $message_id_accepter);
    }

    public function dealSold(Deal $deal)
    {
        $this->dealService->dealSold($deal);
    }

    public function dealDisallow(Deal $deal)
    {
        $this->dealService->dealDisallow($deal);
    }
}