<?php
use Illuminate\Contracts\Routing\UrlGenerator;


if (! function_exists('calculateTheDistance')){
    // Радиус земли
    define('EARTH_RADIUS', 6372795);

    /*
     * Расстояние между двумя точками
     * $φA, $λA - широта, долгота 1-й точки,
     * $φB, $λB - широта, долгота 2-й точки
     * Написано по мотивам http://gis-lab.info/qa/great-circles.html
     * Михаил Кобзарев <kobzarev@inforos.ru>
     *
     */
    function calculateTheDistance ($φA, $λA, $φB, $λB, $bFormat = true) {

        // перевести координаты в радианы
        $lat1 = $φA * M_PI / 180;
        $lat2 = $φB * M_PI / 180;
        $long1 = $λA * M_PI / 180;
        $long2 = $λB * M_PI / 180;

        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $long2 - $long1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);

        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

        //
        $ad = atan2($y, $x);
        $dist = $ad * EARTH_RADIUS;
        $dist = (int)($dist/1000);
        if($bFormat) $dist = number_format($dist, 0, ' ', ' ').' км.';

        return $dist;
    }
}

if ( !function_exists('downloadAndCropCenterImage') ){
    function downloadAndCropCenterImage ($src, $dest){
        $im = new \Eventviva\ImageResize($src);

        $width = 1;
        $height = 1;
        $imageWidth = $im->getSourceWidth();
        $imageHeight = $im->getSourceHeight();
        if( $imageWidth > $imageHeight ) {
            $height = (int)($imageHeight-($imageHeight*0.01));
            $width = (int)$imageHeight;
        } elseif ( $imageWidth < $imageHeight ) {
            $height = (int)($imageWidth-($imageWidth*0.01));
            $width = (int)$imageWidth;
        } else {
            $width = $imageWidth;
            $height = (int)($imageHeight-($imageHeight*0.01));
        }

        /*
        if($height / $imageHeight > $width / $imageWidth)
        {
            $width = (int)round($imageHeight / $height * $width);
            $height = $imageHeight;
        }
        else
        {
            $width = $imageWidth;
            $height = (int)round($imageWidth / $width * $height);
        }

        $width = (int)min($width, $im->getSourceWidth());
        $height = (int)min($height, $im->getSourceHeight());
        */
        $im->crop($width, $height, false, \Eventviva\ImageResize::CROPCENTER);
        $im->save($dest);
    }
}

if ( !function_exists('sendSMS') ){
    /**
     * @param $to 0997777662
     * @param $text
     * @return mixed|null|string
     */
    function sendSMS ($to, $text){

        $url = 'https://gate.smsclub.mobi/token/?';
        $username = env('sms_user');    // string User ID (phone number)
        $password = env('sms_pass');        // string Password
        $from = env('sms_from');        // string, sender id (alpha-name) (as long as your alpha-name is not spelled out, it is necessary to use it)
        $text = urlencode(mb_convert_encoding($text, "windows-1251"));       // string Message
        $url_result = $url.'username='.$username.'&token='.$password.'&from='.urlencode($from).'&to='.$to.'&text='.$text;
        if($curl = curl_init())
        {
            curl_setopt($curl, CURLOPT_URL, $url_result);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $out = curl_exec($curl);
            if($out===false){
                echo curl_errno($curl);
                echo curl_error($curl);
            }
            $res = $out;
            $arr = explode("<br/>", $res);
            curl_close($curl);
            return sizeof($arr)>2 ? $arr[1] : null; //smscid
        }
        return null;
    }
}

if ( !function_exists('reportSMS') ){
    function reportSMS ($smscid){
        $url = 'https://gate.smsclub.mobi/http/state.php?';
        $username = env('sms_user');    // string User ID (phone number)
        $password = env('sms_pass');        // string Password
        $url_result = $url.'username='.$username.'&password='.$password.'&smscid='.$smscid;
        if($curl = curl_init())
        {
            curl_setopt($curl, CURLOPT_URL, $url_result);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $out = curl_exec($curl);
            $res = $out;
            curl_close($curl);
            return $res;
        }
        return null;
    }
}

if ( !function_exists('balanceSMS') ){
    function balanceSMS (){
        $url = 'http://smpp.smsclub.mobi/hfw_smpp_addon/httpgetbalance.php?';
        $username = env('sms_user');    // string User ID (phone number)
        $password = env('sms_pass');        // string Password

        $url_result = $url.'username='.$username.'&password='.$password;
        if($curl = curl_init())
        {
            curl_setopt($curl, CURLOPT_URL, $url_result);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $out = curl_exec($curl);
            $res = $out;
            curl_close($curl);
            return $res;
        }
        return null;
    }
}

if ( !function_exists('buttonBuilder') ){
    /**
     * Telegram buttons
     * @param array $arr
     * @param string $key
     * @param array $bottom_buttons
     * @return array
     */
    function buttonBuilder(array $arr, $key = 'name', $bottom_buttons = null){
        if (!$bottom_buttons) $bottom_buttons = [trans('menu.back'), trans('menu.to_menu')];
        $keyboard = [];
        $keyboard[0] = [];
        $iCount = 0; $bTwice = false;
        foreach ($arr as $row){
            //четные начинаются с начала (в строке 2 пункта)
            if(!$bTwice) $keyboard[$iCount] = [];
            $keyboard[$iCount][] = $key==null ? $row : $row[$key];
            if($iCount!=0) $bTwice = !$bTwice;  //для определения что было 2 или не было
            if(!$bTwice || $iCount==0) $iCount++; //только как 2 прошло в ряду, переходим к след, либо в первом ряду 1 знач
        }
        if(is_array($bottom_buttons)){
            if(sizeof($arr)%2==0 && $bTwice) $iCount++;
            $keyboard[$iCount] = $bottom_buttons;
        }
        return $keyboard;
    }
}

if ( !function_exists('buttonLinksBuilder') ){
    /**
     * Inline Telegram buttons
     * @param array $arr
     * @param string $key
     * @param string $link_key
     * @return array
     */
    function buttonLinksBuilder(array $arr, $key = 'name', $link_key='link'){
        $keyboard = [];
        $keyboard[0] = [];
        $iCount = 0; $bTwice = false;
        foreach ($arr as $row){
            if($link_key==null || strlen($row[$link_key])<=0) continue;
            //четные начинаются с начала (в строке 2 пункта)
            if(!$bTwice) $keyboard[$iCount] = [];
            $keyboard[$iCount][] = [
                'text' => $key==null ? $row : $row[$key],
                'url'  => $row[$link_key]
            ];
            if($iCount!=0) $bTwice = !$bTwice;  //для определения что было 2 или не было
            if(!$bTwice || $iCount==0) $iCount++; //только как 2 прошло в ряду, переходим к след, либо в первом ряду 1 знач
        }
        return $keyboard;
    }
}

if ( !function_exists('user_info') ){
    /**
     * @param \App\User|null $user
     * @param bool           $bDetail
     *
     * @return string
     */
    function user_info(App\User $user = null, $bDetail = false){
        if($user==null){
            return 'такой пользователь не зарегистрирован в системе';
        }
        $text = 'Найден пользователь '.$user->id.' ...';
        if(\Activation::completed(Sentinel::findById($user->id))) {
            $text .= "номер 0".$user->tel." подтвержден\n";
        } else {
            $text .= "номер 0".$user->tel." НЕ подтвержден, code: ".$user->sms_code."\n";
        }
        $text .= "пригласил 0".$user->referer_tel." \n";
        if(strlen($user->username)>0) $text .= "nickname: @".$user->username."\n";
        $questionnaire = App\Models\Questionnaire::find($user->id);
        if($questionnaire==null){
            $text .= "анкету в группы вообще не заполнял\n";
        }elseif ($questionnaire->finished){
            $text .= "анкету в группы полностью заполнил\n";
            $text .= "анкета:\n";
            $text .= var_export($questionnaire->getAttributes(), true);
            $text .= "\n";
            $text .= "специальности в анкете:\n";
            $careers = [];
            foreach ( $questionnaire->careers as $career)
                $careers[] = $career->name;
            $text .= var_export($careers, true);
            $text .= "\n";
        }else{
            $text .= "анкету в группы начал заполнять но не закончил\n";
        }
        if($bDetail)
            $text .= "доступны группы:\n";
        $groups = [];
        $availGroups = $user->availableGroups();
        foreach ($availGroups as $group) $groups[] = $group->title;
        if($bDetail) $text .= var_export($groups, true);
        if($bDetail) $text .= "\n";
        $text .= "забанен в группах:\n";
        $kicked = App\Models\Kicked::where('user_id', $user->id)->get();
        for($i=0, $c=sizeof($kicked); $i<$c; $i++){
            /** @var \App\Models\Kicked $kick */
            $kick = &$kicked[$i];
            for($j=0, $u=sizeof($availGroups); $j<$u; $j++){
                $group = &$availGroups[$j];
                if($kick->chat_id==$group->id)
                    $text .= $group->title." " . ($kick->permanent ? '!вручную!' : '') . "\n";
            }
        }
        $text .= "\n";
        $text .= "проявлял активность в группах (находится):\n";
        $groups = [];
        foreach ($user->groups as $group) $groups[] = $group->title;
        $text .= var_export($groups, true);
        $text .= "\n";
        $text .= "назад в главное меню: /main";

        return $text;
    }
}

if ( !function_exists('init_command_params') ){
    /**
     * @param \Telegram\Bot\Objects\Update $update
     *
     * @return array
     */
    function init_command_params (\Telegram\Bot\Objects\Update $update){
        $message = (isset($update['callback_query']) ?
          new \Telegram\Bot\Objects\Message($update['callback_query']['message']) : ($update->getMessage() ? $update->getMessage() : null)
        );
        $chat = $message!=null ? $message->getChat() : null;
        $from = (isset($update['callback_query']['from']) ?
          new \Telegram\Bot\Objects\User($update['callback_query']['from']) : ($message!=null ? $message->getFrom() : null)
        );
        $user_id = $from!=null ? $from->getId() : -1;
        $user = \App\User::find($user_id);
        $text = $message!=null ? $message->getText() : (isset($update['callback_query']['message']['text'] ) ? $update['callback_query']['message']['text'] : '');

        return [$chat, $message, $from, $user, $text];
    }
}


if ( !function_exists('isNeedsWalletBidMessage') ){
    /**
     * @param $type
     * @param $is_fiat
     *
     * @return bool
     */
    function isNeedsWalletBidMessage ($type, $is_fiat){
        //Вывод в фиат. вывести мон (bid_type:buy) на фиат, то у сделки выполняем перевод по фиату и отдаем МОН.
        if ($type == \App\Models\Bid::TYPE_BUY && $is_fiat) {
            return true;
        }
        //Пополнить из фиата. Если пополнить (bid_type:sell) мон за фиат, то у сделки выполняем перевод по фиату и получаем МОН.               is_fiat:true =>  false
        if ($type == \App\Models\Bid::TYPE_SELL && $is_fiat) {
            return false;
        }
        //купить крипту за мон
        if ($type == \App\Models\Bid::TYPE_SELL && !$is_fiat) {
            return true;
        }
        //продать крипту за мон
        if ($type == \App\Models\Bid::TYPE_BUY && !$is_fiat) {
            return false;
        }
    }
}

if ( !function_exists('currencyHumanized') ){
    function currencyHumanized ($is_fiat, $amount){
        if ($is_fiat) {
            return number_format($amount, 2, '.', ' ');
        } else {
            return number_format($amount, 8, '.', ' ');
        }
    }
}