<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\BotsManager;
use Telegram\Bot\Commands\CommandBus;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;

class TelegramController extends Controller
{
    /** @var Api */
    protected $telegram;

    public function __construct()
    {
        $this->telegram= new Api(config('telegram.bot_token'));
    }

    public function setWebhook(){
        $response = $this->telegram->setWebhook(['url' => route('main_telegram_hook')]);
        return var_export($response, true);
    }

    public function removeWebhook(){
        $response = $this->telegram->removeWebhook();
        return json_encode($response);
    }

    public function hook($update = null){
        $user_id = -1;
        try{
            global $bNotChangeCommand, $bAccessChangeData;  //не сохранять пользователю текущую команду, предоставить доступ заново к изменению данных
            $bNotChangeCommand = false;
            $bAccessChangeData = false;
            $telegram = $this->telegram;
            $update = $update == null ? $telegram->commandsHandler(true) : $update;
            $update = new Update($update);
            $message = $update->getMessage();
            $from = $message!=null ? $message->getFrom() : null;
            $chat = $message!=null ? $message->getChat() : null;
            $user_id = $from!=null ? $from->getId() : -1;
            $user = \App\User::find($user_id);
            /** @var CommandBus $commandBus */
            $commandBus = Telegram::getCommandBus();
            $text = "";
            // $this->telegram->sendMessage([
            //     'chat_id'   => 174366505,
            //     'text'   => var_export($update->toArray(), true)
            // ]);

            if($message !== null && $message->has('text'))  //obtain text
                $text = $message->getText();
            if (isset($update['inline_query'])) {
                $this->inline($update);
            } elseif (isset($update['callback_query'])) {   //для inline buttons command [command_dataOfCommand]
                $message = new Message($update['callback_query']['message']);
                $user_id = $update['callback_query']['from']['id'];
                $user = \App\User::find($user_id);
                $arr = explode('_', $update['callback_query']['data']);

                if(sizeof($arr) > 0 && $user) { //всегда помимо команды передаем параметр
                    $command = $arr[0];
                    if ($command[0] == "/") $command = substr($command, 1);
                    $arr = array_splice($arr, 1);
                    if (empty($arr)) {
                        $arr[0] = 1;    //op
                    }
                    $commandBus->execute($command, $arr, $update);
                }
            } elseif($chat!=null && $user_id==$chat->getId()) {   //только когда чат не групповой (1на1)
                if (strlen($text) == 0 || $text[0] != '/') {    //если не команда      <<============================
                    //Telegram::getCommandBus()->handler('/start '.$text, $update);
                    //то вызовем команду у пользователя
                    if ($user != null) {
                        $commandBus->execute($user->command, ['step' => $user->op], $update);
                    } else if ($user_id > 0) {
                        $telegram->sendMessage([
                          'chat_id' => $message->getChat()->getId(),
                          'text' => "Вы не зарегистрированы."
                        ]);
                        $commandBus->execute('start', ['step' => 1], $update);
                    }
                } else {
                    $match = $commandBus->parseCommand($text);
                    $command = "";
                    if (!empty($match)) {
                        $command = $match[1];

                        $arr = explode('_', $match[1]);
                        if (sizeof($arr) > 1) { //помимо команды передаем параметр
                            $command = $arr[0];
                            $arr = array_splice($arr, 1);
                        }

                        $commandBus->execute($command, $arr, $update);
                    }

                    if (strlen($command) && !$bNotChangeCommand && $user) {
                        $user->command = $command;
                        $user->op = 1;
                        $user->save();
                    }
                }

            }elseif($chat!=null && $chat->getId()!=$user_id && $chat->getTitle()!=null){  //сообщение в группе

                $arr = explode('_', $text);
                $command = sizeof($arr) > 1 ? str_replace("/", "", $arr[0]) : "";

                /*
                $telegram->sendMessage([
                    'chat_id' => 174366505, //$chat->getId(),
                    'text' => 'user['.$chat->getId().'] command['.$message->getText()."]\r\n".
                        var_export($arr, true)."\r\n".
                        var_export($update, true)
                ]);
                */

                if($command == 'vote' && sizeof($arr)>1 && $user) { //всегда помимо команды передаем параметр
                    $arr = array_splice($arr, 1);
                    $commandBus->execute($command, $arr[0], $update);
                } else {
                    $commandBus->execute('group', [''], $update);
                }

            }

            if($user!=null && $from!=null){
                $user->last_login = date('Y-m-d H:i:s');
                $user->username = $from->getUsername();
                $user->save();
            }

        }
        catch (TelegramSDKException $e){
            if(isset($message)){
                $text = 'user['.$message->getChat()->getId().'] command['.$message->getText()."]\r\n-----------\r\n".$e->getMessage()."\r\n".
                  var_export($update, true)."\r\n trace: \r\n";
                $arr = explode("%|%", wordwrap($text, 4096, "%|%", true));
                for($i=0; $i<2 && $i<sizeof($arr); $i++) {
                    $this->telegram->sendMessage([
                      'chat_id'=>174366505,
                      'text'=>$arr[$i]
                    ]);
//                    if(env('APP_DEBUG'))
//                    $this->telegram->sendMessage([
//                        'chat_id'=>137830263,
//                        'text'=>$arr[$i]
//                    ]);
                }
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                      'chat_id'=>174366505,
                      'text'=>$arr[$i]
                    ]);
//                    if(env('APP_DEBUG'))
//                    $this->telegram->sendMessage([
//                        'chat_id'=>137830263,
//                        'text'=>$arr[$i]
//                    ]);
                }
            }
            elseif($user_id>0)
                $this->telegram->sendMessage([
                  'chat_id'=>$user_id,
                  'text' => $e->getMessage()
                ]);
            else {
                $this->telegram->sendMessage([
                  'chat_id'=>174366505,
                  'text' => $e->getMessage()
                ]);
//                if(env('APP_DEBUG'))
//                $this->telegram->sendMessage([
//                    'chat_id'=>137830263,
//                    'text' => $e->getMessage()
//                ]);
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                      'chat_id'=>174366505,
                      'text'=>$arr[$i]
                    ]);
//                    if(env('APP_DEBUG'))
//                    $this->telegram->sendMessage([
//                        'chat_id'=>137830263,
//                        'text'=>$arr[$i]
//                    ]);
                }
            }
            return "Ok";
        }
        catch (\Exception $e){
            Log::error('exception');
            if(isset($message)){
                $this->telegram->sendMessage([
                  'chat_id'=>174366505,
                  'text' => 'user['.$message->getChat()->getId().'] command['.$message->getText()."]\r\n------------\r\n".$e->getMessage()."\r\n".
                    var_export($update, true)."\r\n trace: \r\n"
                ]);
//                if(env('APP_DEBUG'))
//                $this->telegram->sendMessage([
//                    'chat_id'=>137830263,
//                    'text' => 'user['.$message->getChat()->getId().'] command['.$message->getText()."]\r\n------------\r\n".$e->getMessage()."\r\n".
//                        var_export($update, true)."\r\n trace: \r\n"
//                ]);
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                      'chat_id'=>174366505,
                      'text'=>$arr[$i]
                    ]);
//                    if(env('APP_DEBUG'))
//                    $this->telegram->sendMessage([
//                        'chat_id'=>137830263,
//                        'text'=>$arr[$i]
//                    ]);
                }
            } else {
                $this->telegram->sendMessage([
                  'chat_id'=>174366505,
                  'text' => $e->getMessage()
                ]);
//                if(env('APP_DEBUG'))
//                $this->telegram->sendMessage([
//                    'chat_id'=>137830263,
//                    'text' => $e->getMessage()
//                ]);
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                      'chat_id'=>174366505,
                      'text'=>$arr[$i]
                    ]);
//                    if(env('APP_DEBUG'))
//                    $this->telegram->sendMessage([
//                        'chat_id'=>137830263,
//                        'text'=>$arr[$i]
//                    ]);
                }
            }
            return "Ok";
        }

        return 'Ok';
    }

    public function getMe(){
        $telegram = $this->telegram;
        return $telegram->getMe();
    }

    public function getCommands(){
        $telegram = $this->telegram;
        return $telegram->getCommands();
    }

    public function inline(Update $update){
        return;
    }

    public function getUpdates()
    {
        $telegram = $this->telegram;
        $update = $telegram->commandsHandler();
        //$telegram->triggerCommand('buy', $update);

        dd($update);
    }
}
