<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property int    id
 * @property string name
 * @property string command
 * @property string auth_token
 * @property int    op
 * @property int    chat_id
 * @property string username
 * @property \DateTime  last_login
 * @property int    additional_int
 * @property double amount
 * @property boolean is_admin
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name', 'command', 'auth_token',
        'op', 'chat_id', 'username',
        'last_login', 'additional_int', 'amount', 'is_admin'
    ];

    protected $casts = [
        'last_login'    => 'datetime',
        'updated_at'    => 'datetime',
        'created_at'    => 'datetime',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setStateCommand($cmdName, $op = 1, $additionalInt = 0) {
        $this->op = $op;
        $this->additional_int = $additionalInt;
        $this->command = $cmdName;
        $this->save();
    }

    public function getUsernameOrName()
    {
        return (strlen($this->username) > 0 ? '@' . $this->username : $this->name);
    }
}
