<?php

namespace App\Events;

use App\Models\Deal;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Telegram\Bot\Api;

class DealStatusChangedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $dealId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Deal $deal)
    {
        $this->dealId   = $deal->id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('default');
    }
}
