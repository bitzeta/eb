<?php

namespace App\Listeners;

use App\Events\DealStatusChangedEvent;
use App\Jobs\AutoCloseDealJob;
use App\Models\Deal;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DealStatusChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DealStatusChangedEvent $event)
    {
        $deal = Deal::find($event->dealId);

        if (!$deal) {
            \Log::error('DealStatusChangedListener: deal not found');
            return;
        }

        if ($deal->status == Deal::STATUS_PRE_SOLD) {
            $date = Carbon::now()->addMinutes($deal->auto_close_minutes);

            \Queue::later($date, new AutoCloseDealJob($deal));
        }
    }
}
