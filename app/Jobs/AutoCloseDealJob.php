<?php

namespace App\Jobs;

use App\Models\Deal;
use App\Services\BidsService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Telegram\Bot\Api;

class AutoCloseDealJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Deal */
    protected $deal;

    /** @var string  */
    protected $cacheKey;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
        $this->cacheKey = 'AutoCloseDealJob' . $deal->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $api  = new Api(config('telegram.bot_token'));

        $bidsService = new BidsService($api);
        $timeout = 5 * 60;  //seconds
        $this->deal->refresh();
        $deal = $this->deal;

        $cacheKey = $this->cacheKey;
        $alreadyStarted = cache()->has($cacheKey) ? cache()->get($cacheKey) : false;
        $now = new Carbon();
        \Log::info('AutoCloseDealJob');

        if ($alreadyStarted && (($now->getTimestamp() - $alreadyStarted) <= $timeout)) {
            \Log::info('AutoCloseDealJob for request ' . $deal->id . ' already started, sleep...');
            $this->release(40);
            return;
        }

        cache()->forget($cacheKey);
        cache()->set($cacheKey, $now->getTimestamp(), new \DateInterval('P5M'));

        //TODO:: проверять время последнего обновления сделки, от этого времени надо брать +auto_close_time

        if ($deal->status == Deal::STATUS_PRE_SOLD) {
            $bidsService->dealSold($deal);
            $bidsService->messageDeal($deal);
            $bidsService->messageDeal($deal, null, false);
        }

        cache()->forget($cacheKey); //НЕ ЗАБЫВАЕМ
    }
}
