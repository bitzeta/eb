<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DropTablesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:drop_tables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WARNING! all data will be erased.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (app()->environment() == 'production')
            throw new \Exception('Oh no, I don\'t work in production mode.');

        \DB::select('SET foreign_key_checks = 0;');
        $tables = \DB::select('SHOW tables;');
        foreach ($tables as $table) {
            foreach ($table as $key => $value) {
                $res = \DB::select('DROP TABLE `'.$value.'`');
            }
        }
        \DB::select('SET foreign_key_checks = 1;');
    }
}
