<?php

namespace App\Console\Commands;

use App\Http\Controllers\TelegramController;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class LocalTgCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:tg:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // if (cache()->has('tg_started')) return;

        cache()->set('tg_started', true, 123);

        $api = new Api(config('telegram.bot_token'));

        while (cache()->has('tg_started')) {
            $update = $api->commandsHandler();
            if (is_array($update)) {
                foreach ($update as $upd) {
                    app(TelegramController::class)->hook($upd);
                }
            } else {
                app(TelegramController::class)->hook($update);
            }
            usleep(200);
        }
    }
}
