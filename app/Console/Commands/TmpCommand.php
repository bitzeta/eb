<?php

namespace App\Console\Commands;

use App\Bitfinex;
use App\Services\PriceService;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class TmpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:tmp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $priceService = new PriceService();

        //234 кроны в SEK => BTC
        //234 cron => UAH
        //UAH => USD
        //USD => BTC

        $details = [];
        dd((new Api(config('telegram.bot_token')))->getMe());

        $fiat = 803.9323250900;
        $usd = $priceService->fiat2usd("UAH", $fiat, 1);
        $fiat2 = $priceService->usd2fiat('UAH', $usd, 1);
        dd($fiat . ' => ' . $usd, $fiat2 .' => ' . $usd);
        dd(
          $priceService->fiat2usd("EUR", 1234),
            $priceService->usd2fiat("EUR", 1444.5203821467)
            //$priceService->usd2crypt("btc", 8194),
            //$priceService->crypt2usd("btc", 1),
            //$priceService->calc('usd', 8075, 'uah', 'bid', $details),
          //$priceService->calc('USD', 8075, 'BTC', 'bid', $details),
          //$priceService->calc('BTC', 1, 'USD', 'bid', $details)
        );
        $bitfinex = new Bitfinex(env('BITFINEX_KEY'), env('BITFINEX_SECRET'));

        $stats = $bitfinex->get_ticker();

        if (!is_array($stats) || !isset($stats['bid']) || !isset($stats['ask'])) {
            throw new \Exception('Bitfinex is not work');
        }

        dd($stats);
    }
}
