<?php

return [

    'buy'       => '📈 купить',
    'sell'      => '📉 продать',
    'fill_in'   => '➕ пополнить',
    'withdraw'  => '➖ вывести',
    'account'   => '💼 Кошелек',
    'settings'  => '⚙️ настройки',
    'back'      => 'назад 🔙',
    'callback'  => 'обратная связь ☎️',
    'to_menu'   => '📜 в меню',
    'update'    => '🔄 обновить',
    'cabinet'   => '🏢 Кабинет',
    'my_bids'   => 'предложения',
    'my_deals'  => 'сделки',
    'my_transactions'   => 'транзакции',

];
