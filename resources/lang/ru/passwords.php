<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароли должны быть как минимум из 6 символов и совпадать.',
    'reset' => 'Ваш пароль успешно сброшен!',
    'sent' => 'Мы отправили вам ссылку сброса пароля!',
    'token' => 'Такой ключ сброса пароля не найден.',
    'user' => "Пользователь с таким e-mail не найден.",

];
