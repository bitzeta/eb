<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

  'accepted'             => 'Параметр :attribute должен быть принят.',
  'active_url'           => 'Параметр :attribute не соответствует ссылке.',
  'after'                => 'Параметр :attribute должен быть датой позднее :date.',
  'after_or_equal'       => 'Параметр :attribute должен быть датой равной или позднее :date.',
  'alpha'                => 'Параметр :attribute должен содержать только буквы.',
  'alpha_dash'           => 'Параметр :attribute должен содержать только буквы, числа и тире.',
  'alpha_num'            => 'Параметр :attribute должен содержать только буквы и числа.',
  'array'                => 'Параметр :attribute должен быть массивом.',
  'before'               => 'Параметр :attribute должен быть датой ранее :date.',
  'before_or_equal'      => 'Параметр :attribute должен быть датой равной или ранее :date.',
  'between'              => [
    'numeric' => 'Параметр :attribute должен быть между :min и :max.',
    'file'    => 'Параметр :attribute должен быть между :min и :max кб.',
    'string'  => 'Параметр :attribute должен быть между :min и :max символов.',
    'array'   => 'Параметр :attribute должен быть между :min и :max значениями.',
  ],
  'boolean'              => 'Параметр :attribute должен быть true или false.',
  'confirmed'            => 'Параметр :attribute не подтвержден.',
  'date'                 => 'Параметр :attribute не похож на дату.',
  'date_format'          => 'Параметр :attribute не совпадает с форматом :format.',
  'different'            => 'Параметры :attribute и :other должен отличатся.',
  'digits'               => 'Параметр :attribute должен иметь :digits цифр.',
  'digits_between'       => 'Параметр :attribute должен быть между :min и :max цифрами.',
  'dimensions'           => 'Параметр :attribute имеет не правильное соотношение сторон изображения.',
  'distinct'             => 'Параметр :attribute имеет дублирующееся значение.',
  'email'                => 'Параметр :attribute должен быть email адресом.',
  'exists'               => 'Параметр :attribute не существует.',
  'file'                 => 'Параметр :attribute должен быть файлом.',
  'filled'               => 'Параметр :attribute должен быть заполнен.',
  'gt'                   => [
    'numeric' => 'Параметр :attribute должен быть больше чем :value.',
    'file'    => 'Параметр :attribute должен быть больше чем :value кб.',
    'string'  => 'Параметр :attribute должен быть больше чем :value символов.',
    'array'   => 'Параметр :attribute должен быть больше чем :value значений.',
  ],
  'gte'                  => [
    'numeric' => 'Параметр :attribute должен быть больше или равен чем :value.',
    'file'    => 'Параметр :attribute должен быть больше или равен чем :value кб.',
    'string'  => 'Параметр :attribute должен быть больше или равен чем :value символов.',
    'array'   => 'Параметр :attribute должен быть больше или равен чем :value значений.',
  ],
  'image'                => 'Параметр :attribute должен быть изображением.',
  'in'                   => 'Параметр :attribute имеет недопустимое значение.',
  'in_array'             => 'Параметра :attribute нет в :other.',
  'integer'              => 'Параметр :attribute должен быть чистом.',
  'ip'                   => 'Параметр :attribute должен быть IP адресом.',
  'ipv4'                 => 'Параметр :attribute должен быть IPv4 адресом.',
  'ipv6'                 => 'Параметр :attribute должен быть IPv6 адресом.',
  'json'                 => 'Параметр :attribute должен быть JSON строкой.',
  'lt'                   => [
    'numeric' => 'Параметр :attribute должен быть меньше чем :value.',
    'file'    => 'Параметр :attribute должен быть меньше чем :value кб.',
    'string'  => 'Параметр :attribute должен быть меньше чем :value символов.',
    'array'   => 'Параметр :attribute должен быть меньше чем :value значений.',
  ],
  'lte'                  => [
    'numeric' => 'Параметр :attribute должен быть меньше или равен :value.',
    'file'    => 'Параметр :attribute должен быть меньше или равен :value кб.',
    'string'  => 'Параметр :attribute должен быть меньше или равен :value симв.',
    'array'   => 'Параметр :attribute должен быть меньше или равен :value знач.',
  ],
  'max'                  => [
    'numeric' => 'Параметр :attribute не должен быть больше чем :max.',
    'file'    => 'Параметр :attribute не должен быть больше чем :max кб.',
    'string'  => 'Параметр :attribute не должен быть больше чем :max символов.',
    'array'   => 'Параметр :attribute не должен быть больше чем :max значений.',
  ],
  'mimes'                => 'Параметр :attribute должен быть фалом типа: :values.',
  'mimetypes'            => 'Параметр :attribute должен быть фалом типа: :values.',
  'min'                  => [
    'numeric' => 'Параметр :attribute должен иметь как минимум :min.',
    'file'    => 'Параметр :attribute должен иметь как минимум :min kilobytes.',
    'string'  => 'Параметр :attribute должен иметь как минимум :min символах.',
    'array'   => 'Параметр :attribute должен иметь как минимум :min значениях.',
  ],
  'not_in'               => 'Параметр :attribute не имеет таких значений.',
  'not_regex'            => 'У :attribute не правильный формат.',
  'numeric'              => 'Параметр :attribute должен быть числом.',
  'present'              => 'Параметр :attribute должен быть представлен/выбран.',
  'regex'                => 'Параметр :attribute не соответствует формату.',
  'required'             => 'Параметр :attribute обязателен.',
  'required_if'          => 'Параметр :attribute обязателен когда :other имеет значение :value.',
  'required_unless'      => 'Параметр :attribute обязателен пока :other не имеет значения :values.',
  'required_with'        => 'Параметр :attribute обязателен с :values значениями.',
  'required_with_all'    => 'Параметр :attribute обязателен со всеми :values значениями.',
  'required_without'     => 'Параметр :attribute обязателен когда нет значений :values.',
  'required_without_all' => 'Параметр :attribute обязателен когда нет таких значений :values.',
  'same'                 => 'Параметр :attribute и :other должны совпадать.',
  'size'                 => [
    'numeric' => 'Параметр :attribute должен быть :size.',
    'file'    => 'Параметр :attribute должен быть :size кб.',
    'string'  => 'Параметр :attribute должен быть :size символов.',
    'array'   => 'Параметр :attribute содержать :size значений.',
  ],
  'string'               => 'Параметр :attribute должен быть строкой.',
  'timezone'             => 'Параметр :attribute должен быть правильной временной зоной.',
  'unique'               => 'Параметр :attribute с этим значением уже существует.',
  'uploaded'             => 'Параметр :attribute с ошибкой загрузки.',
  'url'                  => 'Параметр :attribute не соответствует формату.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

  'custom' => [
    'attribute-name' => [
      'rule-name' => 'custom-message',
    ],
  ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

  'attributes' => [],

];
