<?php

return [

    'sub_bids' => [ //можно ли разбивать на меньшие
        'no'    => 'нет',
        'yes'   => 'можно, диапазон укажу',
    ],
    'accept_done'  => 'готово',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'yes_close'    => 'Да, закрыть',

];
