<?php namespace Tests\commands;

use App\User;
use Carbon\Carbon;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;

/**
 * Created by PhpStorm.
 * User: bitze
 * Date: 14.05.2017
 * Time: 16:28
 */

trait TestCommandTrait {
    /** @var array|\Illuminate\Support\Collection  */
    public $triggered = [];
    /** @var array|\Illuminate\Support\Collection  */
    public $sended = [];

    public function __construct($telegram, Update $update = null)
    {
        $this->telegram = $telegram;
        $this->update = $update ? $update : new \Telegram\Bot\Objects\Update([]);
        $this->triggered = collect([]);
        $this->sended = collect([]);
    }

    public function setUpdate(Update $update)
    {
        $this->update = $update;

        return $this;
    }

    public function incomeMessageText(User $user, $bPrivate = true, string $string)
    {
        file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' incomeMessageText - ' . $string . "\r\n", FILE_APPEND | LOCK_EX);
        return new Update([
          'update_id' => rand(10000, 100000000),
          'message' => [
              'message_id' => rand(0, 10000),
              'from' => [
                  'id' => $user->id,
                  'is_bot' => false,
                  'first_name' => $user->name,
                  'username' => $user->username,
                  'language_code' => 'ru-UA'
              ],
              'chat' => [
                'id' => $user->id,
                'first_name' => $user->name,
                'username' => $user->username,
                'type' => $bPrivate ? 'private' : 'chanell TODO:' //TODO: if not private?,
              ],
              'date' => (new Carbon())->timestamp,
              'text' => $string
          ],
        ]);
    }

    public function incomecallback_query(User $user, $bPrivate = true, $data)
    {
        file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' incomecallback_query - ' . $data . "\r\n", FILE_APPEND | LOCK_EX);
        return new Update([
          'update_id' => rand(10000, 100000000),
          'callback_query' => [
              'id' => rand(100000000, 100000000000),
              'from' => [
                  'id' => $user->id,
                  'is_bot' => false,
                  'first_name' => $user->name,
                  'username' => $user->username,
                  'language_code' => 'ru-UA'
              ],

              'message' => [
                'message_id' => rand(0, 10000),
                'from' => [
                  'id' => $user->id,
                  'is_bot' => false,
                  'first_name' => $user->name,
                  'username' => $user->username,
                  'language_code' => 'ru-UA'
                ],
                'chat' => [
                  'id' => $user->id,
                  'first_name' => $user->name,
                  'username' => $user->username,
                  'type' => $bPrivate ? 'private' : 'chanell TODO:' //TODO: if not private?,
                ],
                'date' => (new Carbon())->timestamp,
                'text' => 'noneeds'
              ],

              'chat_instance'   => rand(100000000, 10000000000),
              'data'            => $data,
          ],
        ]);
    }

    public function parseCallbackQuery()
    {
        $update = $this->getUpdate();
        $message = new Message($update['callback_query']['message']);
        $user_id = $update['callback_query']['from']['id'];
        $user = \App\User::find($user_id);
        $arr = explode('_', $update['callback_query']['data']);

        if(sizeof($arr)>1 && $user) { //всегда помимо команды передаем параметр
            $command = $arr[0];
            $arr = array_splice($arr, 1);
        }

        return $arr;
    }

    public function video(User $user, $bPrivate = true)
    {
        file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' video - file_id:BAADAgADXgIAAjnIyEve-zUUtFYAAUIC' . "\r\n", FILE_APPEND | LOCK_EX);
        return new Update([
          'update_id' => rand(10000, 100000000),
          'message' => [
            'message_id' => rand(0, 10000),
            'from' => [
              'id' => $user->id,
              'is_bot' => false,
              'first_name' => $user->name,
              'username' => $user->username,
              'language_code' => 'ru-UA'
            ],
            'chat' => [
              'id' => $user->id,
              'first_name' => $user->name,
              'username' => $user->username,
              'type' => $bPrivate ? 'private' : 'chanell TODO:' //TODO: if not private?,
            ],
            'date' => (new Carbon())->timestamp,
            'video' => [
                'duration'    => 240,
                'width'       => 444,
                'height'      => 240,
                'mime_type'   => 'video/mp4',
                'thumb'       => [
                    'file_id'   => 'AAQCABNmcLcOAAQTuo6dMU1U5slfAAIC',
                    'file_size' => 1993,
                    'width'     => 90,
                    'height'    => 48,
                ],
                'file_id'   => 'BAADAgADXgIAAjnIyEve-zUUtFYAAUIC',
                'file_size' => 12922908
            ]
          ],
        ]);
    }

    public function replyWithMessage($params) {
        //echo 'replyWithMessage' . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . PHP_EOL;
        file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\r\n", FILE_APPEND | LOCK_EX);
        $this->sended->push(array_merge($params, ['cmd' => 'replyWithMessage']));
    }

    public function sendMessage($params) {
        //echo 'sendMessage' . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . PHP_EOL;
        file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\r\n", FILE_APPEND | LOCK_EX);
        $this->sended->push(array_merge($params, ['cmd' => 'sendMessage']));
    }

    public function editMessageText($params) {
        //echo 'editMessageText' . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . PHP_EOL;
        file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\r\n", FILE_APPEND | LOCK_EX);
        $this->sended->push(array_merge($params, ['cmd' => 'editMessageText']));
    }

    public function triggerCommand($command, $arguments = null) {
        $this->triggered->push([
            'command' => $command,
            'arguments' => $arguments
        ]);
    }
}