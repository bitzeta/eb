<?php namespace Tests\commands;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

include 'TestCommandTrait.php';

class StartCommandTest extends \Telegram\Bot\Commands\StartCommand {
    use TestCommandTrait;
}

// class StartTest extends \Tests\TestCase
// {
//     public function testNotRegistered() {
//         $startCommand = new StartCommandTest();
//
//         exit(var_dump($startCommand->getUpdate()));
//         $arguments = [];
//         $startCommand->handle($arguments);
//     }
// }
