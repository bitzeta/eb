<?php namespace Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Collection;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\HttpClients\GuzzleHttpClient;
use Telegram\Bot\HttpClients\HttpClientInterface;

class MockHttpClient implements HttpClientInterface
{
    /** @var array|Collection */
    public $sended = [];

    /**
     * HTTP client.
     *
     * @var Client
     */
    protected $client;

    /**
     * @var PromiseInterface[]
     */
    private static $promises = [];

    /**
     * Timeout of the request in seconds.
     *
     * @var int
     */
    protected $timeOut = 30;

    /**
     * Connection timeout of the request in seconds.
     *
     * @var int
     */
    protected $connectTimeOut = 10;

    /**
     * @var string
     */
    protected $mocksPath;

    /** @var Logger */
    protected $logger;

    protected $baseUrl = "";

    public $bOnlyMocks = true;

    public $mockScenario = 'main';

    /**
     * @param Client|null $client
     *
     * @throws \Exception
     */
    public function __construct(Client $client = null, $mocksPath, $baseUrl)
    {
        $config         = [];
        $this->logger   = $this->initLogMessageFormatter();
        $this->client   = $client ?: new Client($config);
        $this->mocksPath= $mocksPath;
        $this->baseUrl  = $baseUrl;
        $this->sended   = collect([]);
    }

    /**
     * @return Logger
     * @throws \Exception
     */
    private function initLogMessageFormatter()
    {
        $logger = new Logger('Logger');
        $logger->pushHandler(new StreamHandler(storage_path('logs/guzzle-' . date('Y-m-d') . '.log')));
        return $logger;
    }

    /**
     * Unwrap Promises.
     */
    public function __destruct()
    {
        Promise\unwrap(self::$promises);
    }

    /**
     * Sets HTTP client.
     *
     * @param Client $client
     *
     * @return GuzzleHttpClient
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Gets HTTP client for internal class use.
     *
     * @return Client
     */
    private function getClient()
    {
        return $this->client;
    }

    /**
     * {@inheritdoc}
     */
    public function send(
      $url,
      $method,
      array $headers = [],
      array $options = [],
      $timeOut = 30,
      $isAsyncRequest = false,
      $connectTimeOut = 10
    ) {
        $this->timeOut = $timeOut;
        $this->connectTimeOut = $connectTimeOut;
        $body = isset($options['body']) ? $options['body'] : null;
        $options = $this->getOptions($headers, $body, $options, $timeOut, $isAsyncRequest, $connectTimeOut);

        $cmd = str_replace($this->baseUrl . config('telegram.bot_token') . '/', "", $url) . '.php';
        $data = [];

        if (file_exists($this->mocksPath . DIRECTORY_SEPARATOR . $cmd)) {
            $bodyArr = (array)json_decode($body, true);
            $params = array_merge($bodyArr, ['mockScenario' => $this->mockScenario], $options['form_params']);
            $data = include $this->mocksPath . DIRECTORY_SEPARATOR . $cmd;
            //echo $cmd . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . PHP_EOL;
            $this->sended->push(array_merge($params, ['cmd' => $cmd]));
            file_put_contents(storage_path('logs') . DIRECTORY_SEPARATOR . 'mocked.log', date('Y-m-d H:i:s') . ' ' . $cmd . ' ' . json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . ' - ' . json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\r\n", FILE_APPEND | LOCK_EX);
            return new Response(200, $headers, json_encode($data, JSON_PRETTY_PRINT));
        } elseif ($this->bOnlyMocks) {
            dd('file NOT exists ', $this->mocksPath . DIRECTORY_SEPARATOR . $cmd);
        }

        try {
            $response = $this->getClient()->requestAsync($method, $url, $options);
            if ($isAsyncRequest) {
                self::$promises[] = $response;
            } else {
                $response = $response->wait();
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response instanceof ResponseInterface) {
                throw new TelegramSDKException($e->getMessage(), $e->getCode());
            }
        }
        return $response;
    }

    /**
     * Prepares and returns request options.
     *
     * @param array $headers
     * @param       $body
     * @param       $options
     * @param       $timeOut
     * @param       $isAsyncRequest
     * @param int   $connectTimeOut
     *
     * @return array
     */
    private function getOptions(array $headers, $body, $options, $timeOut, $isAsyncRequest = false, $connectTimeOut = 10)
    {
        $default_options = [
          RequestOptions::HEADERS         => $headers,
          RequestOptions::BODY            => $body,
          RequestOptions::TIMEOUT         => $timeOut,
          RequestOptions::CONNECT_TIMEOUT => $connectTimeOut,
          RequestOptions::SYNCHRONOUS     => !$isAsyncRequest,
        ];
        return array_merge($default_options, $options);
    }

    /**
     * @return int
     */
    public function getTimeOut()
    {
        return $this->timeOut;
    }

    /**
     * @return int
     */
    public function getConnectTimeOut()
    {
        return $this->connectTimeOut;
    }
}