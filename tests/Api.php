<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 19.08.18
 * Time: 1:17
 */

namespace Tests;


use GuzzleHttp\Client;
use Telegram\Bot\HttpClients\HttpClientInterface;

class Api extends \Telegram\Bot\Api
{
    public function __construct(
      ?string $token = null,
      bool $async = false,
      ?HttpClientInterface $httpClientHandler = null
    ) {
        parent::__construct($token, $async, $httpClientHandler);

        $this->getClient()->setHttpClientHandler(
          new \Tests\MockHttpClient(
            new Client(),
            base_path() . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . 'mock_telegram',
            $this->getClient()->getBaseBotUrl()
          )
        );
    }
}