<?php

namespace Tests\Unit;

use App\Events\DealStatusChangedEvent;
use App\Models\BalanceAccept;
use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\PaymentSystem;
use App\Models\Selection;
use App\User;
use Illuminate\Support\Collection;
use Tests\commands\AccountBuyCommandTest;
use Tests\commands\AddBuyCryptCommandTest;
use Tests\commands\AddBuyFiatCommandTest;
use Tests\commands\BalanceAcceptCommandTest;
use Tests\commands\BuyCommandTest;
use Tests\commands\DealCommandTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuyTest extends TestCase
{
    public function testBuyCryptDeal()   //покупатель покупает cryp, отдает usd
    {
        $user = User::find(174366505);
        $userBuyer = User::find(204367870);
        $dealc = new DealCommandTest($this->api);
        $addbc = new BuyCommandTest($this->api);
        $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, ''));
        $addbc->handle([]); //select currency message

        $selection = Selection::latest()->first();

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $addbc->getName() . '_2_0_' . $selection->id . '_' . Currency::find(1)->id));
        $addbc->handle($addbc->parseCallbackQuery()); //what amount message

        //what amount usd message

        $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, 1000));
        $addbc->handle(['step' => $userBuyer->refresh()->op]);

        $selection->refresh();
        $this->assertTrue($selection->amount == 1000);

        //select first bid
        $lastMessage = $this->api->getClient()->getHttpClientHandler()->sended->last();
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $inline_keyboard[0][0]['callback_data']));
        $addbc->handle($addbc->parseCallbackQuery());

        //showed info message

        //select "start deal"
        $lastMessage = $this->api->getClient()->getHttpClientHandler()->sended->last();
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $inline_keyboard[0][1]['callback_data']));
        $addbc->handle($addbc->parseCallbackQuery());   //wallet number message

        $selection->refresh();
        $this->assertInstanceOf(Bid::class, $selection->bid);

        $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, "tipaWalletNumber"));
        $addbc->handle(['step' => $userBuyer->refresh()->op]);

        $selection->refresh();
        $this->assertTrue($selection->details == "tipaWalletNumber");

        $lastMessage = $addbc->sended->last();
        $reply_markup = $lastMessage['reply_markup'];
        $keyboard = $reply_markup['keyboard'];

        $amount = $userBuyer->amount;
        $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, $keyboard[0][0]['text']));
        $inputed = floatval(str_replace([" ", ","], ["", "."], $keyboard[0][0]['text']));
        $addbc->handle(['step' => $userBuyer->refresh()->op]);  //input amount

        $this->assertTrue($userBuyer->refresh()->amount + $inputed == $amount); //деньги заблокировались

        $deal = Deal::latest()->first();
        $this->assertTrue($deal->status == Deal::STATUS_WAIT);
        $this->assertTrue($deal->amount == $inputed);

        /** @var Collection $sended */
        $sended = $this->api->getClient()->getHttpClientHandler()->sended;
        $prev2Message = $sended->get($sended->count() - 2);
        $this->assertTrue($prev2Message['chat_id'] == $user->chat_id);  //check seller got message
        $reply_markup = json_decode($prev2Message['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];
        $dealc->setUpdate($dealc->incomecallback_query($user, true, $inline_keyboard[0][0]['callback_data']));
        $dealc->handle($dealc->parseCallbackQuery());   //seller accept deal

        $deal->refresh();
        $this->assertTrue($deal->status == Deal::STATUS_ALLOWED);

        $sended = $this->api->getClient()->getHttpClientHandler()->sended;
        $prev2Message = $sended->get($sended->count() - 2);
        $this->assertTrue($prev2Message['chat_id'] == $user->chat_id);
        $reply_markup = json_decode($prev2Message['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $dealc->setUpdate($dealc->incomecallback_query($user, true, $inline_keyboard[0][0]['callback_data']));  //seller set conditions are done
        $dealc->handle($dealc->parseCallbackQuery());

        $sended = $this->api->getClient()->getHttpClientHandler()->sended;
        $lastMessage = $sended->get($sended->count() - 1);
        $this->assertTrue($lastMessage['chat_id'] == $userBuyer->chat_id);
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $amount = $user->amount;
        $dealc->setUpdate($dealc->incomecallback_query($userBuyer, true, $inline_keyboard[0][0]['callback_data']));  //all done
        $dealc->handle($dealc->parseCallbackQuery());
        $this->assertTrue($user->refresh()->amount - $inputed == $amount);   //после сделки деньги пришли на счет продавцу

        $deal->refresh();

        $this->assertTrue($deal->status == Deal::STATUS_SOLD);
    }

    public function testBuyFiatBid()    //купить USD и отдать фиат
    {
        $user = User::find(174366505);
        $userBuyer = User::find(204367870);
        $amount = $userBuyer->amount;
        $dealc = new DealCommandTest($this->api);
        $addbc = new AccountBuyCommandTest($this->api);
        $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, ''));
        $addbc->handle([]); //select currency message

        // create deal

        $selection = Selection::latest()->first();

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $addbc->getName() . '_2_0_' . $selection->id . '_' . Currency::find(4)->id));
        $addbc->handle($addbc->parseCallbackQuery()); //select payment system message

        //select first payment system
        $lastMessage = $this->api->getClient()->getHttpClientHandler()->sended->last();
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $inline_keyboard[0][0]['callback_data']));
        $addbc->handle($addbc->parseCallbackQuery()); //bids list

        //select first bid
        $lastMessage = $this->api->getClient()->getHttpClientHandler()->sended->last();
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $inline_keyboard[0][0]['callback_data']));
        $addbc->handle($addbc->parseCallbackQuery());

        //showed info message

        //select "start deal"
        $lastMessage = $this->api->getClient()->getHttpClientHandler()->sended->last();
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $addbc->setUpdate($addbc->incomecallback_query($userBuyer, true, $inline_keyboard[0][1]['callback_data']));
        $addbc->handle($addbc->parseCallbackQuery());   //wallet number message

        $selection->refresh();
        $this->assertInstanceOf(Bid::class, $selection->bid);
        $bid_amount = $selection->bid->amount_total;

        $bid_amount = $this->bidsService->priceService->calc(
          $selection->bid->currency->shortcode,
          $bid_amount,
          config('pricing.associated_currency'),
          $selection->bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',
          $details,
          $selection->bid->commission_creator_value
        );

        $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, "не обязательный comment"));
        $addbc->handle(['step' => $userBuyer->refresh()->op]);

        $selection->refresh();
        $this->assertTrue($selection->details == "не обязательный comment");

        if (!$selection->bid->is_fixed_price) {
            $lastMessage = $addbc->sended->last();
            $reply_markup = $lastMessage['reply_markup'];
            $keyboard = $reply_markup['keyboard'];

            $addbc->setUpdate($addbc->incomeMessageText($userBuyer, true, $keyboard[0][0]['text']));
            $inputed = floatval(str_replace([" ", ","], ["", "."], $keyboard[0][0]['text']));

            $inputed = $this->bidsService->priceService->calc(
              $selection->bid->currency->shortcode,
              $inputed,
              config('pricing.associated_currency'),
              $selection->bid->type == Bid::TYPE_SELL ? 'ask' : 'bid',
              $details,
              $selection->bid->commission_creator_value
            );
            echo $keyboard[0][0]['text'].' => '.$inputed.PHP_EOL;
            $addbc->handle(['step' => $userBuyer->refresh()->op]);  //input amount
        }

        $deal = Deal::latest()->first();
        $userBuyer->refresh();

        $this->assertTrue(
          bcsub($amount, $userBuyer->amount, 10) == round($deal->amount, 10),
          ' deal->amount[' . var_export(round($deal->amount, 10), true) . '], uamount diff ' . bcsub($amount, $userBuyer->amount, 10)
        ); //деньги заблокировались, продавец будет с фиатного кошелька платить
        $this->assertTrue($deal->status == Deal::STATUS_WAIT);

        /** @var Collection $sended */
        $sended = $this->api->getClient()->getHttpClientHandler()->sended;
        $prev2Message = $sended->get($sended->count() - 2);
        $this->assertTrue($prev2Message['chat_id'] == $user->chat_id);  //check seller got message
        $reply_markup = json_decode($prev2Message['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];
        $dealc->setUpdate($dealc->incomecallback_query($user, true, $inline_keyboard[0][0]['callback_data']));
        $dealc->handle($dealc->parseCallbackQuery());   //seller accept deal

        $deal->refresh();
        $this->assertTrue($deal->status == Deal::STATUS_ALLOWED);

        $sended = $this->api->getClient()->getHttpClientHandler()->sended;
        $prev2Message = $sended->get($sended->count() - 2);
        $this->assertTrue($prev2Message['chat_id'] == $userBuyer->chat_id);
        $reply_markup = json_decode($prev2Message['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $dealc->setUpdate($dealc->incomecallback_query($userBuyer, true, $inline_keyboard[0][0]['callback_data']));  //seller set conditions are done
        $dealc->handle($dealc->parseCallbackQuery());

        $sended = $this->api->getClient()->getHttpClientHandler()->sended;
        $lastMessage = $sended->get($sended->count() - 1);
        $this->assertTrue($lastMessage['chat_id'] == $user->chat_id);
        $reply_markup = json_decode($lastMessage['reply_markup'], true);
        $inline_keyboard = $reply_markup['inline_keyboard'];

        $dealc->setUpdate($dealc->incomecallback_query($user, true, $inline_keyboard[0][0]['callback_data']));  //all done
        $dealc->handle($dealc->parseCallbackQuery());
        $this->assertTrue(round($userBuyer->amount, 10) == bcsub($amount, $deal->amount, 10));   //после сделки деньги пришли на счет фиат покупателю, доллары ушли со счета

        $deal->refresh();

        $this->assertTrue($deal->status == Deal::STATUS_SOLD);
    }
}
