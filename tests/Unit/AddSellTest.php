<?php

namespace Tests\Unit;

use App\Events\DealStatusChangedEvent;
use App\Models\BalanceAccept;
use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\PaymentSystem;
use App\Models\Selection;
use App\User;
use Tests\commands\AddBuyCryptCommandTest;
use Tests\commands\AddBuyFiatCommandTest;
use Tests\commands\AddSellCryptCommandTest;
use Tests\commands\AddSellFiatCommandTest;
use Tests\commands\BalanceAcceptCommandTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddSellTest extends TestCase
{
    public function testAddSellCryptBid()   //продавец продает cryp
    {
        $user = User::first();
        $amount = $user->amount;
        $addbc = new AddSellCryptCommandTest($this->api);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, ''));
        $addbc->handle([]); //select currency message

        // create bid

        $selection = Selection::latest()->first();

        $addbc->setUpdate($addbc->incomecallback_query($user, true, $addbc->getName() . '_2_0_' . $selection->id . '_' . Currency::find(1)->id));
        $addbc->handle($addbc->parseCallbackQuery()); //what amount message

        // $addbc->setUpdate($addbc->incomeMessageText($user, true, 'tipa comment'));
        // $addbc->handle(['step' => 4]);
        //
        // $this->assertTrue(Selection::latest()->first()->details == 'tipa comment');

        //sub bids message

        $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.sub_bids.yes')));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid = Bid::latest()->first();

        $bid->refresh();
        $this->assertTrue(!$bid->is_fixed_price);

        //what percent

        $percent = round(rand(-100, 100)/100, 2);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, $percent));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->commission_creator_value == $percent);

        //what BTC you can bid

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 1));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_total == 1);

        $user->refresh();
        $this->assertTrue(($amount) == $user->amount);

        //min avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 0.01));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_min == 0.01);

        //max avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 1));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_max == 1);

        //upload video

        $addbc->setUpdate($addbc->video($user, true));
        $addbc->handle(['step'  => $user->refresh()->op]);

        //done
        $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.accept_done')));
        $addbc->handle(['step' => $user->refresh()->op]);

        $bid = Bid::latest()->first();
        $this->assertTrue($bid->status == Bid::STATUS_PREPARE);

        //accept balance
        /** @var BalanceAccept $balanceAccept */
        $balanceAccept = BalanceAccept::latest()->first();
        $blacc = new BalanceAcceptCommandTest($this->api);
        $blacc->setUpdate($blacc->incomecallback_query($user, true, $blacc->getName() . '_allow_' . $balanceAccept->token));
        $blacc->handle($blacc->parseCallbackQuery());

        $bid->refresh();
        $this->assertTrue($bid->status == Bid::STATUS_OPEN);
    }

    public function testSellFiatBid()
    {
        $user = User::first();
        $amount = $user->amount;
        $addbc = new AddSellFiatCommandTest($this->api);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, ''));
        $addbc->handle([]); //select currency message

        // create bid

        $selection = Selection::latest()->first();

        $addbc->setUpdate($addbc->incomecallback_query($user, true, $addbc->getName() . '_2_0_' . $selection->id . '_' . Currency::find(4)->id));    //select currency
        $addbc->handle($addbc->parseCallbackQuery());   //what payment system message

        $addbc->setUpdate($addbc->incomecallback_query($user, true, $addbc->getName() . '_3_0_' . $selection->id . '_' . PaymentSystem::find(1)->id));   //select Payment system
        $addbc->handle($addbc->parseCallbackQuery());
        //what amount message

        // $addbc->setUpdate($addbc->incomeMessageText($user, true, 'tipa comment fiat'));
        // $addbc->handle(['step' => 4]);
        //
        // $this->assertTrue(Selection::latest()->first()->details == 'tipa comment fiat');

        //sub bids message

        $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.sub_bids.yes')));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid = Bid::latest()->first();

        $bid->refresh();
        $this->assertTrue(!$bid->is_fixed_price);

        //what percent

        $percent = round(rand(-100, 100)/100, 2);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, $percent));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->commission_creator_value == $percent);

        //what UAH you can bid

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 1800));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_total == 1800);

        $user->refresh();
        $this->assertTrue(($amount) == $user->amount);

        //min avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 100));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_min == 100);

        //max avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 600));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_max == 600);

        //upload video

        $addbc->setUpdate($addbc->video($user, true));
        $addbc->handle(['step'  => $user->refresh()->op]);

        //done
        $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.accept_done')));
        $addbc->handle(['step' => $user->refresh()->op]);

        $bid = Bid::latest()->first();
        $this->assertTrue($bid->status == Bid::STATUS_PREPARE);

        //accept balance
        /** @var BalanceAccept $balanceAccept */
        $balanceAccept = BalanceAccept::latest()->first();
        $blacc = new BalanceAcceptCommandTest($this->api);
        $blacc->setUpdate($blacc->incomecallback_query($user, true, $blacc->getName() . '_allow_' . $balanceAccept->token));
        $blacc->handle($blacc->parseCallbackQuery());

        $bid->refresh();
        $this->assertTrue($bid->status == Bid::STATUS_OPEN);
    }
}
