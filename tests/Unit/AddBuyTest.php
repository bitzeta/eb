<?php

namespace Tests\Unit;

use App\Events\DealStatusChangedEvent;
use App\Models\BalanceAccept;
use App\Models\Bid;
use App\Models\Currency;
use App\Models\Deal;
use App\Models\PaymentSystem;
use App\Models\Selection;
use App\User;
use Tests\commands\AddBuyCryptCommandTest;
use Tests\commands\AddBuyFiatCommandTest;
use Tests\commands\BalanceAcceptCommandTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddBuyTest extends TestCase
{
    public function testAddBuyCryptBid()   //продавец покупает cryp
    {
        $user = User::first();
        $amount = $user->amount;
        $addbc = new AddBuyCryptCommandTest($this->api);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, ''));
        $addbc->handle([]); //select currency message

        // create bid

        $selection = Selection::latest()->first();

        $addbc->setUpdate($addbc->incomecallback_query($user, true, $addbc->getName() . '_2_0_' . $selection->id . '_' . Currency::find(1)->id));
        $addbc->handle($addbc->parseCallbackQuery()); //what amount message

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 'tipa wallet'));
        $addbc->handle(['step' => $user->refresh()->op]);

        $this->assertTrue(Selection::latest()->first()->details == 'tipa wallet');

        //sub bids message

        $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.sub_bids.yes')));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid = Bid::latest()->first();

        $bid->refresh();
        $this->assertTrue(!$bid->is_fixed_price);

        //what percent

        $percent = round(rand(-100, 100)/100, 2);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, $percent));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->commission_creator_value == $percent);

        //what usd you can bid

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 1000));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $user->refresh();
        $this->assertTrue(($amount - 1000) == $user->amount);

        $bid->refresh();
        $this->assertTrue($bid->amount_total == 1000);

        //min avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 10));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_min == 10);

        //min avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 1000));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_max == 1000);

        // //upload video
        //
        // $addbc->setUpdate($addbc->video($user, true));
        // $addbc->handle(['step'  => $user->refresh()->op]);

        //done
        // $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.accept_done')));
        // $addbc->handle(['step' => $user->refresh()->op]);

        $bid = Bid::latest()->first();
        $this->assertTrue($bid->status == Bid::STATUS_PREPARE);

        //accept balance
        /** @var BalanceAccept $balanceAccept */
        $balanceAccept = BalanceAccept::latest()->first();
        $blacc = new BalanceAcceptCommandTest($this->api);
        $blacc->setUpdate($blacc->incomecallback_query($user, true, $blacc->getName() . '_allow_' . $balanceAccept->token));
        $blacc->handle($blacc->parseCallbackQuery());

        $bid->refresh();
        $this->assertTrue($bid->status == Bid::STATUS_OPEN);
    }

    public function testBuyFiatBid()    //продавец покупает фиат за долл
    {
        $user = User::first();
        $amount = $user->amount;
        $addbc = new AddBuyFiatCommandTest($this->api);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, ''));
        $addbc->handle([]); //select currency message

        // create bid

        $selection = Selection::latest()->first();

        $addbc->setUpdate($addbc->incomecallback_query($user, true, $addbc->getName() . '_2_0_' . $selection->id . '_' . Currency::find(4)->id));    //select currency
        $addbc->handle($addbc->parseCallbackQuery());   //what payment system message

        $addbc->setUpdate($addbc->incomecallback_query($user, true, $addbc->getName() . '_3_0_' . $selection->id . '_' . PaymentSystem::find(1)->id));   //select Payment system
        $addbc->handle($addbc->parseCallbackQuery());
        //what wallet message

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 'tipa wallet'));
        $addbc->handle(['step' => $user->refresh()->op]);

        $this->assertTrue(Selection::latest()->first()->details == 'tipa wallet');

        //sub bids message

        $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.sub_bids.yes')));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid = Bid::latest()->first();

        $bid->refresh();
        $this->assertTrue(!$bid->is_fixed_price);

        //what percent

        $percent = round(rand(-100, 100)/100, 2);
        $addbc->setUpdate($addbc->incomeMessageText($user, true, $percent));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->commission_creator_value == $percent);

        //what usd you can bid

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 2000));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_total == 2000);

        $user->refresh();
        $this->assertTrue(($amount - 2000) == $user->amount);

        //min avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 10));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_min == 10);

        //min avail amount

        $addbc->setUpdate($addbc->incomeMessageText($user, true, 800));
        $addbc->handle(['step'  => $user->refresh()->op]);

        $bid->refresh();
        $this->assertTrue($bid->amount_max == 800);

        //upload video

        // $addbc->setUpdate($addbc->video($user, true));
        // $addbc->handle(['step'  => $user->refresh()->op]);

        //done
        // $addbc->setUpdate($addbc->incomeMessageText($user, true, trans('bid.accept_done')));
        // $addbc->handle(['step' => $user->refresh()->op]);

        $bid = Bid::latest()->first();
        $this->assertTrue($bid->status == Bid::STATUS_PREPARE);

        //accept balance
        /** @var BalanceAccept $balanceAccept */
        $balanceAccept = BalanceAccept::latest()->first();
        $blacc = new BalanceAcceptCommandTest($this->api);
        $blacc->setUpdate($blacc->incomecallback_query($user, true, $blacc->getName() . '_allow_' . $balanceAccept->token));
        $blacc->handle($blacc->parseCallbackQuery());

        $bid->refresh();
        $this->assertTrue($bid->status == Bid::STATUS_OPEN);
    }
}
