<?php

namespace Tests;

use App\Services\BidsService;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /** @var Api */
    protected $api;

    /** @var BidsService */
    protected $bidsService;

    protected function setUp()
    {
        parent::setUp();

        $this->api = $api = new \Tests\Api(config('telegram.bot_token'));
                $this->bidsService = new BidsService($api);
    }
}
